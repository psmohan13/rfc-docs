---
title: "2020 Survey, Leafy Greens Samples Data Analysis"
author: "Real Food Campaign"
date: "07/26/2021"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
toc_depth: 3
---













# Some Summary Statistics

### Censoring The More Evident Outliers




## General: Variation and ratios between maximum and mimimum value


<!--html_preserve--><div id="htmlwidget-d8c22aebfa18dc038a33" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-d8c22aebfa18dc038a33">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108"],["Antioxidants_max","Antioxidants_median","Antioxidants_min","Antioxidants_range","Antioxidants_ratio","Antioxidants_sd","Brix_max","Brix_median","Brix_min","Brix_range","Brix_ratio","Brix_sd","Polyphenols_max","Polyphenols_median","Polyphenols_min","Polyphenols_range","Polyphenols_ratio","Polyphenols_sd","produce_xrf_Al_max","produce_xrf_Al_median","produce_xrf_Al_min","produce_xrf_Al_range","produce_xrf_Al_ratio","produce_xrf_Al_sd","produce_xrf_Ca_max","produce_xrf_Ca_median","produce_xrf_Ca_min","produce_xrf_Ca_range","produce_xrf_Ca_ratio","produce_xrf_Ca_sd","produce_xrf_Cl_max","produce_xrf_Cl_median","produce_xrf_Cl_min","produce_xrf_Cl_range","produce_xrf_Cl_ratio","produce_xrf_Cl_sd","produce_xrf_Cu_max","produce_xrf_Cu_median","produce_xrf_Cu_min","produce_xrf_Cu_range","produce_xrf_Cu_ratio","produce_xrf_Cu_sd","produce_xrf_Fe_max","produce_xrf_Fe_median","produce_xrf_Fe_min","produce_xrf_Fe_range","produce_xrf_Fe_ratio","produce_xrf_Fe_sd","produce_xrf_K_max","produce_xrf_K_median","produce_xrf_K_min","produce_xrf_K_range","produce_xrf_K_ratio","produce_xrf_K_sd","produce_xrf_Mg_max","produce_xrf_Mg_median","produce_xrf_Mg_min","produce_xrf_Mg_range","produce_xrf_Mg_ratio","produce_xrf_Mg_sd","produce_xrf_Mn_max","produce_xrf_Mn_median","produce_xrf_Mn_min","produce_xrf_Mn_range","produce_xrf_Mn_ratio","produce_xrf_Mn_sd","produce_xrf_Mo_max","produce_xrf_Mo_median","produce_xrf_Mo_min","produce_xrf_Mo_range","produce_xrf_Mo_ratio","produce_xrf_Mo_sd","produce_xrf_Na_max","produce_xrf_Na_median","produce_xrf_Na_min","produce_xrf_Na_range","produce_xrf_Na_ratio","produce_xrf_Na_sd","produce_xrf_Ni_max","produce_xrf_Ni_median","produce_xrf_Ni_min","produce_xrf_Ni_range","produce_xrf_Ni_ratio","produce_xrf_Ni_sd","produce_xrf_P_max","produce_xrf_P_median","produce_xrf_P_min","produce_xrf_P_range","produce_xrf_P_ratio","produce_xrf_P_sd","produce_xrf_S_max","produce_xrf_S_median","produce_xrf_S_min","produce_xrf_S_range","produce_xrf_S_ratio","produce_xrf_S_sd","produce_xrf_Si_max","produce_xrf_Si_median","produce_xrf_Si_min","produce_xrf_Si_range","produce_xrf_Si_ratio","produce_xrf_Si_sd","produce_xrf_Zn_max","produce_xrf_Zn_median","produce_xrf_Zn_min","produce_xrf_Zn_range","produce_xrf_Zn_ratio","produce_xrf_Zn_sd"],[7639.68,1958.23,64.95,7574.73,117.624018475751,1404.2222147906,7.3,4.05,1.8,5.5,4.05555555555556,1.17838538062179,205.06,68.44,22.55,182.51,9.09356984478936,40.2501556989562,0.42,0.24,0.13,0.29,3.23076923076923,0.0540995563941415,163.2,56.74,15.9,147.3,10.2641509433962,29.4099965646302,103.7,30,0.07,103.63,1481.42857142857,25.0588191468131,0.04,0.02,0.01,0.03,4,0.00757327163843338,1.01,0.55,0.24,0.77,4.20833333333333,0.191917372414602,270.93,152.38,79.35,191.58,3.41436672967864,43.1586593976345,36.24,14.14,5.49,30.75,6.60109289617486,6.54027504005691,0.92,0.28,0.12,0.8,7.66666666666667,0.146597431580035,0.01,0.01,0.01,0,1,0,7.87,2.06,0.05,7.82,157.4,2.06876343452813,0.02,0.01,0.01,0.01,2,0.0024230584229878,44.27,25.23,9.38,34.89,4.71961620469083,6.93511332705096,82.92,35.29,8.65,74.27,9.58612716763006,12.8221144113414,69.27,18.96,1.72,67.55,40.2732558139535,10.0655197173716,0.6,0.18,0.04,0.56,15,0.116385050937464],[25368.73,8586.79,1648.31,23720.42,15.3907517396606,5368.64973610439,17.8,11.4,3.8,14,4.68421052631579,3.02657603116231,683.61,196.21,59.96,623.65,11.4011007338225,154.4209407443,0.8,0.42,0.23,0.57,3.47826086956522,0.090827300862703,464.01,236.78,33.59,430.42,13.8139327180709,85.2720724673297,213.94,35.97,0.46,213.48,465.086956521739,40.6127510944513,0.11,0.06,0.03,0.08,3.66666666666667,0.0133092003474296,2.58,1.12,0.62,1.96,4.16129032258065,0.431255063783114,520.85,240.79,89.65,431.2,5.80981595092025,65.3616021176316,128.76,44.78,13.89,114.87,9.26997840172786,20.1885178574138,3.39,0.69,0.4,2.99,8.475,0.423505855785096,0.02,0.01,0.01,0.01,2,0.00307793505625546,29.18,7.45,0.01,29.17,2918,5.9542517202741,0.08,0.03,0.01,0.07,8,0.0123018020157042,102.07,38.52,13.36,88.71,7.63997005988024,16.9636563439343,237.22,105.28,8.34,228.88,28.4436450839329,43.3010991478887,146.44,34.98,18.23,128.21,8.03291278113,18.249114356002,1.81,0.27,0.07,1.74,25.8571428571429,0.213657666799027],[1472.38,438.285,126.22,1346.16,11.6651877673903,319.529043670724,17.6,7.9,3,14.6,5.86666666666667,3.3569184737411,179.4,83.67,32.61,146.79,5.50137994480221,33.4968965406216,1.3,0.6,0.3,1,4.33333333333333,0.213344695700815,238.86,79.845,20.61,218.25,11.589519650655,44.4654764958164,424.24,108.895,3.39,420.85,125.144542772861,106.134492768105,0.18,0.09,0.05,0.13,3.6,0.0333050219906236,4.13,1.78,0.97,3.16,4.25773195876289,0.723484228281052,743.51,369.295,213.71,529.8,3.47906040896542,110.686161243474,88.45,30.49,7.32,81.13,12.0833333333333,14.3575816407718,1.73,0.73,0.44,1.29,3.93181818181818,0.274567117552005,0.02,0.01,0.01,0.01,2,0.00383482494423685,33.66,6.09,0.15,33.51,224.4,6.23273921449807,0.13,0.04,0.01,0.12,13,0.0333205103540809,94.86,42.605,20.43,74.43,4.6431718061674,17.9126175174521,194.43,69.67,23.67,170.76,8.21419518377693,30.4318764697379,168.95,59.755,33.62,135.33,5.02528256989887,33.8908797773329,5.57,0.455,0.1,5.47,55.7,0.933713433292145],[7190.77,483.62,75.2,7115.57,95.6219414893617,1319.22223634339,5.3,3.6,1.7,3.6,3.11764705882353,0.813124027754568,166.24,27.78,1.98,164.26,83.959595959596,28.447670586075,0.51,0.33,0.16,0.35,3.1875,0.0819834069802092,117.7,43.46,1.01,116.69,116.534653465347,23.1711989818414,132.04,41.62,3.95,128.09,33.4278481012658,22.3411221456461,0.03,0.02,0.01,0.02,3,0.00586487933134912,1.23,0.6,0.18,1.05,6.83333333333333,0.235924332269693,378.27,226.46,95.6,282.67,3.95679916317992,60.8353658849738,55.75,25.55,7.32,48.43,7.61612021857923,9.97618568108492,1.15,0.45,0.17,0.98,6.76470588235294,0.220333486285561,0.01,0.01,0.01,0,1,0,5.41,1.245,0.64,4.77,8.453125,2.20388293700006,0.01,0.01,0.01,0,1,0,58.37,27.25,10.54,47.83,5.53795066413662,9.29107122920932,39.8,12.6,4.62,35.18,8.61471861471861,4.61563345110546,69.32,18.47,1.57,67.75,44.1528662420382,12.056501507482,0.42,0.12,0.03,0.39,14,0.0635787616855919],[18703.8,9357.65,2138.9,16564.9,8.74458833980083,2737.07154227935,11.3,9.1,3.5,7.8,3.22857142857143,1.35063519618038,236.04,143.88,63.85,172.19,3.69678935003915,28.1750616044502,0.38,0.35,0.31,0.07,1.2258064516129,0.0330403793359983,218.78,209.025,168.93,49.85,1.29509264192269,23.033906891074,58.83,6.265,3.99,54.84,14.7443609022556,26.6858131785411,0.05,0.035,0.03,0.02,1.66666666666667,0.00957427107756338,1.44,1.025,0.85,0.59,1.69411764705882,0.284546422691741,238.6,212.285,156.45,82.15,1.525087887504,34.9535544210695,68.7,37.365,29.01,39.69,2.36814891416753,17.8421990423453,0.71,0.6,0.51,0.2,1.3921568627451,0.0818535277187245,null,null,null,null,null,null,6.47,1.74,0.29,6.18,22.3103448275862,1.85655644088237,0.02,0.015,0.01,0.01,2,0.00577350269189626,61.07,51.53,41.71,19.36,1.46415727643251,8.13947582259857,109.88,84.14,63.95,45.93,1.7182173573104,19.9994272834666,40.98,22.495,19.42,21.56,2.11019567456231,10.109979805453,0.41,0.405,0.29,0.12,1.41379310344828,0.0585234995535981],[27160.75,6316.5,1861.12,25299.63,14.593766119326,4927.70450822094,11.6,8.55,4,7.6,2.9,1.56510608362816,719.59,181.015,72.19,647.4,9.96800110818673,150.426140601343,0.67,0.465,0.34,0.33,1.97058823529412,0.0885255986096417,258.58,157.76,35.03,223.55,7.38167285184128,54.8790859406219,126.93,50.93,2.81,124.12,45.1708185053381,34.6111306078554,0.07,0.04,0.02,0.05,3.5,0.0130472175216585,2.12,1.18,0.78,1.34,2.71794871794872,0.446692699533014,426,292.495,210.56,215.44,2.02317629179331,52.0664640053748,61.92,37.125,6.45,55.47,9.6,14.3171265056683,0.88,0.655,0.33,0.55,2.66666666666667,0.142401407716147,0.01,0.01,0.01,0,1,0,9.8,1.48,0.81,8.99,12.0987654320988,2.58281418048397,0.02,0.01,0.01,0.01,2,0.00408248290463863,70.1,43.03,21.77,48.33,3.22002756086357,13.8378627718284,125.46,83.37,27.69,97.77,4.53087757313109,23.5292601307149,123.07,53.335,19.64,103.43,6.2662932790224,27.0733489875615,0.69,0.315,0.2,0.49,3.45,0.132474677693079],[1489.78,441.62,20.53,1469.25,72.5660009741841,388.89575459586,9.8,6.5,2.3,7.5,4.26086956521739,2.08993892314281,490.98,217.78,102.22,388.76,4.80316963412248,107.57349922687,0.88,0.58,0.34,0.54,2.58823529411765,0.120119074932146,139.05,44.43,10.53,128.52,13.2051282051282,25.3014212557651,164.64,47.84,6.94,157.7,23.7233429394813,33.1178515708754,0.07,0.04,0.02,0.05,3.5,0.00947841486383216,1.73,1.07,0.69,1.04,2.50724637681159,0.29850121494989,617.63,407.22,170.81,446.82,3.6158889994731,101.325878172585,145.69,87.56,40.32,105.37,3.61334325396825,24.4525407559102,1.31,0.68,0.31,1,4.2258064516129,0.230617584426545,0.02,0.01,0.01,0.01,2,0.00316227766016838,14.08,10.55,8.69,5.39,1.62025316455696,2.73777890512242,0.01,0.01,0.01,0,1,0,89.22,36.16,18.5,70.72,4.8227027027027,13.1685830248527,62.14,32.86,18.14,44,3.42557883131202,7.04753382016,117.96,42.06,23.03,94.93,5.12201476335215,22.8123018909027,3.49,0.36,0.13,3.36,26.8461538461538,0.553021060339043],[3202.31,952.01,88.53,3113.78,36.1720320795211,628.292898577042,10.9,6.6,1.4,9.5,7.78571428571429,1.71142886619484,443.12,180.275,36.46,406.66,12.1535929786067,98.7931996037384,0.7,0.39,0.18,0.52,3.88888888888889,0.122329072886645,221.66,59.805,11.58,210.08,19.1416234887737,36.0856403091799,145.86,21.285,2.75,143.11,53.04,31.8480208119307,0.09,0.05,0.03,0.06,3,0.0153045112199024,1.72,0.885,0.42,1.3,4.09523809523809,0.350916317838226,534.44,266.7,45.56,488.88,11.7304653204565,107.830404930223,123.12,48.77,9.58,113.54,12.8517745302714,21.3677236381532,4.38,0.94,0.22,4.16,19.9090909090909,0.714553166197739,0.02,0.01,0.01,0.01,2,0.00403112887414927,23.82,6.3,0.36,23.46,66.1666666666667,5.97562902419892,0.04,0.01,0.01,0.03,4,0.00930604986568222,55.72,26.975,10.89,44.83,5.11662075298439,9.86588103087427,98.35,34.125,15.22,83.13,6.46189224704336,15.8678676719308,99.41,32.14,11.83,87.58,8.40321217244294,17.1330851888852,1.39,0.26,0.07,1.32,19.8571428571429,0.261413581032087]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>variable<\/th>\n      <th>bok_choi<\/th>\n      <th>kale<\/th>\n      <th>leeks<\/th>\n      <th>lettuce<\/th>\n      <th>mizuna<\/th>\n      <th>mustard_greens<\/th>\n      <th>spinach<\/th>\n      <th>swiss_chard<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":[2,3,4,5,6,7,8,9]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->


### Histograms of the Main Nutrients by Crop



![](greensDataAnalysis_files/figure-html/preliminaryhistograms-1.png)<!-- -->



# Farm Practices: Median Shifts Comparisons







# Synthesis Median Shift Tables.

  These tables synthesise the results from our investigations on the relationships between farm practices, soil amendments and the variables we've identified as indicators of nutritional quality on every studied crop. 

* Results shown are shift percentages.
* Hovering over a value will reveal the sample size, the amount of similar subjects over which the variable was measured for each comparison.
* If any result is intersting for you, you can get much more detailed additional information on the general brief, *Quality Realtionships*. Some immediately intersting magnitudes are: the absolute value of the medians, confidence intervals for each shift, values for factor/crop wich were rejected but could be useful if more subjects were available, etc.




## Color Scale

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;">Between 10% and 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;">Between 10% and 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">50% or more%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">50% or more</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for FarmP Practices 

The reference category for this table are farms that informed they do not employ any of these management techniques. The size of that reference sample is 533 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biodynamic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">1.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">40.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">187.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-22.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">72.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">90.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">177.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-20.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-25.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-29.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-23.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">6.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-2.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">9.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">24.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-18.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-27.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-10.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">44.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">34.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">38.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">17.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">32.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">8.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">7.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-5.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">11.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-39.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-41.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">66.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-50.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-22.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">6.99</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-3.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-2.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">79.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-15.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">3.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">3.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">23.06</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Biological </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-17.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">63.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">65.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">151.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-2.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">41.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">80.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">7.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">72.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">30.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">93.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">5.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">14.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-0.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">17.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">2.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Certified Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-16.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 22">37.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">22.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-8.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">53.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">30.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">1.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-7.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-30.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-26.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-1.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 24">-14.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-4.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-30.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-7.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-6.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-27.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-26.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-0.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">48.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-26.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-18.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-3.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">3.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-17.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-13.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-12.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">0.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-1.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-14.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">6.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-20.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-10.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">3.99</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-17.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-39.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-25.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-7.73</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Hydroponic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">172.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-52.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-50.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">-17.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">16.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">9.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">-0.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">-57.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">91.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">-33.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Irrigation </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">89.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">0.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">76.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-23.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-2.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 34">72.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">53.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">76.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">-33.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">-13.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">13.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">27.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">-12.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">20.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-4.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">35.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">-21.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-10.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">41.79</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">26.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">41.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">14.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-34.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-21.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">30.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-37.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">5.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">30.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-31.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">48.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-14.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">8.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">-21.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">4.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">12.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">15.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">6.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">53.86</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Local </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">10.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">23.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-29.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-16.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-22.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">35.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-14.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-36.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">8.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-20.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nospray </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">51.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 52">3.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">44.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">163.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 46">7.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">8.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 50">59.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">75.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">249.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 43">2.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">27.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-27.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">9.92</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">21.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">-2.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-13.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">25.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-21.99</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-5.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">62.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">26.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">19.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">39.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">4.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">46.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-12.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-6.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">5.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">17.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-36.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-39.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">51.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-47.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-7.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">30.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-15.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">17.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-19.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-6.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-10.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">-0.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 49">27.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">16.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-20.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-9.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">15.33</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">64.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 40">2.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">56.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 33">-3.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-2.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">51.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">106.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">30.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">39.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-27.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">16.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-5.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">15.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 20">-7.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">29.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-25.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-37.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">16.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">18.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">38.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">3.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-42.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-43.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">48.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">-40.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 23">1.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">35.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">25.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-23.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-11.11</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">-10.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">1.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 37">19.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">16.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-24.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">16.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">23.06</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Regenerative </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">25.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 27">23.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">53.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">163.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-20.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">29.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 26">77.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">107.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">249.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-4.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-4.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">-27.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 31">16.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">28.49</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-2.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">19.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-11.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">56.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-16.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-12.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">62.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">37.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">35.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">33.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-11.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">46.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-12.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-6.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">5.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">17.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">-25.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">-40.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">44.7</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">-45.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 28">-21.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">79.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-21.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">19.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-34.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">0.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">-13.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">3.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 25">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">17.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-13.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">10.61</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">23.07</span> </td>
  </tr>
</tbody>
</table>



## Percentual Shift Over the Median for Soil Amendments

The reference category are farms that informed they do not use soil amendments.The size of that reference sample is 327 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Mulch </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">36.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-1.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">3.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">54.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-34.57</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">25.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">41.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">0.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">50.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-2.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">69.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-16.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">40.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-9.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">7.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">6.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">7.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">4.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">32.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 113">-30.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 82">-25.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 86">-23.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 50">-15.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 39">19.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 111">-2.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 82">33.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 87">-10.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 48">42.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 32">2.47</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 91">46.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 79">26.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 81">28.36</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 44">11.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 29">21.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 55">30.77</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">24.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 35">-2.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">34.48</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">24.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">45.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">60.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">31.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">20.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">-15.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">7.14</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">48.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">9.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">-0.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">2.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">8.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-19.13</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-32.52</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">22.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-28.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 38">-0.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">28.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-18.64</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">25.74</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 36">-19.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">-6.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">46.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">1.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 77">17.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">-6.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">81.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">53.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 56">0.05</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Tillage Intensity

The reference category are farms informing they avoid tillage. The size of that reference sample is 152 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-30.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-30.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">67.16</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">7.07</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-2.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">40.25</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">31.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-0.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-7.69</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">-18.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-2.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-26.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-16.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-5.45</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-23.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">7.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-14.54</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-10.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">14.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">2.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">72.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-35.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">19.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-12.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-29.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-28.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">15.66</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-9.94</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">47.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Irrigation


The reference category are farms informing no irrigation. The size of that reference sample is 735 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Nonchlorine Water </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">76.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 31">-14.34</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">74.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-22.1</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-17.98</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 31">68.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">66.3</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">62.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-14.63</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">-28.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-5.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-3.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">17.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">-13.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">29.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">28.9</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-17.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-6.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">2.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">18.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">29.04</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">16.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-54.18</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">8.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">36.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">-11.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 7">54.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">20.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-34.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">47.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">0.02</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">16.87</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-16.73</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-1.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 30">-3.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">2.06</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">24.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">20.5</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">66.69</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Land Preparation Techniques


The reference category are farms informing that they use any form of tillage. The size of that reference sample is 217 .

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Antioxidants </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Polyphenols </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Brix </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Bqi </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Mg </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Mustard Greens Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard S </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Swiss Chard Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Bok Choi Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Kale Zn </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce K </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Ca </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Fe </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Lettuce Zn </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Broadforking </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">35.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 18">35.83</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">11.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-9.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">1.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 17">46.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">49.28</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 13">-3.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-22.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-25.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 4">23.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">-5.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">27.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 10">3.4</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">14.29</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-2.56</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">32.19</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">11.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-37.2</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-18.26</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">42.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">36.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-8.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">9.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">11.03</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-23.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">15.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 8">-26.44</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">70.88</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-3.51</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">1.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">48.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">19.33</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-9.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">0.39</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 16">-0.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-16.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">-3.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">22.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 5">70.72</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Sheet Mulching </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">59.8</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-8.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">127.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">262.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-5.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">-42.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">5.42</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">-4.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">32.37</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-28.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">4.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 11">-2.93</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">20.84</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">-45.46</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">-22.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 6">30</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Solarization </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">2.17</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">-29.55</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">36.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 21">-24.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-41.86</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 19">-8.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-14.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 14">-10.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-8.12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">5.76</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-0.09</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-5.43</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">12</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-1.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">1.01</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 15">-32.35</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">16.22</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-28.67</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">11.81</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 9">-20.59</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: black !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: none !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="NA">NA</span> </td>
  </tr>
</tbody>
</table>





# Random Forest on Antioxidants, Polyphenols and Brix

  These regressions are performed using a *Random Forest* algorithm. The only tuned parameter besides the selection of initial variables is `mtry`.
  For the classifications, we are using *Accuracy* as a precission metric, while for regressions the custom random forest $R^2$ is used, which is a special metric comparable to the homonym in the context of linear models.

### Compared Datasets

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-soil </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-max </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-pro </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> f-c-med </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-min </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-nir-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no-vis-who </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> c-no </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , processed    , nir_whole    , nir_processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole        , nir_whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil         , whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, soil </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , processed    , nir_whole    , nir_processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , processed </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole        , nir_whole </td>
   <td style="text-align:left;"> metadata     , farmPractices, whole </td>
   <td style="text-align:left;"> metadata     , farmPractices </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , processed       , nir_whole       , nir_processed </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , processed </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole           , nir_whole </td>
   <td style="text-align:left;"> metadata        , medFarmPractices, whole </td>
   <td style="text-align:left;"> metadata        , medFarmPractices </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices, whole           , nir_whole </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices, whole </td>
   <td style="text-align:left;"> color           , region          , minFarmPractices </td>
   <td style="text-align:left;"> color    , whole    , nir_whole </td>
   <td style="text-align:left;"> color, whole </td>
   <td style="text-align:left;"> color </td>
  </tr>
</tbody>
</table>





## Random Forest Regression



<!--html_preserve--><div id="htmlwidget-aa21be4371f9d18923d5" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-aa21be4371f9d18923d5">{"x":{"filter":"none","extensions":["Buttons"],"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52"],["Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","Antioxidants","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","BQI","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Brix","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols","Polyphenols"],["c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-soil","f-max-soil-vis-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-soil","f-max-soil-vis-who","f-max-vis-who"],[0.555679564969903,0.479322471018939,0.474653398421475,0.459206082165902,0.320027144206222,0.452636145554758,0.540936388287061,0.340346272571317,0.530820968160882,0.597968684881196,0.533450252870691,null,null,0.49371072801821,0.532605721359261,0.399944706693219,0.520942522320298,0.291663226408218,0.509260914808435,0.449637454639727,0.61022144314957,0.409765395874608,0.556909300271717,0.626822703716747,0.393146157587176,0.561992140358212,0.59648784614222,0.504327785152903,0.531474542626539,0.395884557191954,0.325796509993913,0.342313409355984,0.536710230991482,0.501076677064812,0.455294563942852,0.608164006415152,0.451366442013968,0.584250423435543,0.536647298631722,0.404878446955357,0.422199583982396,0.308486578076487,0.427030455018675,0.456993393073329,0.640062552218082,0.372525743416697,0.482835427648361,0.540674071483024,0.533662172029495,null,null,0.449232628069765],[93,92,93,103,102,103,93,92,93,93,92,null,null,93,84,83,84,94,93,94,84,83,84,84,83,84,86,85,86,96,95,96,86,85,86,86,85,86,94,93,94,104,103,104,94,93,94,94,93,null,null,94],[0.795698924731183,0.782608695652174,0.78494623655914,0.766990291262136,0.794117647058823,0.757281553398058,0.806451612903226,0.771739130434783,0.741935483870968,0.709677419354839,0.771739130434783,null,null,0.795698924731183,0.75,0.746987951807229,0.773809523809524,0.765957446808511,0.709677419354839,0.787234042553192,0.797619047619048,0.843373493975904,0.761904761904762,0.738095238095238,0.710843373493976,0.821428571428571,0.709302325581395,0.788235294117647,0.802325581395349,0.78125,0.736842105263158,0.791666666666667,0.779069767441861,0.776470588235294,0.802325581395349,0.767441860465116,0.776470588235294,0.813953488372093,0.75531914893617,0.763440860215054,0.75531914893617,0.798076923076923,0.776699029126214,0.740384615384615,0.808510638297872,0.774193548387097,0.829787234042553,0.776595744680851,0.774193548387097,null,null,0.74468085106383],[0.128006850559203,0.173158735288636,0.115266634741425,0.124305960190954,0.181468852051669,0.111016881847512,0.127227277052121,0.168721126725123,0.115729786643454,0.0831994596299031,0.169703142434543,null,null,0.126962360454848,0.520196578716872,0.529185664103887,0.521099779467527,0.51462158069083,0.527589144881205,0.543727884779587,0.544466856778757,0.581579570723717,0.534173145534906,0.492941101670795,0.52028173436579,0.519722851644629,0.392156862745098,0.494117647058824,0.470588235294118,0.527272727272727,0.563636363636364,0.538181818181818,0.448039215686274,0.498039215686274,0.490196078431372,0.392156862745098,0.549019607843137,0.490196078431372,0.504359249970142,0.498268243162546,0.501851188343485,0.556966741548408,0.480959947400142,0.439888225302723,0.554878777021378,0.494518093873164,0.536175803176878,0.54621999283411,0.52813209124567,null,null,0.460524901469008],[0.373685239562683,0.261507388946576,0.190603359082621,0.212108797765515,0.25080917776001,0.242789307434809,0.435326215143272,0.323581295588754,0.318256788291712,0.440627025353819,0.307764610534062,null,null,0.284040646687613,0.241288486277912,0.618195350606974,0.465160559021031,0.279962759182276,0.236567187868106,0.309253923503911,0.486298657711062,0.519100490641644,0.540827069202937,0.516575892502251,0.511751953896071,0.501448334785945,0.686529560978605,0.440398664932951,0.698084042720285,0.205486672114508,0.256276048839766,0.614252497770964,0.751512417530342,0.408602190333078,0.696263495168701,0.708507416933016,0.466265822910852,0.674154955229469,0.400149502786699,0.366189476565899,0.417726783600014,0.185981354354744,0.437658437711366,0.30641816860936,0.502818964389539,0.38489122677132,0.544656100644581,0.581090721563272,0.30415784877018,null,null,0.528180876356945],[156,106,156,189,121,189,156,106,156,156,106,null,null,156,88,69,88,110,81,110,88,69,88,88,69,88,118,72,118,151,87,151,118,72,118,118,72,118,153,103,153,189,121,189,153,103,153,153,103,null,null,153],[0.705128205128205,0.773584905660377,0.794871794871795,0.772486772486772,0.752066115702479,0.73015873015873,0.724358974358974,0.754716981132076,0.807692307692308,0.794871794871795,0.80188679245283,null,null,0.775641025641026,0.761363636363636,0.739130434782609,0.784090909090909,0.727272727272727,0.703703703703704,0.7,0.761363636363636,0.710144927536232,0.772727272727273,0.772727272727273,0.739130434782609,0.761363636363636,0.864406779661017,0.819444444444444,0.76271186440678,0.774834437086093,0.758620689655172,0.834437086092715,0.830508474576271,0.833333333333333,0.796610169491525,0.847457627118644,0.833333333333333,0.771186440677966,0.803921568627451,0.786407766990291,0.758169934640523,0.80952380952381,0.768595041322314,0.767195767195767,0.790849673202614,0.776699029126214,0.830065359477124,0.712418300653595,0.815533980582524,null,null,0.758169934640523],[0.44803968161254,0.682847080974405,0.667407123715866,0.587495450045869,0.719051475117351,0.568380583459704,0.35499015880433,0.691180738188084,0.630353163519608,0.43597381817152,0.672826007159915,null,null,0.552843680748781,0.523267995757787,0.628641343244388,0.563203456541477,0.545784609035533,0.601780507275293,0.488718837167011,0.537911089079768,0.628395268849684,0.578618957007597,0.566502378062909,0.606518032297317,0.542443052963296,0.62375,0.416666666666667,0.308333333333333,0.629310344827586,0.545945945945946,0.43448275862069,0.6175,0.418333333333333,0.326666666666667,0.63,0.454166666666667,0.31625,0.627776351264604,0.670664635612924,0.474268198741815,0.640614969829246,0.697362302573559,0.542568044678393,0.368211580433945,0.66872444480077,0.57209526254975,0.355998844524329,0.678938507175499,null,null,0.441435036590063],[0.42828469585961,0.436416616811739,0.413026298167004,0.426909586383072,0.411654621562304,0.427866151619238,0.560086609339177,0.459025051241807,0.384354314023473,0.514179214850216,0.403846771958128,null,null,0.46808030836976,0.445717319648061,0.530328015509788,0.562630258190784,0.286370908856254,0.527761784268973,0.42783420051349,0.624451761913151,0.632003201593303,0.549401998375511,0.604052638049257,0.647242365046742,0.560616902870025,0.583870356242045,0.509295589048516,0.430931596934921,0.12218145833769,0.30682978333539,0.305038125450158,0.579527953670624,0.398706577036202,0.461466145750573,0.443847829451397,0.324087693121828,0.473496135914929,0.536148853303675,0.598078775321009,0.407036569958866,0.311441529968552,0.439675707571933,0.484167787665609,0.54569293382599,0.534391361292747,0.445506530400746,0.473989629934115,0.654931330847399,null,null,0.501006814251619],[149,91,149,166,102,166,149,91,149,149,91,null,null,149,145,87,145,158,98,158,145,87,145,145,87,145,145,87,145,162,98,162,145,87,145,145,87,145,149,91,149,166,102,166,149,91,149,149,91,null,null,149],[0.805369127516778,0.835164835164835,0.724832214765101,0.807228915662651,0.803921568627451,0.765060240963855,0.738255033557047,0.78021978021978,0.825503355704698,0.724832214765101,0.824175824175824,null,null,0.704697986577181,0.83448275862069,0.747126436781609,0.793103448275862,0.778481012658228,0.744897959183674,0.772151898734177,0.786206896551724,0.804597701149425,0.8,0.793103448275862,0.816091954022989,0.76551724137931,0.751724137931034,0.770114942528736,0.806896551724138,0.796296296296296,0.755102040816326,0.820987654320988,0.737931034482759,0.735632183908046,0.786206896551724,0.751724137931034,0.781609195402299,0.841379310344828,0.805369127516778,0.769230769230769,0.798657718120805,0.753012048192771,0.754901960784314,0.795180722891566,0.818791946308725,0.802197802197802,0.76510067114094,0.825503355704698,0.78021978021978,null,null,0.785234899328859],[0.346294017894961,0.377648374902836,0.0717629189234476,0.410681644707858,0.384935377376614,0.082718293286533,0.0489512659435659,0.442647975906534,0.1305910115602,0.0554753087090153,0.389734859568079,null,null,0.0665871266129745,0.679171502544416,0.69605422114838,0.460462458709044,0.677952861351665,0.712727401932916,0.53550932952415,0.48272475671815,0.663196418419557,0.471225783412195,0.472904204981698,0.690076037521319,0.416123560396393,0.305555555555556,0.48,0.5,0.616666666666666,0.485,0.616666666666667,0.305555555555556,0.504,0.416666666666667,0.305555555555556,0.52,0.427777777777778,0.248699836994489,0.427558257345491,0.301420476597067,0.325607705243846,0.362578759337447,0.245627579880752,0.223954047970193,0.423037954952849,0.209811379337111,0.241558643173174,0.435429818408542,null,null,0.216797329814484],[0.626076815200368,null,0.673965223101014,0.763740412081196,null,0.591466709547306,0.597579712209165,null,0.658768071199964,0.669638322639169,null,null,null,0.62746485350377,null,null,null,null,null,null,null,null,null,null,null,null,0.493803773891801,null,0.318020408935239,0.356037887876904,null,0.490890259756233,0.414688606680154,null,0.432166613833958,0.462373398650581,null,0.430061360006626,0.671343715422326,null,0.600404776894311,0.604507439415877,null,0.528791545532369,0.769905365081726,null,0.514639669321318,0.689492111276548,null,null,null,0.773739600174235],[108,null,108,112,null,112,108,null,108,108,null,null,null,108,null,null,null,null,null,null,null,null,null,null,null,null,101,null,101,105,null,105,101,null,101,101,null,101,108,null,108,112,null,112,108,null,108,108,null,null,null,108],[0.75,null,0.712962962962963,0.767857142857143,null,0.803571428571429,0.759259259259259,null,0.805555555555556,0.75,null,null,null,0.75,null,null,null,null,null,null,null,null,null,null,null,null,0.782178217821782,null,0.732673267326733,0.780952380952381,null,0.838095238095238,0.782178217821782,null,0.792079207920792,0.792079207920792,null,0.772277227722772,0.740740740740741,null,0.731481481481482,0.776785714285714,null,0.785714285714286,0.833333333333333,null,0.740740740740741,0.787037037037037,null,null,null,0.777777777777778],[0.163526338418778,null,0.149064752306178,0.417482531732237,null,0.250833688234467,0.161901916873664,null,0.187555522499373,0.159603706186691,null,null,null,0.159413812004964,null,null,null,null,null,null,null,null,null,null,null,null,0.371428571428571,null,0.371428571428571,0.472857142857143,null,0.408571428571428,0.474285714285714,null,0.44,0.442857142857143,null,0.371428571428571,0.215603863108658,null,0.21331056297742,0.566559545041559,null,0.225714237641754,0.403716727798903,null,0.217410909580375,0.224295857589932,null,null,null,0.221755224282397],[0.385180193955194,0.34200463091642,0.360343247806243,0.297347441976439,0.312822520201571,0.326328191143281,0.540134771216668,0.363586745533739,0.46490758708738,0.543130879566873,0.29996326717837,0.694316755774004,0.641174804046231,0.442797577876432,0.424077867455506,0.3738137886603,0.465905371176897,0.566467495586839,0.403610660544802,0.429766780614487,0.479237981519676,0.395677843349403,0.43848834347246,0.656006875279329,0.473557607570632,0.482307420298906,0.409892865076016,0.378588050844055,0.352269783502728,0.434168311907352,0.392838114368329,0.38147647191095,0.484514183655074,0.412733345535041,0.50695566916856,0.456205265438339,0.360078600509538,0.32572797475428,0.522970457971124,0.304836506251253,0.397178372896418,0.220112932662299,0.283195290662197,0.27846895748811,0.568119807030879,0.33402758149461,0.452569028532507,0.467899893834841,0.356123880825522,0.614100656897685,0.47169891981363,0.394143204361709],[97,95,97,107,105,107,97,95,97,97,95,50,50,97,85,83,85,91,89,91,85,83,85,85,83,85,86,84,86,92,90,92,86,84,86,86,84,86,97,95,97,107,105,107,97,95,97,97,95,50,50,97],[0.742268041237113,0.736842105263158,0.77319587628866,0.728971962616822,0.723809523809524,0.719626168224299,0.793814432989691,0.747368421052632,0.752577319587629,0.77319587628866,0.736842105263158,0.72,0.74,0.783505154639175,0.705882352941177,0.771084337349398,0.752941176470588,0.769230769230769,0.730337078651685,0.78021978021978,0.788235294117647,0.807228915662651,0.776470588235294,0.788235294117647,0.759036144578313,0.788235294117647,0.779069767441861,0.75,0.755813953488372,0.739130434782609,0.755555555555556,0.75,0.790697674418605,0.738095238095238,0.779069767441861,0.732558139534884,0.785714285714286,0.802325581395349,0.711340206185567,0.757894736842105,0.804123711340206,0.775700934579439,0.723809523809524,0.738317757009346,0.814432989690722,0.821052631578947,0.835051546391753,0.721649484536082,0.736842105263158,0.8,0.8,0.845360824742268],[0.110905063170367,0.136194026614301,0.135915948631034,0.134563636142852,0.137078047451935,0.134534752808969,0.117021824698113,0.136507146239876,0.132898897923002,0.108926858377988,0.13485828084315,0.453328898926462,0.582202854225872,0.131726217220018,0.448645993129173,0.531867765945903,0.531483160899001,0.593250920573359,0.527770803862874,0.581183619916439,0.476390989082532,0.525033025164827,0.462403574980314,0.472948930701408,0.521715476881389,0.465905685903058,0.473684210526316,0.47,0.431578947368421,0.471578947368421,0.463157894736842,0.431578947368421,0.413684210526316,0.468421052631579,0.455789473684211,0.384210526315789,0.482105263157895,0.460526315789474,0.489552053347692,0.585089154430799,0.582351357508232,0.610516288027834,0.582351357508232,0.559904322074264,0.502226272081512,0.563257191376561,0.585074657775385,0.511856193178288,0.586969577733137,0.573300344648324,0.52820725109267,0.568302027460808]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared_bok_choi<\/th>\n      <th>N_bok_choi<\/th>\n      <th>covers_bok_choi<\/th>\n      <th>width_bok_choi<\/th>\n      <th>cvRsquared_kale<\/th>\n      <th>N_kale<\/th>\n      <th>covers_kale<\/th>\n      <th>width_kale<\/th>\n      <th>cvRsquared_lettuce<\/th>\n      <th>N_lettuce<\/th>\n      <th>covers_lettuce<\/th>\n      <th>width_lettuce<\/th>\n      <th>cvRsquared_mustard_greens<\/th>\n      <th>N_mustard_greens<\/th>\n      <th>covers_mustard_greens<\/th>\n      <th>width_mustard_greens<\/th>\n      <th>cvRsquared_swiss_chard<\/th>\n      <th>N_swiss_chard<\/th>\n      <th>covers_swiss_chard<\/th>\n      <th>width_swiss_chard<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"dom":"Bfrtipl","lengthMenu":[7,21,42,84],"buttons":["colvis"],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[7]; $(this.api().cell(row, 7).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[11]; $(this.api().cell(row, 11).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[15]; $(this.api().cell(row, 15).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[19]; $(this.api().cell(row, 19).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->

## Random Forest Regression over BQI Minerals 

<!--html_preserve--><div id="htmlwidget-29858de1fef4eac08fa5" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-29858de1fef4eac08fa5">{"x":{"filter":"none","extensions":["Buttons"],"data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72"],["produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Ca_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_Fe_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_K_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_Mg_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_S_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile","produce_xrf_Zn_Percentile"],["c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-who","f-c-med-vis-who","f-max","f-max-nir-who","f-max-vis-who"],[0.333604734961027,0.267777042416152,0.414218087033111,0.235027558657701,0.352161462041058,0.293052496669647,0.465757890085166,0.360383956978467,0.349038661681991,0.38608844321604,0.280643348386842,0.392069967443548,0.495387834967474,0.350084835617595,0.4703489308828,0.287545058835259,0.469046090654373,0.357150450308106,0.581587595315109,0.370770927845142,0.408860385973294,0.500064703755413,0.305977561731319,0.510902519343117,0.563504240802481,0.416133745964719,0.554832501370766,0.378457435376026,0.344754442892988,0.395054935685778,0.431279879973889,0.46045730383966,0.306158742936687,0.559167205865023,0.438894149693292,0.307018190854838,0.344417500419997,0.408373188327088,0.334848640633989,0.256963610767785,0.459438724150766,0.399056411913723,0.41033046659229,0.485514927770208,0.399764376097316,0.504734621502543,0.287798922604188,0.463054523553775,0.546154154217667,0.394564128559119,0.541507630610548,0.413343052469044,0.402595392130729,0.382721027361752,0.623792028409049,0.443537290057062,0.492632624740769,0.524123248715678,0.359286722571919,0.488287379569205,0.48722067211824,0.317663505759366,0.426268691697932,0.276856608698477,0.189651565603876,0.181979507378867,0.473835806432146,0.287495790270509,0.388237798101719,0.505440235324233,0.258037661486984,0.328373244750309],[93,92,93,103,102,103,93,92,93,93,92,93,93,92,93,103,102,103,93,92,93,93,92,93,93,92,93,103,102,103,93,92,93,93,92,93,93,92,93,103,102,103,93,92,93,93,92,93,93,92,93,103,102,103,93,92,93,93,92,93,93,92,93,103,102,103,93,92,93,93,92,93],[0.741935483870968,0.739130434782609,0.774193548387097,0.737864077669903,0.715686274509804,0.728155339805825,0.78494623655914,0.717391304347826,0.720430107526882,0.763440860215054,0.739130434782609,0.752688172043011,0.881720430107527,0.815217391304348,0.860215053763441,0.893203883495146,0.794117647058823,0.796116504854369,0.709677419354839,0.826086956521739,0.849462365591398,0.763440860215054,0.847826086956522,0.870967741935484,0.817204301075269,0.760869565217391,0.795698924731183,0.805825242718447,0.705882352941177,0.728155339805825,0.795698924731183,0.793478260869565,0.806451612903226,0.806451612903226,0.782608695652174,0.752688172043011,0.720430107526882,0.804347826086957,0.827956989247312,0.87378640776699,0.833333333333333,0.883495145631068,0.860215053763441,0.826086956521739,0.89247311827957,0.709677419354839,0.815217391304348,0.881720430107527,0.720430107526882,0.75,0.795698924731183,0.776699029126214,0.725490196078431,0.766990291262136,0.720430107526882,0.728260869565217,0.78494623655914,0.731182795698925,0.706521739130435,0.752688172043011,0.752688172043011,0.793478260869565,0.731182795698925,0.757281553398058,0.735294117647059,0.776699029126214,0.731182795698925,0.815217391304348,0.741935483870968,0.806451612903226,0.75,0.817204301075269],[0.806060606060606,0.781818181818182,0.818181818181818,0.801980198019802,0.772277227722772,0.762376237623762,0.848484848484848,0.792929292929293,0.814141414141414,0.868686868686869,0.797979797979798,0.809090909090909,0.892682926829268,0.841463414634146,0.763414634146341,0.907142857142857,0.833333333333333,0.740476190476192,0.585365853658536,0.823170731707317,0.780487804878049,0.609756097560976,0.833536585365854,0.890243902439024,0.795918367346939,0.747959183673469,0.780612244897959,0.852040816326531,0.806122448979592,0.733673469387755,0.749489795918367,0.755102040816326,0.779591836734694,0.697959183673469,0.716836734693878,0.653061224489796,0.746666666666667,0.844,0.866666666666667,0.88,0.833333333333333,0.88,0.82,0.853333333333333,0.866666666666667,0.72,0.84,0.893333333333333,0.641304347826087,0.748369565217391,0.867391304347826,0.884782608695652,0.782608695652174,0.804347826086956,0.652173913043478,0.760869565217391,0.854347826086957,0.652173913043478,0.733695652173913,0.673913043478261,0.810526315789474,0.875,0.776842105263158,0.835051546391753,0.804639175257732,0.835051546391752,0.8,0.873684210526316,0.8,0.852631578947368,0.784210526315789,0.88421052631579],[0.272822019259306,0.261531755740281,0.253735834527861,0.167147849757837,0.314585320546702,0.13162483893778,0.228002285375998,0.407162700246438,0.254823749242475,0.407273267546884,0.464015059825647,0.319507330617029,0.466075180346164,0.38403539117424,0.339544020750747,0.0985387969803639,0.274205274794074,0.268263489049415,0.465061898943284,0.411119965330103,0.384425901899379,0.500896896754382,0.395940881318093,0.447702262124068,0.350866637124279,0.358037739137136,0.256454622039233,0.191805208118914,0.254491452547905,0.249407459036379,0.315487091648203,0.420002179119977,0.339475018629672,0.388152729208282,0.359933174504509,0.210218720473236,0.262469846349683,0.220914092396914,0.36853814672703,0.203506270756715,0.277495229069099,0.329915656185013,0.397311082800515,0.323779778334091,0.392327012249132,0.374676022918393,0.345221860548469,0.298615686317602,0.619569145319763,0.363904348810735,0.615584114500009,0.110640789941112,0.202884498329084,0.467641178513023,0.589551405619509,0.420031101661325,0.503708108211088,0.551203400043162,0.439839674408437,0.453674861284442,0.468314135506556,0.429197419459942,0.432069081708536,0.269872587244329,0.267539197324382,0.277080203197521,0.41357778623272,0.377115756014678,0.447513546839138,0.452909875735829,0.464616438433708,0.361154415610076],[129,106,129,157,124,157,129,106,129,129,106,129,129,106,129,157,124,157,129,106,129,129,106,129,129,106,129,157,124,157,129,106,129,129,106,129,129,106,129,157,124,157,129,106,129,129,106,129,129,106,129,157,124,157,129,106,129,129,106,129,129,106,129,157,124,157,129,106,129,129,106,129],[0.798449612403101,0.783018867924528,0.782945736434108,0.751592356687898,0.733870967741935,0.719745222929936,0.728682170542636,0.754716981132076,0.782945736434108,0.790697674418605,0.792452830188679,0.775193798449612,0.829457364341085,0.886792452830189,0.837209302325581,0.89171974522293,0.879032258064516,0.89171974522293,0.883720930232558,0.886792452830189,0.806201550387597,0.790697674418605,0.886792452830189,0.883720930232558,0.806201550387597,0.754716981132076,0.720930232558139,0.738853503184713,0.733870967741935,0.777070063694268,0.705426356589147,0.773584905660377,0.720930232558139,0.837209302325581,0.80188679245283,0.744186046511628,0.744186046511628,0.783018867924528,0.767441860465116,0.751592356687898,0.758064516129032,0.732484076433121,0.782945736434108,0.783018867924528,0.813953488372093,0.713178294573643,0.783018867924528,0.798449612403101,0.837209302325581,0.792452830188679,0.767441860465116,0.75796178343949,0.766129032258065,0.815286624203822,0.86046511627907,0.820754716981132,0.782945736434108,0.829457364341085,0.783018867924528,0.775193798449612,0.813953488372093,0.89622641509434,0.906976744186046,0.89171974522293,0.895161290322581,0.853503184713376,0.891472868217054,0.924528301886792,0.922480620155039,0.837209302325581,0.915094339622642,0.906976744186046],[0.772903225806452,0.766025641025641,0.770967741935484,0.801935483870968,0.736774193548387,0.748387096774194,0.696774193548387,0.791025641025641,0.754838709677419,0.807741935483871,0.77724358974359,0.767741935483871,0.706521739130442,0.763440860215054,0.782608695652174,0.869565217391304,0.771739130434783,0.858695652173913,0.83695652173913,0.812903225806452,0.771739130434783,0.608695652173913,0.763440860215054,0.826086956521739,0.787096774193548,0.814102564102564,0.745161290322581,0.781935483870968,0.778709677419355,0.816774193548387,0.709677419354839,0.794871794871795,0.780645161290323,0.8,0.808974358974359,0.783225806451613,0.72258064516129,0.735483870967742,0.72258064516129,0.825806451612903,0.724675324675325,0.651612903225807,0.735483870967742,0.743548387096774,0.740645161290323,0.683870967741936,0.75741935483871,0.734838709677419,0.806493506493506,0.658064516129032,0.558441558441558,0.82987012987013,0.681493506493506,0.779220779220779,0.798701298701299,0.676774193548387,0.569480519480519,0.558441558441558,0.670967741935484,0.590909090909091,0.635416666666667,0.830208333333333,0.791666666666667,0.856701030927835,0.864583333333333,0.783505154639175,0.71875,0.838541666666667,0.786458333333333,0.666666666666667,0.802083333333333,0.778125],[0.553768991677664,0.497707796622573,0.476306452079234,0.229589720899709,0.450206470828913,0.475275550944372,0.524261218557959,0.661605267319258,0.523184060123706,0.599667979758482,0.60304038509052,0.452978727324737,0.307511582169941,0.351103306948208,0.390012895010508,0.166550216959892,0.434002334519141,0.335539211543013,0.456159944928625,0.37861538942438,0.323178754371607,0.513466693142982,0.375339474011144,0.413392327352043,0.31518144055295,0.420992774747998,0.420287861503803,0.182088599107738,0.233383009228645,0.277329666175998,0.373528762008682,0.277679091334618,0.425078346524021,0.420636560992422,0.293311874019712,0.363585697588241,0.389154812162449,0.409993024966599,0.325159108384109,0.201870838784999,0.471824179829072,0.362863295489267,0.435073900613101,0.292381651920324,0.346208172105015,0.381263146956718,0.407348066339152,0.352246062127047,0.538557911577897,0.432872515928442,0.500581318797384,0.264992060201401,0.401903782181104,0.340540465719848,0.469105389449335,0.60391995983728,0.413839552001929,0.423310069343743,0.560804163814647,0.43322297120545,0.379672033322685,0.396734142835644,0.302754334944253,0.251557312124099,0.426192212232147,0.391252487327848,0.336919605679577,0.399080684547423,0.305096355043594,0.355441653883436,0.353643973618923,0.292076892231052],[149,91,149,162,102,162,149,91,149,149,91,149,149,91,149,162,102,162,149,91,149,149,91,149,149,91,149,162,102,162,149,91,149,149,91,149,149,91,149,162,102,162,149,91,149,149,91,149,149,91,149,162,102,162,149,91,149,149,91,149,149,91,149,162,102,162,149,91,149,149,91,149],[0.718120805369127,0.769230769230769,0.805369127516778,0.790123456790123,0.774509803921569,0.808641975308642,0.731543624161074,0.769230769230769,0.845637583892617,0.818791946308725,0.769230769230769,0.838926174496644,0.778523489932886,0.791208791208791,0.812080536912752,0.771604938271605,0.735294117647059,0.734567901234568,0.771812080536913,0.769230769230769,0.785234899328859,0.778523489932886,0.758241758241758,0.805369127516778,0.778523489932886,0.725274725274725,0.771812080536913,0.759259259259259,0.754901960784314,0.716049382716049,0.711409395973154,0.758241758241758,0.74496644295302,0.791946308724832,0.769230769230769,0.738255033557047,0.899328859060403,0.868131868131868,0.912751677852349,0.882716049382716,0.892156862745098,0.907407407407407,0.879194630872483,0.89010989010989,0.812080536912752,0.798657718120805,0.89010989010989,0.912751677852349,0.89261744966443,0.879120879120879,0.845637583892617,0.882716049382716,0.862745098039216,0.91358024691358,0.906040268456376,0.901098901098901,0.879194630872483,0.885906040268456,0.901098901098901,0.919463087248322,0.711409395973154,0.802197802197802,0.825503355704698,0.777777777777778,0.764705882352941,0.771604938271605,0.798657718120805,0.78021978021978,0.852348993288591,0.711409395973154,0.769230769230769,0.838926174496644],[0.513924050632911,0.706918238993711,0.607594936708861,0.812101910828025,0.764150943396226,0.710191082802548,0.493670886075949,0.704402515723271,0.70379746835443,0.588607594936709,0.691823899371069,0.69746835443038,0.480263157894737,0.803921568627451,0.572368421052632,0.849006622516557,0.849673202614379,0.589403973509934,0.564473684210526,0.791503267973856,0.539473684210526,0.547368421052632,0.803921568627451,0.5875,0.618092105263158,0.869281045751634,0.736842105263158,0.795364238410596,0.856862745098039,0.71523178807947,0.539473684210526,0.854901960784314,0.611842105263158,0.644736842105263,0.89281045751634,0.631578947368421,0.87,0.71,0.86,0.878787878787879,0.698000000000001,0.848484848484849,0.818,0.73,0.73,0.71,0.67,0.86,0.747663551401869,0.87962962962963,0.710280373831776,0.823584905660378,0.847222222222222,0.792452830188679,0.785046728971962,0.87962962962963,0.710280373831776,0.844859813084112,0.877777777777778,0.779439252336449,0.695104895104895,0.802083333333333,0.841958041958042,0.838028169014085,0.844791666666667,0.707042253521127,0.769230769230769,0.830555555555556,0.86013986013986,0.664335664335664,0.819444444444445,0.848951048951049],[0.487107640030137,0.218563466102368,0.365959670924346,0.322877277680398,0.4125818494456,0.30445698472011,0.557013961158059,0.313230643757145,0.400943237386233,0.407868518475473,0.292415356107968,0.489005402963756,0.460085586611963,0.324619984721159,0.429774592670774,0.25922507162562,0.404908911913449,0.376073629974464,0.435391091721253,0.351771639924305,0.47246368059024,0.478820524813998,0.310971555171408,0.471606246029544,0.663292221891153,0.421508490490711,0.418642116534749,0.237076166973632,0.362664647446725,0.313870891702703,0.673056706752311,0.640794028043229,0.58978377380782,0.586822132071031,0.516393349293472,0.461392057959957,0.40464218823807,0.231872936232751,0.344031427358075,0.258769775683273,0.256890795669362,0.381054995893833,0.54350404929849,0.445615868749673,0.530070998409692,0.423523238148561,0.353607061816704,0.428090901963187,0.563679133997603,0.526517612836526,0.49113988110757,0.236900549550001,0.381040293998353,0.329545981360475,0.621912443806014,0.47529786021154,0.676592504727741,0.638475152513658,0.47851892446569,0.560937329663675,0.431771968437635,0.336434634033204,0.307135169123924,0.358248092277192,0.347776192546914,0.363030186533283,0.449375267051948,0.337638169954446,0.42325198534535,0.631852779467888,0.437525843357067,0.339501205604446],[96,94,96,106,104,106,96,94,96,96,94,96,96,94,96,106,104,106,96,94,96,96,94,96,96,94,96,106,104,106,96,94,96,96,94,96,96,94,96,106,104,106,96,94,96,96,94,96,96,94,96,106,104,106,96,94,96,96,94,96,96,94,96,106,104,106,96,94,96,96,94,96],[0.8125,0.723404255319149,0.78125,0.783018867924528,0.740384615384615,0.735849056603774,0.802083333333333,0.734042553191489,0.802083333333333,0.697916666666667,0.75531914893617,0.8125,0.822916666666667,0.75531914893617,0.791666666666667,0.745283018867924,0.721153846153846,0.773584905660377,0.8125,0.74468085106383,0.770833333333333,0.71875,0.765957446808511,0.802083333333333,0.833333333333333,0.776595744680851,0.84375,0.820754716981132,0.740384615384615,0.745283018867924,0.84375,0.808510638297872,0.78125,0.75,0.797872340425532,0.78125,0.791666666666667,0.787234042553192,0.84375,0.839622641509434,0.865384615384615,0.839622641509434,0.71875,0.861702127659574,0.854166666666667,0.770833333333333,0.819148936170213,0.916666666666667,0.8125,0.74468085106383,0.78125,0.773584905660377,0.721153846153846,0.792452830188679,0.729166666666667,0.776595744680851,0.78125,0.708333333333333,0.787234042553192,0.791666666666667,0.75,0.734042553191489,0.760416666666667,0.811320754716981,0.730769230769231,0.80188679245283,0.791666666666667,0.74468085106383,0.78125,0.697916666666667,0.734042553191489,0.739583333333333],[0.768556701030928,0.742268041237113,0.762886597938144,0.814141414141414,0.797979797979798,0.732323232323232,0.742268041237113,0.77319587628866,0.762886597938144,0.587628865979381,0.74639175257732,0.795876288659794,0.814705882352941,0.782352941176471,0.759803921568627,0.783980582524272,0.766504854368932,0.75631067961165,0.758823529411765,0.784313725490196,0.774509803921569,0.595588235294118,0.776960784313726,0.776960784313726,0.806796116504854,0.771844660194175,0.786407766990291,0.816019417475728,0.79126213592233,0.766990291262136,0.725728155339806,0.737864077669903,0.628640776699029,0.533980582524272,0.728155339805825,0.638349514563107,0.74390243902439,0.853658536585366,0.802439024390244,0.914634146341463,0.853658536585366,0.839024390243902,0.560975609756098,0.835365853658537,0.75,0.664634146341463,0.833536585365854,0.826829268292683,0.84375,0.806770833333333,0.669270833333333,0.873958333333333,0.830729166666667,0.863541666666667,0.54375,0.808333333333333,0.674479166666667,0.463541666666667,0.708854166666668,0.682291666666667,0.701030927835052,0.822680412371134,0.783505154639175,0.960714285714286,0.826530612244898,0.795918367346939,0.756701030927835,0.793814432989691,0.783505154639175,0.646907216494845,0.805154639175258,0.77319587628866]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared_bok_choi<\/th>\n      <th>N_bok_choi<\/th>\n      <th>covers_bok_choi<\/th>\n      <th>width_bok_choi<\/th>\n      <th>cvRsquared_kale<\/th>\n      <th>N_kale<\/th>\n      <th>covers_kale<\/th>\n      <th>width_kale<\/th>\n      <th>cvRsquared_lettuce<\/th>\n      <th>N_lettuce<\/th>\n      <th>covers_lettuce<\/th>\n      <th>width_lettuce<\/th>\n      <th>cvRsquared_swiss_chard<\/th>\n      <th>N_swiss_chard<\/th>\n      <th>covers_swiss_chard<\/th>\n      <th>width_swiss_chard<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"dom":"Bfrtipl","lengthMenu":[7,21,42,84],"buttons":["colvis"],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[7]; $(this.api().cell(row, 7).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[11]; $(this.api().cell(row, 11).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[15]; $(this.api().cell(row, 15).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->

## Fit Plots For Nutrients { .tabset }

### Antioxidants { .tabset }

#### kale, lettuce { .tabset }

##### farmerWithoutSoilModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Antioxidants.](./graphics/fitPlots/Antioxidants-Root-consumerModels-fitPlot.png)

### Polyphenols { .tabset }

#### kale, lettuce { .tabset }

##### farmerWithoutSoilModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }

##### farmerWithSoilModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-farmerWithSoilModels-fitPlot.png)

##### farmerWithoutSoilModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Polyphenols.](./graphics/fitPlots/Polyphenols-Root-consumerModels-fitPlot.png)


### Brix { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for Brix.](./graphics/fitPlots/Brix-Root-consumerModels-fitPlot.png)



### BQI { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for BQI.](./graphics/fitPlots/BQI-Root-consumerModels-fitPlot.png)

## Importance for Nutrients { .tabset }

### Bok Choi { .tabset }

#### farmerWithoutSoilModels

![importance plog for bok_choi.](./graphics/importancePlots/allImportanceRegressions-Bok_choi-Farmerwithoutsoilmodels.png )

#### farmerConsumerModels

![importance plog for bok_choi.](./graphics/importancePlots/allImportanceRegressions-Bok_choi-Farmerconsumermodels.png )

#### consumerModels

![importance plog for bok_choi.](./graphics/importancePlots/allImportanceRegressions-Bok_choi-Consumermodels.png )


### Kale { .tabset }


#### farmerWithoutSoilModels

![importance plog for kale.](./graphics/importancePlots/allImportanceRegressions-Kale-Farmerwithoutsoilmodels.png )

#### farmerConsumerModels

![importance plog for kale.](./graphics/importancePlots/allImportanceRegressions-Kale-Farmerconsumermodels.png )

#### consumerModels

![importance plog for kale.](./graphics/importancePlots/allImportanceRegressions-Kale-Consumermodels.png )


### Lettuce { .tabset }

#### farmerWithoutSoilModels

![importance plog for lettuce.](./graphics/importancePlots/allImportanceRegressions-Lettuce-Farmerwithoutsoilmodels.png )

#### farmerConsumerModels

![importance plog for lettuce.](./graphics/importancePlots/allImportanceRegressions-Lettuce-Farmerconsumermodels.png )

#### consumerModels

![importance plog for lettuce.](./graphics/importancePlots/allImportanceRegressions-Lettuce-Consumermodels.png )


### Swiss Chard { .tabset }

#### farmerWithoutSoilModels

![importance plog for swiss_chard.](./graphics/importancePlots/allImportanceRegressions-Swiss_chard-Farmerwithoutsoilmodels.png )

#### farmerConsumerModels

![importance plog for swiss_chard.](./graphics/importancePlots/allImportanceRegressions-Swiss_chard-Farmerconsumermodels.png )

#### consumerModels

![importance plog for swiss_chard.](./graphics/importancePlots/allImportanceRegressions-Swiss_chard-Consumermodels.png )


## Importance for Minerals { .tabset }

### Bok Choi { .tabset }


#### farmerWithoutSoilModels

![importance plog for bok_choi.](./graphics/importancePlots/allImportanceRegressions-Bok_choi-FarmerwithoutsoilmodelsMinerals.png )

#### farmerConsumerModels

![importance plog for bok_choi.](./graphics/importancePlots/allImportanceRegressions-Bok_choi-FarmerconsumermodelsMinerals.png )

#### consumerModels

![importance plog for bok_choi.](./graphics/importancePlots/allImportanceRegressions-Bok_choi-ConsumermodelsMinerals.png )


### Kale { .tabset }


#### farmerWithoutSoilModels

![importance plog for kale.](./graphics/importancePlots/allImportanceRegressions-Kale-FarmerwithoutsoilmodelsMinerals.png )

#### farmerConsumerModels

![importance plog for kale.](./graphics/importancePlots/allImportanceRegressions-Kale-FarmerconsumermodelsMinerals.png )

#### consumerModels

![importance plog for kale.](./graphics/importancePlots/allImportanceRegressions-Kale-ConsumermodelsMinerals.png )


### Lettuce { .tabset }

#### farmerWithoutSoilModels

![importance plog for lettuce.](./graphics/importancePlots/allImportanceRegressions-Lettuce-FarmerwithoutsoilmodelsMinerals.png )

#### farmerConsumerModels

![importance plog for lettuce.](./graphics/importancePlots/allImportanceRegressions-Lettuce-FarmerconsumermodelsMinerals.png )

#### consumerModels

![importance plog for lettuce.](./graphics/importancePlots/allImportanceRegressions-Lettuce-ConsumermodelsMinerals.png )


### Swiss Chard { .tabset }

#### farmerWithoutSoilModels

![importance plog for swiss_chard.](./graphics/importancePlots/allImportanceRegressions-Swiss_chard-FarmerwithoutsoilmodelsMinerals.png )

#### farmerConsumerModels

![importance plog for swiss_chard.](./graphics/importancePlots/allImportanceRegressions-Swiss_chard-FarmerconsumermodelsMinerals.png )

#### consumerModels

![importance plog for swiss_chard.](./graphics/importancePlots/allImportanceRegressions-Swiss_chard-ConsumermodelsMinerals.png )


## Fit Plots for Minerals { .tabset }

### Ca { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Ca_Percentile.](./graphics/fitPlots/produce_xrf_Ca_Percentile-Root-consumerModels-fitPlot.png)



### Fe { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }

Groups )[1] & cropGroup == "Root" ) %>% .[["plotPath"]]`) -->

##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Fe_Percentile.](./graphics/fitPlots/produce_xrf_Fe_Percentile-Root-consumerModels-fitPlot.png)


### K { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }

Groups )[1] & cropGroup == "Root" ) %>% .[["plotPath"]]`) -->

##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_K_Percentile.](./graphics/fitPlots/produce_xrf_K_Percentile-Root-consumerModels-fitPlot.png)


### Mg { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Mg_Percentile.](./graphics/fitPlots/produce_xrf_Mg_Percentile-Root-consumerModels-fitPlot.png)


### S { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_S_Percentile.](./graphics/fitPlots/produce_xrf_S_Percentile-Root-consumerModels-fitPlot.png)


### Zn { .tabset }

#### kale, lettuce { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Fruit-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Fruit-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Fruit-consumerModels-fitPlot.png)


#### bok_choi, mustard_greens, swiss_chard { .tabset }


##### farmerWithoutSoilModels

![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Root-farmerWithoutSoilModels-fitPlot.png)

##### farmerConsumerModels

![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Root-farmerConsumerModels-fitPlot.png)

##### consumerModels

![Grain Fitplots for produce_xrf_Zn_Percentile.](./graphics/fitPlots/produce_xrf_Zn_Percentile-Root-consumerModels-fitPlot.png)



## Random Forest Classification of The Percentile on the Explained Variable


Two targets are generated for each explained variable (Antioxidants, Polyphenols, Brix). The objective is predicting the quantile were the sample contents belong. Instead of using each quantile, the central ones are meged into a center category.

* Quartiles is three categories, *lower* for the first quartile, *center* for the second and third and *higher* for the fourth.
* Quintiles is also three categories, with *center* composed of the second, third and fourth quintiles.




<!--html_preserve--><div id="htmlwidget-c641e7a02783d2bfa080" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-c641e7a02783d2bfa080">{"x":{"filter":"none","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84"],["antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","antiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","bqiQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","brixQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile","polyQuartile"],["c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who","c-min","c-min-nir-who","c-min-vis-who","c-no","c-no-nir-who","c-no-vis-who","f-c-med","f-c-med-nir-pro","f-c-med-nir-who","f-c-med-vis-pro","f-c-med-vis-who","f-max","f-max-nir-pro","f-max-nir-who","f-max-soil","f-max-soil-nir-pro","f-max-soil-nir-who","f-max-soil-vis-pro","f-max-soil-vis-who","f-max-vis-pro","f-max-vis-who"],[0.624201169201169,0.702344877344877,0.66489898989899,0.480839002267574,0.584100529100529,0.575772005772006,0.632222222222222,0.668636363636364,0.741212121212121,0.646585755514327,0.67974987974988,0.684218559218559,0.624444444444444,0.666118326118326,0,0,0,0,0,0.69462481962482,0.619047619047619,0.536660561660562,0.583143047428762,0.494361590790162,0.493308913308913,0.442154882154882,0.469603174603175,0.572751322751323,0.517619047619048,0.498257725180802,0.573262293262293,0.517913832199546,0.561666666666667,0.5870670995671,0.495165945165945,0,0,0,0,0,0.531613756613757,0.486262626262626,0.598571428571429,0.631037414965986,0.609206349206349,0.412039072039072,0.508982683982684,0.461851851851852,0.581031746031746,0.61008658008658,0.66952380952381,0.589219576719577,0.656747049247049,0.528253968253968,0.540661375661376,0.643452380952381,0,0,0,0,0,0.565793650793651,0.694505494505494,0.572380952380952,0.518068783068783,0.504903628117914,0.474491434491434,0.503155363155363,0.509563492063492,0.595132275132275,0.584444444444444,0.541562881562882,0.58382807668522,0.541969696969697,0.602912960055817,0.622395382395382,0.503610278610279,0,0,0,0,0,0.512000962000962,0.543750530515236],[93,93,93,103,103,103,93,85,93,85,93,93,85,93,0,0,0,0,0,85,93,84,84,84,94,94,94,84,84,84,84,84,84,84,84,0,0,0,0,0,84,84,86,86,86,96,96,96,86,86,86,86,86,86,86,86,0,0,0,0,0,86,86,94,94,94,104,104,104,94,86,94,86,94,94,86,94,0,0,0,0,0,86,94],[0.600519268313386,0.458857808857809,0.527483861168072,0.543443413311834,0.480191452853992,0.477024415786026,0.600913715913716,0.58,0.541904761904762,0.61463998963999,0.56456524956525,0.554318829318829,0.576844636844637,0.586138589079766,0,0,0,0,0,0.609814814814815,0.560919866453922,0.472374252731396,0.568730158730159,0.515541125541126,0.43968253968254,0.567000962000962,0.551295093795094,0.627630471380471,0.485714285714286,0.528996101364522,0.535238095238095,0.516117216117216,0.590793650793651,0.598056758056758,0.59973544973545,0,0,0,0,0,0.623930143930144,0.576772486772487,0.606866423337012,0.709470899470899,0.719377932009511,0.535591542959964,0.673856104908736,0.715269697622639,0.629432419432419,0.658412698412698,0.653632315397021,0.722008745223031,0.640731120731121,0.644379509379509,0.696587301587302,0.722424242424242,0,0,0,0,0,0.638650793650794,0.656887755102041,0.643878713878714,0.616940466940467,0.607577607577608,0.507232266644031,0.578385525150231,0.599978780478781,0.720544662309368,0.603659147869674,0.629558998506367,0.658147865206689,0.622039257039257,0.720820105820106,0.60979797979798,0.620319865319865,0,0,0,0,0,0.651528286528287,0.620500279447648],[156,156,156,189,189,189,156,118,156,118,156,156,118,156,0,0,0,0,0,118,156,88,88,88,110,110,110,88,88,88,88,88,88,88,88,0,0,0,0,0,88,88,118,118,118,151,151,151,118,118,118,118,118,118,118,118,0,0,0,0,0,118,118,153,153,153,189,189,189,153,115,153,115,153,153,115,153,0,0,0,0,0,115,153],[0.541074234407568,0.633763556704733,0.580770803270803,0.578487816531295,0.557779072779073,0.557192970428265,0.578784265843089,0.622805435305435,0.607946682946683,0.604086605116017,0.622649572649573,0.558811743811744,0.610367317867318,0.612444748234222,0,0,0,0,0,0.637267177267177,0.582467532467532,0.697551707551708,0.731374643874644,0.736577311577312,0.549594436065024,0.658015840368782,0.643388370888371,0.750954878454879,0.759486461251167,0.696263366263366,0.73773569023569,0.716379268879269,0.747964954435543,0.739411144411144,0.756654641654642,0,0,0,0,0,0.758096903096903,0.73004588004588,0.674119769119769,0.597839067250832,0.558512413512414,0.533602856838151,0.513776749566223,0.567070707070707,0.6968303591833,0.668107448107448,0.644099947671376,0.639743404743405,0.611249520460047,0.642726902726903,0.652580135722551,0.608029655529655,0,0,0,0,0,0.670056446821153,0.647065342065342,0.65487668277142,0.68626338573707,0.653192733192733,0.558638797849324,0.658914457861826,0.635390496443128,0.678835140893964,0.695479242979243,0.680973188032012,0.696286676286676,0.715833803877282,0.726616161616162,0.688982683982684,0.691454841454841,0,0,0,0,0,0.699310411810412,0.653537758537759],[149,149,149,166,166,166,149,145,149,145,149,149,145,149,0,0,0,0,0,145,149,145,145,145,158,158,158,145,145,145,145,145,145,145,145,0,0,0,0,0,145,145,145,145,145,162,162,162,145,145,145,145,145,145,145,145,0,0,0,0,0,145,145,149,149,149,166,166,166,149,145,149,145,149,149,145,149,0,0,0,0,0,145,149],[0.637833092833093,0.622847522847523,0.673653198653199,0.603484848484849,0.644497354497354,0.624497354497354,0.629354904354904,0.668136123136123,0.633054353054353,0.640521885521885,0.672888407888408,0.627311207311207,0.665334295334295,0.67926859632742,0,0,0,0,0,0.687089947089947,0.638645983645984,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.583514104942676,0.603318903318903,0.57448717948718,0.49471102971103,0.59,0.527445887445887,0.590873015873016,0.621216931216931,0.572698412698413,0.687089947089947,0.593756613756614,0.559047619047619,0.677527657527658,0.596878306878307,0,0,0,0,0,0.633015873015873,0.536565101565102,0.617082732082732,0.557184112184112,0.554064454064454,0.527189662189662,0.586239316239316,0.535555555555556,0.625742775742776,0.611587301587302,0.589808802308802,0.585846560846561,0.631599326599327,0.622480482480482,0.589047619047619,0.571825396825397,0,0,0,0,0,0.567606467606468,0.613534243534244],[108,108,108,112,112,112,108,101,108,101,108,108,101,108,0,0,0,0,0,101,108,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,101,101,101,105,105,105,101,101,101,101,101,101,101,101,0,0,0,0,0,101,101,108,108,108,112,112,112,108,101,108,101,108,108,101,108,0,0,0,0,0,101,108],[0.50962962962963,0.505343915343915,0.543629431570608,0.534165834165834,0.480788840788841,0.44984126984127,0.648068783068783,0.624047619047619,0.559259259259259,0.626296296296296,0.52515873015873,0.668595848595849,0.598301698301698,0.57218253968254,0.669444444444444,0,0.694074074074074,0,0.665800865800866,0.573164983164983,0.590343915343915,0.556195286195286,0.497089947089947,0.560141895141895,0.584126984126984,0.536185666185666,0.482951307951308,0.549285714285714,0.581464646464646,0.634485699485699,0.511464646464647,0.56513431013431,0.56042328042328,0.59025493025493,0.576269841269841,0,0,0,0,0,0.530873015873016,0.594771241830065,0.500671185539607,0.530793650793651,0.561404864976293,0.541868686868687,0.432395382395382,0.44996151996152,0.564390142021721,0.535209235209235,0.576626984126984,0.513968253968254,0.507794784580499,0.593888888888889,0.515678210678211,0.51482683982684,0,0,0,0,0,0.465531135531136,0.51,0.596681096681097,0.495525988173047,0.543101343101343,0.435473785473785,0.49494708994709,0.449365079365079,0.613857253857254,0.57506734006734,0.558852813852814,0.580026455026455,0.566624486624487,0.591573056573057,0.573669108669109,0.557857142857143,0.505952380952381,0,0.475,0,0.404603174603175,0.587407407407407,0.508962148962149],[97,97,97,107,107,107,97,86,97,86,97,97,86,97,50,0,50,0,50,86,97,85,85,85,91,91,91,85,85,85,85,85,85,85,85,0,0,0,0,0,85,85,86,86,86,92,92,92,86,86,86,86,86,86,86,86,0,0,0,0,0,86,86,97,97,97,107,107,107,97,86,97,86,97,97,86,97,50,0,50,0,50,86,97]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>Accuracy_bok_choi<\/th>\n      <th>N_bok_choi<\/th>\n      <th>Accuracy_kale<\/th>\n      <th>N_kale<\/th>\n      <th>Accuracy_lettuce<\/th>\n      <th>N_lettuce<\/th>\n      <th>Accuracy_mustard_greens<\/th>\n      <th>N_mustard_greens<\/th>\n      <th>Accuracy_swiss_chard<\/th>\n      <th>N_swiss_chard<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[7,21,42,84],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6,7,8,9,10,11,12]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[5]; $(this.api().cell(row, 5).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[7]; $(this.api().cell(row, 7).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[9]; $(this.api().cell(row, 9).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\nvar value=data[11]; $(this.api().cell(row, 11).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve-->

### Confusion Matrix for a Succesful Model



# Clusterization for Climate Regions and Soil Suborders { .tabset }


### Bok_Choi { .tabset } 

#### by Climate Region

![](greensDataAnalysis_files/figure-html/unnamed-chunk-1-1.png)<!-- -->

#### by Soil Suborder

![](greensDataAnalysis_files/figure-html/unnamed-chunk-2-1.png)<!-- -->

### Kale { .tabset } 

#### by Climate Region

![](greensDataAnalysis_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

#### by Soil Suborder

![](greensDataAnalysis_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

### Lettuce { .tabset } 

#### by Climate Region

![](greensDataAnalysis_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

#### by Soil Suborder

![](greensDataAnalysis_files/figure-html/unnamed-chunk-6-1.png)<!-- -->

### Mustard_Greens { .tabset } 

#### by Climate Region

![](greensDataAnalysis_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

#### by Soil Suborder

![](greensDataAnalysis_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

### Swiss_Chard { .tabset } 

#### by Climate Region

![](greensDataAnalysis_files/figure-html/unnamed-chunk-9-1.png)<!-- -->

#### by Soil Suborder

![](greensDataAnalysis_files/figure-html/unnamed-chunk-10-1.png)<!-- -->




# Agregation of Farm Practices Analysis Over Crops

  In this section, we'll try to find meaningful results for farm practices **across** crops.
  Our strategy to achieve a meaningful comparison is replacing on each point the actual variable value by the crop's percentile reached. The goal of this study is checking if a practices is able to push each crop far from its *median*.
  This means that if a tomatto that is bigger than 60% of all other tomato observations has a value of 0.1 for a certain variable and for Kale you need to reach 10 to be above 60% of all subjects, we are giving the same score to a a 0.1 tomato sample and a kale sample with a value of 10. We are measuring the relative scarcity of a value among it's crop peers, rather than the absolute value.



## Agregation by Percentile Transformation


Instead of percentiles, we'll directly use the *Empirical Cumulative Distribution Function*.




<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Shifts over percentile across crops</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> sourceBucket </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.5413547 </td>
   <td style="text-align:right;"> 0.5906897 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.12,0.3] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.30 </td>
   <td style="text-align:right;"> 0.0074869 </td>
   <td style="text-align:right;"> 0.2049876 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4726601 </td>
   <td style="text-align:right;"> 0.3175862 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.12,0.12] </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.0090679 </td>
   <td style="text-align:right;"> 0.0579858 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4917297 </td>
   <td style="text-align:right;"> 0.5380818 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.13,0.38] </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 0.0245330 </td>
   <td style="text-align:right;"> 0.2454872 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.6178325 </td>
   <td style="text-align:right;"> 0.8258621 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.07,0.5] </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.0311418 </td>
   <td style="text-align:right;"> 0.2748151 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.8902335 </td>
   <td style="text-align:right;"> 0.8742138 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.07,-0.03] </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0323032 </td>
   <td style="text-align:right;"> -0.0258983 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.6381281 </td>
   <td style="text-align:right;"> 0.6731034 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.11,0.14] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.0409518 </td>
   <td style="text-align:right;"> 0.0516780 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.5906142 </td>
   <td style="text-align:right;"> 0.6551481 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.11,0.12] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.0455696 </td>
   <td style="text-align:right;"> 0.0465977 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_V_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4673420 </td>
   <td style="text-align:right;"> 0.3483780 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.36,-0.06] </td>
   <td style="text-align:right;"> -0.36 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0567387 </td>
   <td style="text-align:right;"> -0.2488602 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.4859213 </td>
   <td style="text-align:right;"> 0.4598025 </td>
   <td style="text-align:right;"> 55 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.28,-0.03] </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0605055 </td>
   <td style="text-align:right;"> -0.1291048 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.7209923 </td>
   <td style="text-align:right;"> 0.6886792 </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.15,0] </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0613837 </td>
   <td style="text-align:right;"> -0.0457276 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.5295616 </td>
   <td style="text-align:right;"> 0.4785714 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.15,0.03] </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.0711351 </td>
   <td style="text-align:right;"> -0.0609060 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.6085222 </td>
   <td style="text-align:right;"> 0.6417241 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.05,0.13] </td>
   <td style="text-align:right;"> -0.05 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0821914 </td>
   <td style="text-align:right;"> 0.0459024 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Biodynamic </td>
   <td style="text-align:right;"> 0.6020453 </td>
   <td style="text-align:right;"> 0.6931217 </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.07,0.23] </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.0934380 </td>
   <td style="text-align:right;"> 0.1499726 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.9051271 </td>
   <td style="text-align:right;"> 0.8750000 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0026040 </td>
   <td style="text-align:right;"> -0.0252210 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.6939822 </td>
   <td style="text-align:right;"> 0.7074639 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.17,-0.01] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.0189324 </td>
   <td style="text-align:right;"> -0.0932874 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_size </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 7.3750000 </td>
   <td style="text-align:right;"> 3.0000000 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0194745 </td>
   <td style="text-align:right;"> -0.0000503 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.7205411 </td>
   <td style="text-align:right;"> 0.8356841 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.06,0.05] </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.0532019 </td>
   <td style="text-align:right;"> 0.0091917 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.4570030 </td>
   <td style="text-align:right;"> 0.4358251 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.04,0.24] </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.0586478 </td>
   <td style="text-align:right;"> 0.1205196 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.5519225 </td>
   <td style="text-align:right;"> 0.6520270 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.04,0.16] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0599023 </td>
   <td style="text-align:right;"> 0.0542127 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.4237295 </td>
   <td style="text-align:right;"> 0.2016925 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.55,-0.01] </td>
   <td style="text-align:right;"> -0.55 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.0765651 </td>
   <td style="text-align:right;"> -0.1388678 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.5503640 </td>
   <td style="text-align:right;"> 0.6113793 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.21,0.12] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.0780656 </td>
   <td style="text-align:right;"> -0.0710895 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.2950958 </td>
   <td style="text-align:right;"> 0.2765517 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.12,0.06] </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0874088 </td>
   <td style="text-align:right;"> -0.0379866 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Biological </td>
   <td style="text-align:right;"> 0.3209386 </td>
   <td style="text-align:right;"> 0.2884344 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.02,0.17] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.0877583 </td>
   <td style="text-align:right;"> 0.0344654 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_size </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 65.5609756 </td>
   <td style="text-align:right;"> 57.5000000 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,54.5] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 54.50 </td>
   <td style="text-align:right;"> 0.0008635 </td>
   <td style="text-align:right;"> 9.4999881 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.7816709 </td>
   <td style="text-align:right;"> 0.8243243 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.07,0] </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0152631 </td>
   <td style="text-align:right;"> -0.0000499 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.9200487 </td>
   <td style="text-align:right;"> 0.9423077 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.02,0] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0168003 </td>
   <td style="text-align:right;"> -0.0000064 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.4302307 </td>
   <td style="text-align:right;"> 0.3554510 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.16,-0.01] </td>
   <td style="text-align:right;"> -0.16 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.0192482 </td>
   <td style="text-align:right;"> -0.0786910 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6128731 </td>
   <td style="text-align:right;"> 0.7919605 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.16,0.48] </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.0245532 </td>
   <td style="text-align:right;"> 0.3525897 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6143590 </td>
   <td style="text-align:right;"> 0.8193103 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.2,0.52] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.52 </td>
   <td style="text-align:right;"> 0.0264212 </td>
   <td style="text-align:right;"> 0.3978882 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6237182 </td>
   <td style="text-align:right;"> 0.7291961 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.2,0.46] </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.46 </td>
   <td style="text-align:right;"> 0.0299439 </td>
   <td style="text-align:right;"> 0.3216243 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6958090 </td>
   <td style="text-align:right;"> 0.7834483 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.06,0.25] </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.0553367 </td>
   <td style="text-align:right;"> 0.1503433 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.3780217 </td>
   <td style="text-align:right;"> 0.2934783 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.26,-0.13] </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.0553517 </td>
   <td style="text-align:right;"> -0.1978541 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.3780217 </td>
   <td style="text-align:right;"> 0.2934783 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.26,-0.13] </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.0553517 </td>
   <td style="text-align:right;"> -0.1978541 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.7665553 </td>
   <td style="text-align:right;"> 0.7928571 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.01,0.04] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.0561066 </td>
   <td style="text-align:right;"> 0.0000785 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.4279614 </td>
   <td style="text-align:right;"> 0.3976418 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.11,-0.04] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.0614941 </td>
   <td style="text-align:right;"> -0.0745003 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.6203714 </td>
   <td style="text-align:right;"> 0.6172414 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.09,0.11] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.0646113 </td>
   <td style="text-align:right;"> -0.0138125 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mg_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.3496636 </td>
   <td style="text-align:right;"> 0.2823413 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.21,-0.07] </td>
   <td style="text-align:right;"> -0.21 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.0741073 </td>
   <td style="text-align:right;"> -0.1386583 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQINutrients </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.9974302 </td>
   <td style="text-align:right;"> 1.0708791 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0.21] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.21 </td>
   <td style="text-align:right;"> 0.0786134 </td>
   <td style="text-align:right;"> 0.1048533 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5909541 </td>
   <td style="text-align:right;"> 0.6784203 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.11,0.16] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.0822151 </td>
   <td style="text-align:right;"> 0.0000571 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.7863241 </td>
   <td style="text-align:right;"> 0.8076923 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0.01] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.0831902 </td>
   <td style="text-align:right;"> -0.0000162 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Certified Organic </td>
   <td style="text-align:right;"> 0.5280602 </td>
   <td style="text-align:right;"> 0.5099518 </td>
   <td style="text-align:right;"> 72 </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.13,0] </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0897105 </td>
   <td style="text-align:right;"> -0.0587987 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> 0.9422703 </td>
   <td style="text-align:right;"> 0.9753086 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0003532 </td>
   <td style="text-align:right;"> 0.0000625 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> 0.7543297 </td>
   <td style="text-align:right;"> 0.9753086 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0.03] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.0006744 </td>
   <td style="text-align:right;"> 0.0000180 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Hydroponic </td>
   <td style="text-align:right;"> 0.4233886 </td>
   <td style="text-align:right;"> 0.2428571 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.54,-0.17] </td>
   <td style="text-align:right;"> -0.54 </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.0403570 </td>
   <td style="text-align:right;"> -0.3165300 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.9114311 </td>
   <td style="text-align:right;"> 0.8750000 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000009 </td>
   <td style="text-align:right;"> -0.0000303 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.6449896 </td>
   <td style="text-align:right;"> 0.6292135 </td>
   <td style="text-align:right;"> 76 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.19,-0.09] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.0025103 </td>
   <td style="text-align:right;"> -0.1564925 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_size </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 10.7037037 </td>
   <td style="text-align:right;"> 3.0000000 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-2,0] </td>
   <td style="text-align:right;"> -2.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0049434 </td>
   <td style="text-align:right;"> -1.9999721 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.6453632 </td>
   <td style="text-align:right;"> 0.6917241 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0.09] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.09 </td>
   <td style="text-align:right;"> 0.0075316 </td>
   <td style="text-align:right;"> 0.0283928 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5903155 </td>
   <td style="text-align:right;"> 0.6530324 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.04,0.1] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.0219654 </td>
   <td style="text-align:right;"> 0.0317878 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4627185 </td>
   <td style="text-align:right;"> 0.3293371 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.5,-0.14] </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.0242289 </td>
   <td style="text-align:right;"> -0.2800504 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_K_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5764294 </td>
   <td style="text-align:right;"> 0.6802002 </td>
   <td style="text-align:right;"> 76 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.12,0.27] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.0300475 </td>
   <td style="text-align:right;"> 0.1953989 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5909555 </td>
   <td style="text-align:right;"> 0.6878307 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.11,0.24] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.0379651 </td>
   <td style="text-align:right;"> 0.1786089 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.6400324 </td>
   <td style="text-align:right;"> 0.7301587 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.14,0.26] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.0388810 </td>
   <td style="text-align:right;"> 0.2020882 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.3546061 </td>
   <td style="text-align:right;"> 0.3258110 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0.12] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.0391307 </td>
   <td style="text-align:right;"> 0.0366720 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5189696 </td>
   <td style="text-align:right;"> 0.5042313 </td>
   <td style="text-align:right;"> 89 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.09,0.11] </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.0428361 </td>
   <td style="text-align:right;"> 0.0289543 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.5169626 </td>
   <td style="text-align:right;"> 0.5582759 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.01,0.25] </td>
   <td style="text-align:right;"> 0.01 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.0456010 </td>
   <td style="text-align:right;"> 0.1096663 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.6733762 </td>
   <td style="text-align:right;"> 0.6886792 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.14,0] </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0656989 </td>
   <td style="text-align:right;"> -0.0507941 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQIBrix </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4016406 </td>
   <td style="text-align:right;"> 0.3271605 </td>
   <td style="text-align:right;"> 71 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.24,-0.1] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.0784643 </td>
   <td style="text-align:right;"> -0.1663461 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Brix_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4016406 </td>
   <td style="text-align:right;"> 0.3271605 </td>
   <td style="text-align:right;"> 71 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.24,-0.1] </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.0784643 </td>
   <td style="text-align:right;"> -0.1663461 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.4391416 </td>
   <td style="text-align:right;"> 0.4210345 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.18,0.06] </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0958343 </td>
   <td style="text-align:right;"> -0.0206625 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Irrigation </td>
   <td style="text-align:right;"> 0.3829054 </td>
   <td style="text-align:right;"> 0.3903448 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.05,0.23] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.0964292 </td>
   <td style="text-align:right;"> 0.1449443 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.9116979 </td>
   <td style="text-align:right;"> 0.8742138 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0014819 </td>
   <td style="text-align:right;"> -0.0000144 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.3558250 </td>
   <td style="text-align:right;"> 0.2493186 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.38,-0.13] </td>
   <td style="text-align:right;"> -0.38 </td>
   <td style="text-align:right;"> -0.13 </td>
   <td style="text-align:right;"> 0.0759301 </td>
   <td style="text-align:right;"> -0.2556902 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Local </td>
   <td style="text-align:right;"> 0.4942104 </td>
   <td style="text-align:right;"> 0.5000000 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.3,-0.14] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> -0.14 </td>
   <td style="text-align:right;"> 0.0803211 </td>
   <td style="text-align:right;"> -0.2297071 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.9015886 </td>
   <td style="text-align:right;"> 0.8742138 </td>
   <td style="text-align:right;"> 143 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000007 </td>
   <td style="text-align:right;"> -0.0251090 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.6336440 </td>
   <td style="text-align:right;"> 0.6292135 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.19,-0.12] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.0027351 </td>
   <td style="text-align:right;"> -0.1631964 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.6490100 </td>
   <td style="text-align:right;"> 0.6986207 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0.11] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.0123672 </td>
   <td style="text-align:right;"> 0.0420821 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4829413 </td>
   <td style="text-align:right;"> 0.4203103 </td>
   <td style="text-align:right;"> 148 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.26,-0.03] </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0224394 </td>
   <td style="text-align:right;"> -0.1233584 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.6324420 </td>
   <td style="text-align:right;"> 0.7142857 </td>
   <td style="text-align:right;"> 144 </td>
   <td style="text-align:right;"> 52 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.14,0.24] </td>
   <td style="text-align:right;"> 0.14 </td>
   <td style="text-align:right;"> 0.24 </td>
   <td style="text-align:right;"> 0.0236065 </td>
   <td style="text-align:right;"> 0.1872556 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.3936931 </td>
   <td style="text-align:right;"> 0.3561354 </td>
   <td style="text-align:right;"> 148 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.49,-0.26] </td>
   <td style="text-align:right;"> -0.49 </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> 0.0326627 </td>
   <td style="text-align:right;"> -0.3780359 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.6510502 </td>
   <td style="text-align:right;"> 0.7002821 </td>
   <td style="text-align:right;"> 148 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0.15] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.0432518 </td>
   <td style="text-align:right;"> 0.0669714 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4649440 </td>
   <td style="text-align:right;"> 0.4605078 </td>
   <td style="text-align:right;"> 148 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.1,0.33] </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.0481897 </td>
   <td style="text-align:right;"> 0.2107098 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4209344 </td>
   <td style="text-align:right;"> 0.3496552 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.5,-0.22] </td>
   <td style="text-align:right;"> -0.50 </td>
   <td style="text-align:right;"> -0.22 </td>
   <td style="text-align:right;"> 0.0651381 </td>
   <td style="text-align:right;"> -0.3578829 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.3880044 </td>
   <td style="text-align:right;"> 0.3749496 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.01,0.03] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.0652164 </td>
   <td style="text-align:right;"> 0.0098285 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI2 </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.5075850 </td>
   <td style="text-align:right;"> 0.4842972 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.02,0.04] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.0652164 </td>
   <td style="text-align:right;"> 0.0062407 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Nospray </td>
   <td style="text-align:right;"> 0.4641646 </td>
   <td style="text-align:right;"> 0.4296552 </td>
   <td style="text-align:right;"> 155 </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.15,0.1] </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.0673832 </td>
   <td style="text-align:right;"> -0.0220203 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.9062608 </td>
   <td style="text-align:right;"> 0.8742138 </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,0] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0000003 </td>
   <td style="text-align:right;"> -0.0251273 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6300971 </td>
   <td style="text-align:right;"> 0.6292135 </td>
   <td style="text-align:right;"> 86 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.19,-0.12] </td>
   <td style="text-align:right;"> -0.19 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.0049652 </td>
   <td style="text-align:right;"> -0.1631736 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6008401 </td>
   <td style="text-align:right;"> 0.6568966 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.08,0.07] </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.0073062 </td>
   <td style="text-align:right;"> 0.0165581 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_S_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5340251 </td>
   <td style="text-align:right;"> 0.5755172 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.11,0.33] </td>
   <td style="text-align:right;"> 0.11 </td>
   <td style="text-align:right;"> 0.33 </td>
   <td style="text-align:right;"> 0.0116736 </td>
   <td style="text-align:right;"> 0.2283123 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4700014 </td>
   <td style="text-align:right;"> 0.4134170 </td>
   <td style="text-align:right;"> 86 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.2,-0.07] </td>
   <td style="text-align:right;"> -0.20 </td>
   <td style="text-align:right;"> -0.07 </td>
   <td style="text-align:right;"> 0.0194400 </td>
   <td style="text-align:right;"> -0.1354610 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.5853654 </td>
   <td style="text-align:right;"> 0.6452750 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.06,0.1] </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.10 </td>
   <td style="text-align:right;"> 0.0298924 </td>
   <td style="text-align:right;"> 0.0267261 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3955298 </td>
   <td style="text-align:right;"> 0.2896552 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.55,-0.25] </td>
   <td style="text-align:right;"> -0.55 </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.0383701 </td>
   <td style="text-align:right;"> -0.4463794 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4971850 </td>
   <td style="text-align:right;"> 0.5696552 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.02,0.25] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.0398473 </td>
   <td style="text-align:right;"> 0.1066289 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_size </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 53.9181034 </td>
   <td style="text-align:right;"> 3.0000000 </td>
   <td style="text-align:right;"> 116 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-2,0] </td>
   <td style="text-align:right;"> -2.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0412393 </td>
   <td style="text-align:right;"> -0.0000289 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4589278 </td>
   <td style="text-align:right;"> 0.4139633 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.3,-0.06] </td>
   <td style="text-align:right;"> -0.30 </td>
   <td style="text-align:right;"> -0.06 </td>
   <td style="text-align:right;"> 0.0446413 </td>
   <td style="text-align:right;"> -0.1484886 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3835982 </td>
   <td style="text-align:right;"> 0.2757405 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.58,-0.24] </td>
   <td style="text-align:right;"> -0.58 </td>
   <td style="text-align:right;"> -0.24 </td>
   <td style="text-align:right;"> 0.0485409 </td>
   <td style="text-align:right;"> -0.4075352 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.3945420 </td>
   <td style="text-align:right;"> 0.3810762 </td>
   <td style="text-align:right;"> 61 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0.04] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.0523204 </td>
   <td style="text-align:right;"> 0.0177817 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ca_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4990282 </td>
   <td style="text-align:right;"> 0.4365517 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.11,0.13] </td>
   <td style="text-align:right;"> -0.11 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0609442 </td>
   <td style="text-align:right;"> 0.0232506 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4954910 </td>
   <td style="text-align:right;"> 0.4353173 </td>
   <td style="text-align:right;"> 86 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.12,0.02] </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.0783743 </td>
   <td style="text-align:right;"> -0.0513905 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.6654604 </td>
   <td style="text-align:right;"> 0.7354497 </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.17,0.28] </td>
   <td style="text-align:right;"> 0.17 </td>
   <td style="text-align:right;"> 0.28 </td>
   <td style="text-align:right;"> 0.0919567 </td>
   <td style="text-align:right;"> 0.2236268 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Organic </td>
   <td style="text-align:right;"> 0.4094743 </td>
   <td style="text-align:right;"> 0.3769394 </td>
   <td style="text-align:right;"> 104 </td>
   <td style="text-align:right;"> 41 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.02,0.2] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.20 </td>
   <td style="text-align:right;"> 0.0970610 </td>
   <td style="text-align:right;"> 0.1072693 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6496030 </td>
   <td style="text-align:right;"> 0.6292135 </td>
   <td style="text-align:right;"> 78 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.17,-0.09] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.0006933 </td>
   <td style="text-align:right;"> -0.1531545 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.8929236 </td>
   <td style="text-align:right;"> 0.8742138 </td>
   <td style="text-align:right;"> 92 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,-0.03] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0015635 </td>
   <td style="text-align:right;"> -0.0258997 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> farm_size </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 31.6651786 </td>
   <td style="text-align:right;"> 3.0000000 </td>
   <td style="text-align:right;"> 112 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-2,0] </td>
   <td style="text-align:right;"> -2.00 </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.0030034 </td>
   <td style="text-align:right;"> -0.0000327 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6359118 </td>
   <td style="text-align:right;"> 0.7383178 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.13,0.25] </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 0.0104182 </td>
   <td style="text-align:right;"> 0.1908393 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4125250 </td>
   <td style="text-align:right;"> 0.4224260 </td>
   <td style="text-align:right;"> 101 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.05,0.27] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.27 </td>
   <td style="text-align:right;"> 0.0231736 </td>
   <td style="text-align:right;"> 0.1594482 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Fe_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5031211 </td>
   <td style="text-align:right;"> 0.4386460 </td>
   <td style="text-align:right;"> 101 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.25,-0.01] </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.0321977 </td>
   <td style="text-align:right;"> -0.1425000 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Si_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4979799 </td>
   <td style="text-align:right;"> 0.4201737 </td>
   <td style="text-align:right;"> 78 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.12,0.02] </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.0329538 </td>
   <td style="text-align:right;"> -0.0499666 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> BQI </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3930361 </td>
   <td style="text-align:right;"> 0.3714521 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.01,0.04] </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> 0.0377475 </td>
   <td style="text-align:right;"> 0.0135640 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Na_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5576773 </td>
   <td style="text-align:right;"> 0.5379310 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.15,0.07] </td>
   <td style="text-align:right;"> -0.15 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.0517923 </td>
   <td style="text-align:right;"> -0.0276117 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Al_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4908601 </td>
   <td style="text-align:right;"> 0.4915374 </td>
   <td style="text-align:right;"> 101 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.26,-0.03] </td>
   <td style="text-align:right;"> -0.26 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0602311 </td>
   <td style="text-align:right;"> -0.1276939 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4958295 </td>
   <td style="text-align:right;"> 0.5389655 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0.26] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.26 </td>
   <td style="text-align:right;"> 0.0811708 </td>
   <td style="text-align:right;"> 0.1145382 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_Ti_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.5915290 </td>
   <td style="text-align:right;"> 0.5734483 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.1,0.08] </td>
   <td style="text-align:right;"> -0.10 </td>
   <td style="text-align:right;"> 0.08 </td>
   <td style="text-align:right;"> 0.0811708 </td>
   <td style="text-align:right;"> 0.0170100 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_P_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.6704099 </td>
   <td style="text-align:right;"> 0.7520690 </td>
   <td style="text-align:right;"> 106 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.05,0.23] </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.23 </td>
   <td style="text-align:right;"> 0.0830570 </td>
   <td style="text-align:right;"> 0.1468655 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Cr_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.4197377 </td>
   <td style="text-align:right;"> 0.4160790 </td>
   <td style="text-align:right;"> 101 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.46,-0.23] </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.0834730 </td>
   <td style="text-align:right;"> -0.3124454 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Ni_Percentile </td>
   <td style="text-align:left;"> Regenerative </td>
   <td style="text-align:right;"> 0.3589074 </td>
   <td style="text-align:right;"> 0.3152327 </td>
   <td style="text-align:right;"> 101 </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.04,0.13] </td>
   <td style="text-align:right;"> -0.04 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> 0.0901642 </td>
   <td style="text-align:right;"> 0.0480282 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Mo_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.8970655 </td>
   <td style="text-align:right;"> 0.8490566 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.03,-0.03] </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> -0.03 </td>
   <td style="text-align:right;"> 0.0046620 </td>
   <td style="text-align:right;"> -0.0259417 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> produce_xrf_Cu_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.5308230 </td>
   <td style="text-align:right;"> 0.4594595 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.33,-0.09] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> -0.09 </td>
   <td style="text-align:right;"> 0.0270627 </td>
   <td style="text-align:right;"> -0.2483548 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_10cm_xrf_Mn_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.4927598 </td>
   <td style="text-align:right;"> 0.5380818 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.15,0.38] </td>
   <td style="text-align:right;"> 0.15 </td>
   <td style="text-align:right;"> 0.38 </td>
   <td style="text-align:right;"> 0.0311577 </td>
   <td style="text-align:right;"> 0.2637168 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> soil_20cm_xrf_K_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.6385057 </td>
   <td style="text-align:right;"> 0.6613793 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.12,0.36] </td>
   <td style="text-align:right;"> 0.12 </td>
   <td style="text-align:right;"> 0.36 </td>
   <td style="text-align:right;"> 0.0385598 </td>
   <td style="text-align:right;"> 0.2164724 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants_Percentile </td>
   <td style="text-align:left;"> Transitioning </td>
   <td style="text-align:right;"> 0.3908477 </td>
   <td style="text-align:right;"> 0.2242991 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.17,0.06] </td>
   <td style="text-align:right;"> -0.17 </td>
   <td style="text-align:right;"> 0.06 </td>
   <td style="text-align:right;"> 0.0447612 </td>
   <td style="text-align:right;"> -0.0499175 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> all </td>
   <td style="text-align:left;"> all </td>
  </tr>
</tbody>
</table>

![](greensDataAnalysis_files/figure-html/percentileShiftsTable-1.png)<!-- -->

## BQI Shifts

![](greensDataAnalysis_files/figure-html/unnamed-chunk-11-1.png)<!-- -->



#### Comparison of two differently weighted candidate Quality Indexes, over the same basic variables.

![](greensDataAnalysis_files/figure-html/bqi1bqi2-1.png)<!-- -->


