---
title: "2020 Survey, Grain Nutrients ANOVA"
author: "Real Food Campaign"
date: "`r format( Sys.Date(), '%m/%d/%Y' )`"
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
    toc_depth: 3
    number_sections: true
---


```{r init,echo=FALSE,warnings=FALSE}

farmersTable <- read_rds( "../dataset/fullTableParsed.Rds" )
## theme_set(theme_gray(base_size = 24))

library("tidyverse")
library("broom")
## library("knitr")
library("kableExtra")
library("MASS")
library(knitr)
library(GGally)
library("caret")
library("DT")
library("doParallel")
source("../scripts/trainHelpers.R")

knitr::opts_chunk$set(
                    fig.width=12,
                    fig.height=10,
                    echo=FALSE,
                    warning=FALSE,
                    message=FALSE
                  )

soilMineralVars <- farmersTable %>%
  select( contains( "soil_10cm_xrf_" ), contains( "soil_20cm_xrf_" ) ) %>%
  colnames

cropMineralVars <-  farmersTable %>%
  select( contains( "produce_xrf_" ), - contains( "alt" ), -contains("percentile") ) %>%
  colnames

cropMineralSymbols <- cropMineralVars %>% str_remove("produce_xrf_")
soilMineralSymbols <- soilMineralVars %>% str_remove( "soil_10cm_xrf_" )
sharedMinerals <- soilMineralSymbols %>% keep( . %in% cropMineralSymbols ) %>% keep( . != "Ni" )

dataframe <-  farmersTable %>%
  filter( ( intake_survey == "grain" ) ) %>%
  filter(  ( sample_source != "farm_market" ) | is.na( sample_source ) ) %>%
  filter(  ( !is.na( polyphenols_final ) || !is.na( antioxidants_final ) ) || !is.na( proteins_final )  ) %>%
  select(
    "lab_id",
    "id_sample",
    "uid",
    "Type",
    "variety",
    "sample_source",
    "Polyphenols",
    "Antioxidants",
    "Proteins",
    "BQI",
    "climateRegion",
    "cropColor",
    "state",
    "wheat_class",
    "companion_cropping",
    "soilOrder",
    "store_name",
    "store_brand",
    matches( "^farm_practices" ),
    matches( "^amendments" ),
    matches( "^tillage" ),
    matches( "^seed_treatment" ),
    matches( "^surface.scan" ),
    matches( "^juice.scan" ),
    matches( "^whole.scan" ),
    matches( "it_1$" ),
    !!cropMineralVars,
    !!soilMineralVars,
    !!soilHealthVars,
    contains( "respiration_soil" ),
    "soilSuborder",
    "soilOrder",
  ) %>%
  mutate_at( vars( !!soilHealthVars, !!cropMineralVars ), ~ ifelse( .x < 0 , NA, .x ) ) %>%
  mutate(
    sample_source = ifelse(is.na(sample_source), "unknown", sample_source ),
    "companion_cropping.none" = map_lgl( companion_cropping, ~ !.x ),
    "companion_cropping.true" = map_lgl( companion_cropping, ~ .x )
  ) %>%
  rename(
    notill = `farm_practices.notill`,
    covercrops = `farm_practices.covercrops`
  )

## usually outliers are excessive sizes, but this particular one is a minimum
## dataframe %>% filter( Type == "oats" ) %>% select(  Proteins) %>% arrange(Proteins)
dataframe <- dataframe %>% filter( Proteins > 1 )

allAnovaModels <- function( explained, data = dataframe ) {
  modelFormula <- as.formula( c( explained, "~", "Type * notill * covercrops" ) %>% str_c( collapse = "" ) )
  model <- lm( modelFormula, data )
  residualsPlot <- tibble( y = model$model[[ explained ]], residual = model$residuals, y_hat = model$fitted.values ) %>%
    ggplot() +
    geom_abline() +
    aes( x = y, y = residual ) +
    geom_point() +
  labs(
    y = "Residuals",
    x = "Observed Y values.",
    title = paste( "Observed values versus residuals", "for", explained, sep = " " ),
    subtitle = "Species, notill, covercrops."
  ) 
  summaryTable <- model %>%
    tidy() %>%
    mutate_at( vars( - term ), round, digits = 3 ) %>%
    kable( caption = paste0( "ANOVA over tillage, covercrops and species for ", explained ) ) %>% kable_styling(
                                                                                                    c( "hover", "striped" ),
                                                                                                    full_width = FALSE,
                                                                                                    fixed_thead = TRUE
                                                                                                  )

  output <- list()
  output$summary <- summaryTable
  output$plot <- residualsPlot
  return( output )
}

explained <- c( "Antioxidants",
               "Polyphenols",
               "Proteins",
               "organic_carbon_percentage_10cm",
               "organic_carbon_percentage_20cm",
               "respiration_soil_10cm",
               "respiration_soil_20cm",
               dataframe %>% select( contains( "produce_xrf" ) ) %>% select( -contains("_cl"), -contains("_Ni"), -contains("_Mo") ) %>% colnames
               )

allAnova <- map( explained,
    allAnovaModels
)

names(allAnova) <- explained

identity <- function(x) {return(x)}

simpleRegression <- function( explained, predictor, data = dataframe, transformX = identity ) {
  data <- data %>% filter( .data[[explained]] > 0 & .data[[predictor]] > 0 )

  data[[predictor]] <- transformX( data[[predictor]] )

  modelFormula <- as.formula( c( explained, "~", predictor ) %>% str_c( collapse = "" ) )
  model <- lm( modelFormula, data )
  summary <- model %>%
    tidy() %>%
    kable( caption = paste( "Linear regression ", explained, " explained by ", predictor ) ) %>% kable_styling(
                                                                                                   c( "hover", "striped" ),
                                                                                                   full_width = FALSE,
                                                                                                   fixed_thead = TRUE
                                                                                                 )


  residualsPlot <- tibble( y = model$model[[explained]], residual = model$residuals, y_hat = model$fitted.values ) %>%
    ggplot() +
    geom_abline() +
    aes( x = y, y = residual ) +
    geom_point() +
    labs(
      y = "Residuals",
      x = "Observed Y values.",
      title = paste( "Observed values versus residuals", "for", explained, sep = " " ),
      subtitle = paste0( explained ," ~ ", predictor )
    ) 
  output <- list()
  output$summary <- summary
  output$plot <- residualsPlot
  output$model <- model
  output$rSquared <- model %>% summary %>% .[["r.squared"]]
  return( output )
}

soilToCropMineralsOats <- tibble(
  soil = c( sharedMinerals %>% map_chr( ~ str_c( "soil_10cm_xrf_", .x ) ), sharedMinerals %>% map_chr( ~ str_c( "soil_20cm_xrf_", .x ) ) ),
  crop = c( sharedMinerals %>% map_chr( ~ str_c( "produce_xrf_", .x ) ), sharedMinerals %>% map_chr( ~ str_c( "produce_xrf_", .x ) ) ),
  ) %>%
  mutate(
    regression = map2(crop, soil, ~ simpleRegression( .x, .y, data = dataframe %>% filter( Type == "oats" ) )),
    rSquared = map_dbl( regression, ~ .x[["rSquared"]] ),
    regression = map2(crop, soil, ~ simpleRegression( .x, .y, data = dataframe %>% filter( Type == "oats" ), transformX = function(x) { sqrt(x) } )),
    rSquaredRoot = map_dbl( regression, ~ .x[["rSquared"]] ),
    regression = map2(crop, soil, ~ simpleRegression( .x, .y, data = dataframe %>% filter( Type == "oats" ), transformX = function(x) { log(x) } )),
    rSquaredLog = map_dbl( regression, ~ .x[["rSquared"]] )
  ) %>%
  mutate(
    soil = str_remove( soil, pattern = "(soil_10cm_)|(soil_20cm_)" ),
    soil = str_remove( soil, pattern = "xrf_" ),
    crop = str_remove( crop, pattern = "produce_xrf_" )
  )

soilToCropMineralsWheat <- tibble(
  soil = c( sharedMinerals %>% map_chr( ~ str_c( "soil_10cm_xrf_", .x ) ), sharedMinerals %>% map_chr( ~ str_c( "soil_20cm_xrf_", .x ) ) ),
  crop = c( sharedMinerals %>% map_chr( ~ str_c( "produce_xrf_", .x ) ), sharedMinerals %>% map_chr( ~ str_c( "produce_xrf_", .x ) ) ),
  ) %>%
  mutate(
    regression = map2(crop, soil, ~ simpleRegression( .x, .y, data = dataframe %>% filter( Type == "wheat" ) )),
    rSquared = map_dbl( regression, ~ .x[["rSquared"]] ),
    regression = map2(crop, soil, ~ simpleRegression( .x, .y, data = dataframe %>% filter( Type == "wheat" ), transformX = function(x) { sqrt(x) } )),
    rSquaredRoot = map_dbl( regression, ~ .x[["rSquared"]] ),
    regression = map2(crop, soil, ~ simpleRegression( .x, .y, data = dataframe %>% filter( Type == "wheat" ), transformX = function(x) { log(x) } )),
    rSquaredLog = map_dbl( regression, ~ .x[["rSquared"]] )
  ) %>%
  mutate(
    soil = str_remove( soil, pattern = "(soil_)|(soil_)" ),
    soil = str_remove( soil, pattern = "xrf_" ),
    crop = str_remove( crop, pattern = "produce_xrf_" )
  )

```

# Crop Mineral on Soil Mineral Regressions

* These regressions have been attempted for both the observed X values and for logarithm and root of these values.

## Oats

```{r}
soilToCropMineralsOats %>%
  select( - regression ) %>%
  kable( caption = "Fits for the models that compare amount of mineral in soil with amount of mineral in crop." ) %>% kable_styling(
                                                                                                                        c( "hover", "striped" ),
                                                                                                                        full_width = FALSE,
                                                                                                                        fixed_thead = TRUE
                                                                                                                      )

```

## Wheat

```{r}
soilToCropMineralsWheat %>%
  select( - regression ) %>%
  kable( caption = "Fits for the models that compare amount of mineral in soil with amount of mineral in crop." ) %>% kable_styling(
                                                                                                                        c( "hover", "striped" ),
                                                                                                                        full_width = FALSE,
                                                                                                                        fixed_thead = TRUE
                                                                                                                      )

```


# Results by Explained Variable

##  Antioxidants

```{r} 
allAnova[[ "Antioxidants" ]]$summary
allAnova[[ "Antioxidants" ]]$plot
```


##  Antioxidants

```{r} 
allAnova[[ "Antioxidants" ]]$summary
allAnova[[ "Antioxidants" ]]$plot
```


##  Proteins

```{r} 
allAnova[[ "Proteins" ]]$summary
allAnova[[ "Proteins" ]]$plot
```


##  Polyphenols

```{r} 
allAnova[[ "Polyphenols" ]]$summary
allAnova[[ "Polyphenols" ]]$plot
```


##  organic_carbon_pecentage_10cm

```{r} 
allAnova[[ "organic_carbon_pecentage_10cm" ]]$summary
allAnova[[ "organic_carbon_pecentage_10cm" ]]$plot
```


##  organic_carbon_pecentage_20cm

```{r} 
allAnova[[ "organic_carbon_pecentage_20cm" ]]$summary
allAnova[[ "organic_carbon_pecentage_20cm" ]]$plot
```

##  respiration_soil_10cm

```{r} 
allAnova[[ "respiration_soil_10cm" ]]$summary
allAnova[[ "respiration_soil_10cm" ]]$plot
```


##  respiration_soil_20cm

```{r} 
allAnova[[ "respiration_soil_20cm" ]]$summary
allAnova[[ "respiration_soil_20cm" ]]$plot
```


##  produce_xrf_Na

```{r} 
allAnova[[ "produce_xrf_Na" ]]$summary
allAnova[[ "produce_xrf_Na" ]]$plot
```


##  produce_xrf_Mg

```{r} 
allAnova[[ "produce_xrf_Mg" ]]$summary
allAnova[[ "produce_xrf_Mg" ]]$plot
```

##  produce_xrf_Al

```{r} 
allAnova[[ "produce_xrf_Al" ]]$summary
allAnova[[ "produce_xrf_Al" ]]$plot
```


##  produce_xrf_Si

```{r} 
allAnova[[ "produce_xrf_Si" ]]$summary
allAnova[[ "produce_xrf_Si" ]]$plot
```


##  produce_xrf_P

```{r} 
allAnova[[ "produce_xrf_P" ]]$summary
allAnova[[ "produce_xrf_P" ]]$plot
```

##  produce_xrf_S

```{r} 
allAnova[[ "produce_xrf_S" ]]$summary
allAnova[[ "produce_xrf_S" ]]$plot
```


##  Antioxidants

```{r} 
allAnova[[ "produce_xrf_K" ]]$summary
allAnova[[ "produce_xrf_K" ]]$plot
```


##  produce_xrf_Ca

```{r} 
allAnova[[ "produce_xrf_Ca" ]]$summary
allAnova[[ "produce_xrf_Ca" ]]$plot
```


##  produce_xrf_Mn

```{r} 
allAnova[[ "produce_xrf_Mn" ]]$summary
allAnova[[ "produce_xrf_Mn" ]]$plot
```


##  produce_xrf_Fe

```{r} 
allAnova[[ "produce_xrf_Fe" ]]$summary
allAnova[[ "produce_xrf_Fe" ]]$plot
```


##  produce_xrf_Cu

```{r} 
allAnova[[ "produce_xrf_Cu" ]]$summary
allAnova[[ "produce_xrf_Cu" ]]$plot
```

##  produce_xrf_Zn

```{r} 
allAnova[[ "produce_xrf_Zn" ]]$summary
allAnova[[ "produce_xrf_Zn" ]]$plot
```
