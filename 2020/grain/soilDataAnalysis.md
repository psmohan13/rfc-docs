---
title: "2020 Survey, Soil Quality Relationships"
author: "Real Food Campaign"
<<<<<<< HEAD
date: "07/07/2021"
=======
date: "08/13/2021"
>>>>>>> 2020xrf
output:
  html_document:
    default: true
    keep_md: true
    toc: true
    toc_float: true
    toc_collapsed: true
    toc_depth: 3
    number_sections: true
---



```
## ── Attaching packages ───────────────────────────────────────────────────────────────────────────── tidyverse 1.3.0 ──
```

```
## ✔ ggplot2 3.3.3     ✔ purrr   0.3.4
## ✔ tibble  3.0.4     ✔ dplyr   1.0.2
## ✔ tidyr   1.1.2     ✔ stringr 1.4.0
## ✔ readr   1.4.0     ✔ forcats 0.5.0
```

```
## ── Conflicts ──────────────────────────────────────────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter() masks stats::filter()
## ✖ dplyr::lag()    masks stats::lag()
```

```
## 
## Attaching package: 'kableExtra'
```

```
## The following object is masked from 'package:dplyr':
## 
##     group_rows
```

```
## 
## Attaching package: 'MASS'
```

```
## The following object is masked _by_ '.GlobalEnv':
## 
##     select
```

```
## The following object is masked from 'package:dplyr':
## 
##     select
```

```
## Registered S3 method overwritten by 'GGally':
##   method from   
##   +.gg   ggplot2
```

```
## Loading required package: lattice
```

```
## 
## Attaching package: 'caret'
```

```
## The following object is masked from 'package:purrr':
## 
##     lift
```

```
## Loading required package: foreach
```

```
## 
## Attaching package: 'foreach'
```

```
## The following objects are masked from 'package:purrr':
## 
##     accumulate, when
```

```
## Loading required package: iterators
```

```
## Loading required package: parallel
```

```
## randomForest 4.6-14
```

```
## Type rfNews() to see new features/changes/bug fixes.
```

```
## 
## Attaching package: 'randomForest'
```

```
## The following object is masked from 'package:dplyr':
## 
##     combine
```

```
## The following object is masked from 'package:ggplot2':
## 
##     margin
```


# Context and Purpose of this Brief

  This document concentrates our analysis around soil helath indicators. Our main intentios are **measuring impact of soil helath over our crop quality variables**, **assesment of the potential of this variables to add predictive power to our machine learning models** and **summarising and testing the effects of management practices over soil health indicators**.


#### Crop Nutritional Density Variables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Crop Nutritional Density Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Nutritional </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
  </tr>
</tbody>
</table>

### Soil Health Variables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Soil Health Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Soil </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> PH_soil_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> PH_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_10cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> respiration_soil_20cm </td>
  </tr>
</tbody>
</table>

* The following table is available as a CSV file for manipulation, filename `./BFA-Soil-Extremes-Table.csv`.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Position values and some dispersion measures for main analytes.</caption>
 <thead>
<tr>
<th style="empty-cells: hide;border-bottom:hidden;" colspan="1"></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="12"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">0cm-10cm</div></th>
<th style="border-bottom:hidden;padding-bottom:0; padding-left:3px;padding-right:3px;text-align: center; " colspan="12"><div style="border-bottom: 1px solid #ddd; padding-bottom: 5px; ">10cm-20cm</div></th>
</tr>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Type </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median_10cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median_10cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> max_10cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> max_10cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> min_10cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> min_10cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> range_10cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> range_10cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sd_10cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sd_10cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio_10cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio_10cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median_20cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median_20cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> max_20cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> max_20cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> min_20cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> min_20cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> range_20cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> range_20cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sd_20cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> sd_20cm_respiration_soil </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio_20cm_organic_carbon_percentage </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> ratio_20cm_respiration_soil </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> oats </td>
   <td style="text-align:right;"> 2.48 </td>
   <td style="text-align:right;"> 19.48 </td>
   <td style="text-align:right;"> 17.67 </td>
   <td style="text-align:right;"> 55.86 </td>
   <td style="text-align:right;"> 1.46 </td>
   <td style="text-align:right;"> 6.72 </td>
   <td style="text-align:right;"> 16.21 </td>
   <td style="text-align:right;"> 49.14 </td>
   <td style="text-align:right;"> 3.784791 </td>
   <td style="text-align:right;"> 11.16015 </td>
   <td style="text-align:right;"> 12.102740 </td>
   <td style="text-align:right;"> 8.31250 </td>
   <td style="text-align:right;"> 2.31 </td>
   <td style="text-align:right;"> 14.75 </td>
   <td style="text-align:right;"> 17.18 </td>
   <td style="text-align:right;"> 35.17 </td>
   <td style="text-align:right;"> 1.12 </td>
   <td style="text-align:right;"> 3.75 </td>
   <td style="text-align:right;"> 16.06 </td>
   <td style="text-align:right;"> 31.42 </td>
   <td style="text-align:right;"> 3.557969 </td>
   <td style="text-align:right;"> 7.93948 </td>
   <td style="text-align:right;"> 15.339286 </td>
   <td style="text-align:right;"> 9.378667 </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> wheat </td>
   <td style="text-align:right;"> 3.29 </td>
   <td style="text-align:right;"> 20.91 </td>
   <td style="text-align:right;"> 7.27 </td>
   <td style="text-align:right;"> 47.81 </td>
   <td style="text-align:right;"> 1.15 </td>
   <td style="text-align:right;"> 4.52 </td>
   <td style="text-align:right;"> 6.12 </td>
   <td style="text-align:right;"> 43.29 </td>
   <td style="text-align:right;"> 1.178903 </td>
   <td style="text-align:right;"> 10.92112 </td>
   <td style="text-align:right;"> 6.321739 </td>
   <td style="text-align:right;"> 10.57743 </td>
   <td style="text-align:right;"> 2.72 </td>
   <td style="text-align:right;"> 12.02 </td>
   <td style="text-align:right;"> 6.94 </td>
   <td style="text-align:right;"> 36.41 </td>
   <td style="text-align:right;"> 0.99 </td>
   <td style="text-align:right;"> 3.30 </td>
   <td style="text-align:right;"> 5.95 </td>
   <td style="text-align:right;"> 33.11 </td>
   <td style="text-align:right;"> 1.222260 </td>
   <td style="text-align:right;"> 7.38056 </td>
   <td style="text-align:right;"> 7.010101 </td>
   <td style="text-align:right;"> 11.033333 </td>
  </tr>
</tbody>
</table>

## Methodologies

  We've performed an extensive medians comparisons in the past for crop nutritional quality indicators ( *antioxidants*, *polyphenols* and *brix* ). Methods, code implementations and a detailed guide on how we are interpreting results are all detailed on a previous brief available on the same site where this one is offered, *2019 Survey, Quality Relationships Analysis*.

  The predictive potential and impact of soil health indicators over nutritional density indicators will be measured by a non parametrical measure of correlation, *Spearman's Rank Correlation Coefficient*, in line with the rank tests employed for median shifts on our *Quality Relationships* synthesis tables.








## Boxplots for Soil Health Variables Over Climate Regions


![](soilDataAnalysis_files/figure-html/unnamed-chunk-3-1.png)<!-- -->![](soilDataAnalysis_files/figure-html/unnamed-chunk-3-2.png)<!-- -->


# Cuantification of Soil Quality Variables over Produce Quality Variables

  As stated, we are measuring correlation between soil and nutritional variables for each crop.
  Two linear regressions will be adjusted for each crop, explaining a nutritional variable with all six soil variables.
  
  On a first step, a **first linear model** is adjusted for all the explicative variables and all data points available. Then an **$\alpha$ trimming** is performed for the $15%$ biggest residuals and a **second model** is adjusted with this high residuals trimmmed dataset.
  
  We don't expect such a simple regression to yield a complete prediction of this variables, which have proven remarkably complex to predict in a regressive context. Good $R^2$ results will inform that this variables are good material to increase the predictive power of our current complex models, like Random Forests which already employ spectrometry, regions, varieties, etc.

## Fit Quality on Each Crop and Variable

* **Trimmed** models are thos were an alfa trimming has been performed: after a first run, the 15% worst residual samples are removed and a new model is trained over the clean dataset.
* The **F Test** p value is offered for both models.
* The **T test** p values are offered just for the trimmed models. No Bonferroni correction has been applied, so the reader should bear in mind that for 6 variables to give the desired confidence, the threshold p value should be divided by 6 ( that is, to satisfy a level 0.05 we need to see 0.083 on the t test ).

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Crop Nutritional Density Variables</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Crop </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Variable </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> Samples </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> R2 </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> F test p-value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> trimmed R2 </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> F test p-value, trimmed </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val (Intercept) </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val PH_soil_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val PH_soil_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val respiration_soil_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val respiration_soil_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val organic_carbon_percentage_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> t test p.val organic_carbon_percentage_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_Intercept </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_PH_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_PH_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_respiration_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_respiration_20cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_carbon_10cm </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> coefficient_carbon_20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:right;"> 0.3336129 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0.5506116 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.1334 </td>
   <td style="text-align:right;"> 0.0299 </td>
   <td style="text-align:right;"> 0.1787 </td>
   <td style="text-align:right;"> 0.1974 </td>
   <td style="text-align:right;"> 0.0054 </td>
   <td style="text-align:right;"> 23566.59 </td>
   <td style="text-align:right;"> -4770.85 </td>
   <td style="text-align:right;"> 980.33 </td>
   <td style="text-align:right;"> 20.85 </td>
   <td style="text-align:right;"> 19.25 </td>
   <td style="text-align:right;"> -86.13 </td>
   <td style="text-align:right;"> 206.13 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:right;"> 0.2255848 </td>
   <td style="text-align:right;"> 0.0000163 </td>
   <td style="text-align:right;"> 0.5235950 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0.0654 </td>
   <td style="text-align:right;"> 0.0072 </td>
   <td style="text-align:right;"> 0.6880 </td>
   <td style="text-align:right;"> 0.1436 </td>
   <td style="text-align:right;"> 0.0008 </td>
   <td style="text-align:right;"> 409.02 </td>
   <td style="text-align:right;"> -81.01 </td>
   <td style="text-align:right;"> 26.43 </td>
   <td style="text-align:right;"> 0.58 </td>
   <td style="text-align:right;"> 0.13 </td>
   <td style="text-align:right;"> -2.04 </td>
   <td style="text-align:right;"> 5.21 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 0.0593321 </td>
   <td style="text-align:right;"> 0.0187955 </td>
   <td style="text-align:right;"> 0.1953265 </td>
   <td style="text-align:right;"> 0.0000083 </td>
   <td style="text-align:right;"> 0.0347 </td>
   <td style="text-align:right;"> 0.1301 </td>
   <td style="text-align:right;"> 0.7720 </td>
   <td style="text-align:right;"> 0.0800 </td>
   <td style="text-align:right;"> 0.0326 </td>
   <td style="text-align:right;"> 0.0010 </td>
   <td style="text-align:right;"> 0.2591 </td>
   <td style="text-align:right;"> 279.38 </td>
   <td style="text-align:right;"> -32.29 </td>
   <td style="text-align:right;"> -5.38 </td>
   <td style="text-align:right;"> 0.44 </td>
   <td style="text-align:right;"> -0.91 </td>
   <td style="text-align:right;"> 12.47 </td>
   <td style="text-align:right;"> -3.57 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:right;"> 0.0986035 </td>
   <td style="text-align:right;"> 0.0111945 </td>
   <td style="text-align:right;"> 0.1662471 </td>
   <td style="text-align:right;"> 0.0015721 </td>
   <td style="text-align:right;"> 0.7470 </td>
   <td style="text-align:right;"> 0.0357 </td>
   <td style="text-align:right;"> 0.0001 </td>
   <td style="text-align:right;"> 0.1984 </td>
   <td style="text-align:right;"> 0.7311 </td>
   <td style="text-align:right;"> 0.0023 </td>
   <td style="text-align:right;"> 0.0032 </td>
   <td style="text-align:right;"> -1.94 </td>
   <td style="text-align:right;"> -3.16 </td>
   <td style="text-align:right;"> 5.44 </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> -0.01 </td>
   <td style="text-align:right;"> 0.44 </td>
   <td style="text-align:right;"> -0.43 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 0.0581945 </td>
   <td style="text-align:right;"> 0.0201620 </td>
   <td style="text-align:right;"> 0.0931013 </td>
   <td style="text-align:right;"> 0.0053923 </td>
   <td style="text-align:right;"> 0.9654 </td>
   <td style="text-align:right;"> 0.7534 </td>
   <td style="text-align:right;"> 0.5215 </td>
   <td style="text-align:right;"> 0.0088 </td>
   <td style="text-align:right;"> 0.1919 </td>
   <td style="text-align:right;"> 0.2600 </td>
   <td style="text-align:right;"> 0.4511 </td>
   <td style="text-align:right;"> -0.46 </td>
   <td style="text-align:right;"> 0.55 </td>
   <td style="text-align:right;"> 0.94 </td>
   <td style="text-align:right;"> 0.05 </td>
   <td style="text-align:right;"> 0.04 </td>
   <td style="text-align:right;"> -0.32 </td>
   <td style="text-align:right;"> -0.18 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:right;"> 156 </td>
   <td style="text-align:right;"> 0.0074932 </td>
   <td style="text-align:right;"> 0.3120372 </td>
   <td style="text-align:right;"> 0.0362460 </td>
   <td style="text-align:right;"> 0.1001291 </td>
   <td style="text-align:right;"> 0.6498 </td>
   <td style="text-align:right;"> 0.0893 </td>
   <td style="text-align:right;"> 0.2137 </td>
   <td style="text-align:right;"> 0.2740 </td>
   <td style="text-align:right;"> 0.0250 </td>
   <td style="text-align:right;"> 0.4899 </td>
   <td style="text-align:right;"> 0.1031 </td>
   <td style="text-align:right;"> 3739.13 </td>
   <td style="text-align:right;"> -2084.70 </td>
   <td style="text-align:right;"> 1901.64 </td>
   <td style="text-align:right;"> 15.82 </td>
   <td style="text-align:right;"> -47.45 </td>
   <td style="text-align:right;"> -139.87 </td>
   <td style="text-align:right;"> 292.13 </td>
  </tr>
</tbody>
</table>

### Residuals Graphics for the Most Promissing Regressions

#### Antioxidants on Oats

![](soilDataAnalysis_files/figure-html/oatsAnti-1.png)<!-- -->![](soilDataAnalysis_files/figure-html/oatsAnti-2.png)<!-- -->![](soilDataAnalysis_files/figure-html/oatsAnti-3.png)<!-- -->![](soilDataAnalysis_files/figure-html/oatsAnti-4.png)<!-- -->

## Random Forest Regressions

This random forest models are cross validated and include several other variables sets, to help better compare the importance of our soil predictors in comparison with other available information, which in this case is the hand spectrometer scans.

### Datasets

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>composition of datasets</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> name </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> variables </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
  <tr>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm, organic_carbon_percentage_20cm, PH_soil_10cm                  , PH_soil_20cm                  , respiration_soil_10cm         , respiration_soil_20cm </td>
  </tr>
</tbody>
</table>


### Observed Results


<table>
<caption>Random Forest Models on Soil and Scans</caption>
 <thead>
  <tr>
   <th style="text-align:left;"> explained </th>
   <th style="text-align:left;"> species </th>
   <th style="text-align:left;"> dataset </th>
   <th style="text-align:right;"> cvRsquared </th>
   <th style="text-align:right;"> mtry </th>
   <th style="text-align:right;"> N </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:right;"> 0.5922025 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:right;"> 0.7474786 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:right;"> 0.8130643 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:right;"> 0.8998767 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:right;"> 0.6560193 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:right;"> 0.7289460 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:right;"> 0.7441049 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:right;"> 0.7510898 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:right;"> 0.2978997 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:right;"> 0.5258622 </td>
   <td style="text-align:right;"> 37 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:right;"> 0.6164505 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> oats </td>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:right;"> 0.7746392 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 62 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:right;"> 0.3057721 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:right;"> 0.4259567 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:right;"> 0.5601010 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Antioxidants </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:right;"> 0.5866954 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:right;"> 0.3720503 </td>
   <td style="text-align:right;"> 35 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:right;"> 0.3910017 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:right;"> 0.5436691 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Polyphenols </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:right;"> 0.5929456 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set2 </td>
   <td style="text-align:right;"> 0.3319374 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set4 </td>
   <td style="text-align:right;"> 0.5075490 </td>
   <td style="text-align:right;"> 34 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set3 </td>
   <td style="text-align:right;"> 0.5555066 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Proteins </td>
   <td style="text-align:left;"> wheat </td>
   <td style="text-align:left;"> set1 </td>
   <td style="text-align:right;"> 0.5760422 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 108 </td>
  </tr>
</tbody>
</table>

#### Quality of fit plots for all Random Forest Regressions on Soil Health

![](soilDataAnalysis_files/figure-html/allImportance-1.png)<!-- -->

![](soilDataAnalysis_files/figure-html/allRegressions-1.png)<!-- -->


# Quality Relationship Tables

  We offer separatedly the synthesis of this tables on the same site were this brief is available, showing the shifts as percentages of deviation over the median and the sample sizes. 
  
  This variables have shown interesting behaviours. As we are not accounting for the crop factor, populations are robust for most evaluated practices, and most of them have shown shifts that our test could detect as significant. 
  
  Tables offered below are much deeper so the recommended procedure is searching for interesting data on the synthesis tables, and then getting a deeper insight by looking at the complete summaries. 
  
  Besites the medians shift analysis, we've performed a much more informal summary for the frequency of factors on certaing percentiles. The intuitive idea behind this procedure is that if the frequency of *Practice X* over the whole dataset is $30%$ but while looking at the "10%" biggest values for the vaiable it doubles to $60%$, it is probably *concentrated* among the most succesfull crops. There are tables for the best and worst percentiles showing this behaviour. 
  
  It's main advantage is that it spares the reader looking at the enormuos amount of graphics that would have been involved in comparint all these factors for each of the three variables and that it shows patterns even while not detected as relevant by the test, could be interesting for certain investigators or worth an additional exploration.

## Percentual Shift Over the Median Value for Farm Practices

### p.values Code Key

<table class="table table-hover table-striped table-condensed" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>p Values Color Scale</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real shift, when observed is Positive </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Chances of 0 real Shift, when observed is Negative </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;">Less than 10%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;">Less than 10%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">Between 10% and 50%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">Between 10% and 50%</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;">50% or more%</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;">50% or more</span> </td>
  </tr>
</tbody>
</table>

### Tables

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Certified Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">12.6</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">44.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">20.32</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 12">33.7</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Covercrops </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">-10.31</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">-16.53</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 82">-15.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 85">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 88">-25.99</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Notill </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 54">10.65</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 75">-29.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 51">-0.95</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 57">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 75">-35.64</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 163">-18.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 172">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 160">-26.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 172">-19.05</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 172">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 172">-30.67</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Regenerative </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 139">-20</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 142">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 133">-22.96</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 142">-21.91</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 142">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 142">-28.43</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 178">-14.38</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 187">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 181">-20.41</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 187">-15.24</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 187">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 187">-28.43</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Transitioning </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 162">-18.82</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 171">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 159">-27.15</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 171">-19.68</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 171">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 171">-33.2</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Soil Amendments

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Organic Amendment </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 65">24.08</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 71">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 68">1.71</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">12.21</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 69">-10.68</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Synth Fertilizer </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 114">8.85</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 120">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 132">-19.72</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 114">-3.23</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 120">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 138">-29.23</span> </td>
  </tr>
</tbody>
</table>

## Percentual Shift Over the Median for Tillage Intensity

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Percent of Variation over Median Value</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Factor </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Organic Carbon Percentage 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Ph Soil 20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> Respiration Soil 20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Heavy Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 81">-26.27</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 87">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 81">13.58</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 87">-25.62</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 87">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.6) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 87">12.49</span> </td>
  </tr>
  <tr>
   <td style="text-align:left;font-weight: bold;border-right:1px solid;"> Light Tillage </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 153">-23.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 162">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 153">5.75</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(255,0,0,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 162">-17.97</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,1) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 162">0</span> </td>
   <td style="text-align:left;"> <span style=" font-weight: bold;    color: white !important;border-radius: 4px; padding-right: 4px; padding-left: 4px; background-color: rgba(1,121,11,0.2) !important;" data-toggle="tooltip" data-container="body" data-placement="right" title="Sample size: 162">7.94</span> </td>
  </tr>
</tbody>
</table>


## Quality Replationships for PH at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 5.515790 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0016985 </td>
   <td style="text-align:right;"> -5.80e-06 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 5.613904 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.7262667 </td>
   <td style="text-align:right;"> 1.30e-06 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.606452 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [5.8,5.9] </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 0.0069266 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 5.899998 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 5.648235 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 85 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.2435823 </td>
   <td style="text-align:right;"> 7.03e-05 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 5.615116 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 172 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.6716861 </td>
   <td style="text-align:right;"> 2.15e-05 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 5.597183 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 142 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.9070666 </td>
   <td style="text-align:right;"> -1.94e-05 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 5.600000 </td>
   <td style="text-align:right;"> 5.50 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.9516992 </td>
   <td style="text-align:right;"> -2.01e-05 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 5.650000 </td>
   <td style="text-align:right;"> 5.65 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0.3] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.3 </td>
   <td style="text-align:right;"> 0.1178566 </td>
   <td style="text-align:right;"> 4.39e-05 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.647273 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [5.8,5.8] </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 5.547500 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 120 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0000002 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -1.80e-05 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 5.563380 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 71 </td>
   <td style="text-align:left;"> PH_soil_10cm </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0003653 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -3.46e-05 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.2307692 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4.2692308 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.5384615 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 2.1346154 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1.6263736 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.3846154 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.2937063 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.3846154 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 1.1858974 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 1.1017370 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8461538 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 0.9991817 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.4615385 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.9487179 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.3846154 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0.8712716 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1538462 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.8131868 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 0.7851459 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 0.7344913 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0.7042030 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.3846154 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.6885856 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.3846154 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.5404090 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0689655 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.5517241 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.4827586 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 1.9137931 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.1034483 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.9137931 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.6206897 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.4060521 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6551724 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.3467433 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.4827586 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1.2758621 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.5517241 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.9877642 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8275862 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 0.9772561 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.7931034 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0.9075720 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.6551724 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 0.8359096 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.2413793 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.8119122 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.6551724 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 0.7819800 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.2413793 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.7442529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.5172414 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.7267569 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.3793103 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.6790879 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1034483 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.5467980 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0588235 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.1764706 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.2941176 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.5546218 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.5294118 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.1992797 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.6470588 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 1.1584440 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.9411765 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 1.1113892 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5294118 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.0882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.4117647 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1.0882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0588235 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 1.0882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0507099 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.2352941 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.9327731 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.7647059 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 0.9127135 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.6470588 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.9091586 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.7647059 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0.8750758 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.4705882 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.8425047 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.1764706 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.5441176 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.1176471 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.3957219 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>


## Quality Replationships for PH at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 5.515790 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0009821 </td>
   <td style="text-align:right;"> -2.30e-05 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 5.591444 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.3397924 </td>
   <td style="text-align:right;"> -4.60e-06 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.634483 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [5.8,6] </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0069266 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 5.999996 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 5.581176 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 85 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.1673679 </td>
   <td style="text-align:right;"> -3.01e-05 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 5.576744 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 172 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.1670173 </td>
   <td style="text-align:right;"> -6.60e-06 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 5.573944 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 142 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.1454968 </td>
   <td style="text-align:right;"> -6.42e-05 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 5.573684 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.1587522 </td>
   <td style="text-align:right;"> -5.20e-06 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 5.500000 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0337461 </td>
   <td style="text-align:right;"> -5.32e-05 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 5.587273 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [5.8,5.9] </td>
   <td style="text-align:right;"> 5.8 </td>
   <td style="text-align:right;"> 5.9 </td>
   <td style="text-align:right;"> 0.0000104 </td>
   <td style="text-align:right;"> 5.89992 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 5.567500 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 120 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 40 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.4141736 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -5.74e-05 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 5.595652 </td>
   <td style="text-align:right;"> 5.5 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:left;"> PH_soil_20cm </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0] </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.0 </td>
   <td style="text-align:right;"> 0.7630738 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 5.20e-06 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.2307692 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 4.2692308 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.4615385 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 1.8296703 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.5384615 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1.4230769 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5384615 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.1068376 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.4615385 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.0455259 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.3076923 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.0349650 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8461538 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 0.9991817 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.5384615 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.9640199 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.5384615 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.9640199 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.3076923 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.9487179 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1538462 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.8131868 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 0.7851459 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 0.7344913 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0.7042030 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.4615385 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.6484907 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.2068966 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 3.8275862 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.3103448 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 1.2302956 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5517241 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.1340996 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.4827586 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.0935961 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8965517 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 1.0586941 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.5862069 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 1.0494994 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.3103448 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.0438871 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.7931034 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0118906 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.3103448 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.9568966 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.5172414 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.9260289 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.6896552 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 0.8231368 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.3103448 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.8201970 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.6896552 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0.7891930 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.5517241 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.7752073 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1379310 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.7290640 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0588235 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 2.1764706 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.5882353 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.3325330 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6470588 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.3300654 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.2352941 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.2436975 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.4117647 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1.0882353 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.5882353 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 1.0531309 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0507099 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8823529 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 1.0419274 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 0.9829222 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0.9423893 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.2352941 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.9327731 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.6470588 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.9091586 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.4117647 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.7371917 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.1176471 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.3627451 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.0588235 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.1978610 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>


## Quality Replationships for respiration at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 20.86120 </td>
   <td style="text-align:right;"> 18.320 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-10.73,-3.55] </td>
   <td style="text-align:right;"> -10.73 </td>
   <td style="text-align:right;"> -3.55 </td>
   <td style="text-align:right;"> 0.7730809 </td>
   <td style="text-align:right;"> -7.500087 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 22.24994 </td>
   <td style="text-align:right;"> 21.210 </td>
   <td style="text-align:right;"> 181 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 57 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-8.26,-1.84] </td>
   <td style="text-align:right;"> -8.26 </td>
   <td style="text-align:right;"> -1.84 </td>
   <td style="text-align:right;"> 0.9886222 </td>
   <td style="text-align:right;"> -5.210052 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 26.95774 </td>
   <td style="text-align:right;"> 26.440 </td>
   <td style="text-align:right;"> 31 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [23.52,29.35] </td>
   <td style="text-align:right;"> 23.52 </td>
   <td style="text-align:right;"> 29.35 </td>
   <td style="text-align:right;"> 0.7756771 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 25.52498 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 23.02294 </td>
   <td style="text-align:right;"> 21.880 </td>
   <td style="text-align:right;"> 85 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-7.43,-0.75] </td>
   <td style="text-align:right;"> -7.43 </td>
   <td style="text-align:right;"> -0.75 </td>
   <td style="text-align:right;"> 0.7103840 </td>
   <td style="text-align:right;"> -4.220060 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 21.38525 </td>
   <td style="text-align:right;"> 19.815 </td>
   <td style="text-align:right;"> 160 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-9.8,-3.02] </td>
   <td style="text-align:right;"> -9.80 </td>
   <td style="text-align:right;"> -3.02 </td>
   <td style="text-align:right;"> 0.9985837 </td>
   <td style="text-align:right;"> -6.649951 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 22.00263 </td>
   <td style="text-align:right;"> 21.210 </td>
   <td style="text-align:right;"> 133 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-9.46,-2.36] </td>
   <td style="text-align:right;"> -9.46 </td>
   <td style="text-align:right;"> -2.36 </td>
   <td style="text-align:right;"> 0.7929066 </td>
   <td style="text-align:right;"> -5.859952 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 20.70208 </td>
   <td style="text-align:right;"> 18.180 </td>
   <td style="text-align:right;"> 159 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-9.98,-3.65] </td>
   <td style="text-align:right;"> -9.98 </td>
   <td style="text-align:right;"> -3.65 </td>
   <td style="text-align:right;"> 0.4696464 </td>
   <td style="text-align:right;"> -6.930037 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 33.61250 </td>
   <td style="text-align:right;"> 38.305 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.58,15.59] </td>
   <td style="text-align:right;"> -0.58 </td>
   <td style="text-align:right;"> 15.59 </td>
   <td style="text-align:right;"> 0.6940149 </td>
   <td style="text-align:right;"> 11.299997 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 23.76925 </td>
   <td style="text-align:right;"> 23.00 </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [22.01,24.87] </td>
   <td style="text-align:right;"> 22.01 </td>
   <td style="text-align:right;"> 24.87 </td>
   <td style="text-align:right;"> 0.6768711 </td>
   <td style="text-align:right;"> 23.43144 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 19.91955 </td>
   <td style="text-align:right;"> 16.40 </td>
   <td style="text-align:right;"> 132 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 44 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-6.99,-2.49] </td>
   <td style="text-align:right;"> -6.99 </td>
   <td style="text-align:right;"> -2.49 </td>
   <td style="text-align:right;"> 0.2064026 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -4.620035 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 23.95956 </td>
   <td style="text-align:right;"> 23.25 </td>
   <td style="text-align:right;"> 68 </td>
   <td style="text-align:left;"> respiration_soil_10cm </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-4.02,4.01] </td>
   <td style="text-align:right;"> -4.02 </td>
   <td style="text-align:right;"> 4.01 </td>
   <td style="text-align:right;"> 0.9633709 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 0.399991 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.2000000 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 2.500000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.4380952 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 2.282609 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.2666667 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 1.875000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.3714286 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 1.346154 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.75 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 1.334746 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.7714286 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 1.296296 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8285714 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.206897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.153846 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.153846 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.75 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.6952381 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 1.078767 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.4857143 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 1.029412 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.3142857 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.2857143 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.4642857 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.2666667 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 1.7410714 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.6071429 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.3714286 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 1.6346154 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.6428571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.4380952 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 1.4673913 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.2500000 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.2000000 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.2500000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.9285714 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.7714286 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 1.2037037 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.3214286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2857143 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 1.1250000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.9285714 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.8285714 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.1206897 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.9642857 </td>
   <td style="text-align:right;"> 27 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.1126374 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.7500000 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.6952381 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 1.0787671 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.9285714 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.0714286 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.3214286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.3142857 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.0227273 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.4285714 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.4857143 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0.8823529 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.4285714 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.7627119 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.3571429 </td>
   <td style="text-align:right;"> 10 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.6355932 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.3750 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.2000000 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.8750000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.8125 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 1.4459746 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.7500 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 1.3347458 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.0000 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8285714 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.2068966 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.8125 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.6952381 </td>
   <td style="text-align:right;"> 73 </td>
   <td style="text-align:right;"> 1.1686644 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 1.0000 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.1538462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8125 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.7714286 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 1.0532407 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8125 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 0.9375000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.2500 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.2857143 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 0.8750000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.2500 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.3142857 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.7954545 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.3750 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.4857143 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0.7720588 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.1875 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.4380952 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.4279891 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.2666667 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.3714286 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

## Quality Replationships for Soil Respiration at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 11.45880 </td>
   <td style="text-align:right;"> 11.540 </td>
   <td style="text-align:right;"> 75 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-9.62,-5.15] </td>
   <td style="text-align:right;"> -9.62 </td>
   <td style="text-align:right;"> -5.15 </td>
   <td style="text-align:right;"> 0.3293664 </td>
   <td style="text-align:right;"> -7.170012 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 14.15048 </td>
   <td style="text-align:right;"> 12.280 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-8.05,-3.08] </td>
   <td style="text-align:right;"> -8.05 </td>
   <td style="text-align:right;"> -3.08 </td>
   <td style="text-align:right;"> 0.6815266 </td>
   <td style="text-align:right;"> -5.720011 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 19.93655 </td>
   <td style="text-align:right;"> 17.360 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [16.75,23.37] </td>
   <td style="text-align:right;"> 16.75 </td>
   <td style="text-align:right;"> 23.37 </td>
   <td style="text-align:right;"> 0.3467162 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 20.11997 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 14.89568 </td>
   <td style="text-align:right;"> 12.280 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-7.99,-2.3] </td>
   <td style="text-align:right;"> -7.99 </td>
   <td style="text-align:right;"> -2.30 </td>
   <td style="text-align:right;"> 0.8994985 </td>
   <td style="text-align:right;"> -5.229965 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 13.94570 </td>
   <td style="text-align:right;"> 11.540 </td>
   <td style="text-align:right;"> 172 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-8.76,-3.64] </td>
   <td style="text-align:right;"> -8.76 </td>
   <td style="text-align:right;"> -3.64 </td>
   <td style="text-align:right;"> 0.8494409 </td>
   <td style="text-align:right;"> -6.170026 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 14.16134 </td>
   <td style="text-align:right;"> 11.945 </td>
   <td style="text-align:right;"> 142 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-8.15,-3.03] </td>
   <td style="text-align:right;"> -8.15 </td>
   <td style="text-align:right;"> -3.03 </td>
   <td style="text-align:right;"> 0.8033115 </td>
   <td style="text-align:right;"> -5.720081 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 13.07550 </td>
   <td style="text-align:right;"> 11.480 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-9.14,-4.4] </td>
   <td style="text-align:right;"> -9.14 </td>
   <td style="text-align:right;"> -4.40 </td>
   <td style="text-align:right;"> 0.5356360 </td>
   <td style="text-align:right;"> -6.679944 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 25.41000 </td>
   <td style="text-align:right;"> 26.780 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [2.66,10.92] </td>
   <td style="text-align:right;"> 2.66 </td>
   <td style="text-align:right;"> 10.92 </td>
   <td style="text-align:right;"> 0.2688513 </td>
   <td style="text-align:right;"> 6.780056 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 16.97036 </td>
   <td style="text-align:right;"> 15.280 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [14.88,17.9] </td>
   <td style="text-align:right;"> 14.88 </td>
   <td style="text-align:right;"> 17.90 </td>
   <td style="text-align:right;"> 0.1901434 </td>
   <td style="text-align:right;"> 16.28503 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 11.29217 </td>
   <td style="text-align:right;"> 9.885 </td>
   <td style="text-align:right;"> 138 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-6.37,-3.16] </td>
   <td style="text-align:right;"> -6.37 </td>
   <td style="text-align:right;"> -3.16 </td>
   <td style="text-align:right;"> 0.5183152 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> -4.759987 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 15.12478 </td>
   <td style="text-align:right;"> 14.720 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:left;"> respiration_soil_20cm </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-3.63,0.48] </td>
   <td style="text-align:right;"> -3.63 </td>
   <td style="text-align:right;"> 0.48 </td>
   <td style="text-align:right;"> 0.3628288 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -1.740012 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.3940345 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.2649573 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 1.1935484 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 1.1443299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8461538 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0795756 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8461538 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 0.9991817 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.2307692 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.9148352 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.6153846 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.8646543 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1538462 </td>
   <td style="text-align:right;"> 2 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.8131868 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.2307692 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.7762238 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.2307692 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.7115385 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.3846154 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.6885856 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.3846154 </td>
   <td style="text-align:right;"> 5 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.6885856 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.2307692 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.6098901 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.6896552 </td>
   <td style="text-align:right;"> 20 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.5622801 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.3103448 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 1.2302956 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5862069 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.2049808 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.2068966 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.0935961 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.8965517 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 1.0700779 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.8965517 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 1.0259509 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.7931034 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0118906 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.7931034 </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 0.9365371 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.5172414 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.9260289 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.6206897 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.8721082 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.2068966 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.6959248 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.2068966 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.6379310 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.3103448 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.5556174 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.2068966 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.5467980 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.3529412 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 1.8655462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 1.7903226 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.6470588 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.4657863 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.6470588 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.3300654 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.2758621 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 1.1935484 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8823529 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 1.0419274 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 0.9423893 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.6470588 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.9091586 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.1764706 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.6995798 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.3529412 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.6318786 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.1764706 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.4663866 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

## Quality Replationships for Carbon Mass Percentage at 10cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 3.688889 </td>
   <td style="text-align:right;"> 3.405 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.03,0.72] </td>
   <td style="text-align:right;"> 0.03 </td>
   <td style="text-align:right;"> 0.72 </td>
   <td style="text-align:right;"> 0.7954177 </td>
   <td style="text-align:right;"> 0.3407743 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 3.489213 </td>
   <td style="text-align:right;"> 2.650 </td>
   <td style="text-align:right;"> 178 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 56 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.81,-0.08] </td>
   <td style="text-align:right;"> -0.81 </td>
   <td style="text-align:right;"> -0.08 </td>
   <td style="text-align:right;"> 0.7163876 </td>
   <td style="text-align:right;"> -0.4600204 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 3.553929 </td>
   <td style="text-align:right;"> 3.025 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:left;"> [2.84,4.02] </td>
   <td style="text-align:right;"> 2.84 </td>
   <td style="text-align:right;"> 4.02 </td>
   <td style="text-align:right;"> 0.5378632 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 3.200003 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 3.071412 </td>
   <td style="text-align:right;"> 2.800 </td>
   <td style="text-align:right;"> 85 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 23 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.74,0.07] </td>
   <td style="text-align:right;"> -0.74 </td>
   <td style="text-align:right;"> 0.07 </td>
   <td style="text-align:right;"> 0.6061381 </td>
   <td style="text-align:right;"> -0.3299976 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 3.394294 </td>
   <td style="text-align:right;"> 2.480 </td>
   <td style="text-align:right;"> 163 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 50 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.9,-0.25] </td>
   <td style="text-align:right;"> -0.90 </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.9042538 </td>
   <td style="text-align:right;"> -0.5999459 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 3.426403 </td>
   <td style="text-align:right;"> 2.460 </td>
   <td style="text-align:right;"> 139 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 45 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.97,-0.31] </td>
   <td style="text-align:right;"> -0.97 </td>
   <td style="text-align:right;"> -0.31 </td>
   <td style="text-align:right;"> 0.6823771 </td>
   <td style="text-align:right;"> -0.6400138 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 3.430185 </td>
   <td style="text-align:right;"> 2.480 </td>
   <td style="text-align:right;"> 162 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 48 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.9,-0.25] </td>
   <td style="text-align:right;"> -0.90 </td>
   <td style="text-align:right;"> -0.25 </td>
   <td style="text-align:right;"> 0.8860400 </td>
   <td style="text-align:right;"> -0.6020907 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 3.442500 </td>
   <td style="text-align:right;"> 3.430 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.02,0.71] </td>
   <td style="text-align:right;"> -0.02 </td>
   <td style="text-align:right;"> 0.71 </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 0.4033526 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 3.018037 </td>
   <td style="text-align:right;"> 2.67 </td>
   <td style="text-align:right;"> 107 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:right;"> 32 </td>
   <td style="text-align:left;"> [2.77,3.18] </td>
   <td style="text-align:right;"> 2.77 </td>
   <td style="text-align:right;"> 3.18 </td>
   <td style="text-align:right;"> 0.0234882 </td>
   <td style="text-align:right;"> 2.989943 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 4.039211 </td>
   <td style="text-align:right;"> 2.96 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0,0.5] </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0.50 </td>
   <td style="text-align:right;"> 0.9003100 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> 0.2645370 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 3.673846 </td>
   <td style="text-align:right;"> 3.51 </td>
   <td style="text-align:right;"> 65 </td>
   <td style="text-align:left;"> organic_carbon_percentage_10cm </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.37,1] </td>
   <td style="text-align:right;"> 0.37 </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 0.4474561 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.7200575 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable. 

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.75 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2857143 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 2.6250000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.75 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.3142857 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 2.3863636 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.1714286 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 1.4583333 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.7238095 </td>
   <td style="text-align:right;"> 76 </td>
   <td style="text-align:right;"> 1.3815789 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.75 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 1.3347458 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.7714286 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 1.2962963 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8285714 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.2068966 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8380952 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 1.1931818 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 1.00 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.1538462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.2666667 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.9375000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.3714286 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.6730769 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.4380952 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.5706522 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.25 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.4449153 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.5142857 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.00 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.1071429 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 3.7500000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.6785714 </td>
   <td style="text-align:right;"> 19 </td>
   <td style="text-align:right;"> 0.4380952 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 1.5489130 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.3214286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2857143 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 1.1250000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.5714286 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.5142857 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 1.1111111 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8571429 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.7714286 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 1.1111111 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8928571 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.8380952 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 1.0653409 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.8571429 </td>
   <td style="text-align:right;"> 24 </td>
   <td style="text-align:right;"> 0.8285714 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0344828 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.8928571 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.0302198 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.3214286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.3142857 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.0227273 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.2500000 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.2666667 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.9375000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.6428571 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7238095 </td>
   <td style="text-align:right;"> 76 </td>
   <td style="text-align:right;"> 0.8881579 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.4642857 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.8262712 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.2500000 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.3714286 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 0.6730769 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1071429 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.1714286 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.6250000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.3214286 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.5720339 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.5625 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.2857143 </td>
   <td style="text-align:right;"> 30 </td>
   <td style="text-align:right;"> 1.9687500 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.5625 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.3142857 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.7897727 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.9375 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 1.6684322 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 1.0000 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.7238095 </td>
   <td style="text-align:right;"> 76 </td>
   <td style="text-align:right;"> 1.3815789 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.0000 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8285714 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.2068966 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 1.0000 </td>
   <td style="text-align:right;"> 16 </td>
   <td style="text-align:right;"> 0.8666667 </td>
   <td style="text-align:right;"> 91 </td>
   <td style="text-align:right;"> 1.1538462 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8125 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.7714286 </td>
   <td style="text-align:right;"> 81 </td>
   <td style="text-align:right;"> 1.0532407 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.3750 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.3714286 </td>
   <td style="text-align:right;"> 39 </td>
   <td style="text-align:right;"> 1.0096154 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8125 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8380952 </td>
   <td style="text-align:right;"> 88 </td>
   <td style="text-align:right;"> 0.9694602 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.4375 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.5142857 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.8506944 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.4375 </td>
   <td style="text-align:right;"> 7 </td>
   <td style="text-align:right;"> 0.5619048 </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0.7786017 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.1875 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.2666667 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.7031250 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.0625 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.4380952 </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0.1426630 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0571429 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.1714286 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0285714 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

## Quality Replationships for Soil Carbon Mass Percentage at 20cm

### Median Shifts

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Farm Practices: Position Measures and Median Comparisons with the 'none' category</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> notill </td>
   <td style="text-align:right;"> 3.017059 </td>
   <td style="text-align:right;"> 2.73 </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.41,0.28] </td>
   <td style="text-align:right;"> -0.41 </td>
   <td style="text-align:right;"> 0.28 </td>
   <td style="text-align:right;"> 0.1877977 </td>
   <td style="text-align:right;"> -0.0300233 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 3.239358 </td>
   <td style="text-align:right;"> 2.26 </td>
   <td style="text-align:right;"> 187 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 59 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.82,-0.12] </td>
   <td style="text-align:right;"> -0.82 </td>
   <td style="text-align:right;"> -0.12 </td>
   <td style="text-align:right;"> 0.1986198 </td>
   <td style="text-align:right;"> -0.4799451 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 3.112069 </td>
   <td style="text-align:right;"> 3.01 </td>
   <td style="text-align:right;"> 29 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:left;"> [2.5,3.63] </td>
   <td style="text-align:right;"> 2.50 </td>
   <td style="text-align:right;"> 3.63 </td>
   <td style="text-align:right;"> 0.3872131 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 3.149989 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 2.677439 </td>
   <td style="text-align:right;"> 2.28 </td>
   <td style="text-align:right;"> 82 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 22 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.95,-0.18] </td>
   <td style="text-align:right;"> -0.95 </td>
   <td style="text-align:right;"> -0.18 </td>
   <td style="text-align:right;"> 0.2493834 </td>
   <td style="text-align:right;"> -0.4799781 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 3.190930 </td>
   <td style="text-align:right;"> 2.19 </td>
   <td style="text-align:right;"> 172 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 53 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.92,-0.23] </td>
   <td style="text-align:right;"> -0.92 </td>
   <td style="text-align:right;"> -0.23 </td>
   <td style="text-align:right;"> 0.2350949 </td>
   <td style="text-align:right;"> -0.5999765 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 3.244789 </td>
   <td style="text-align:right;"> 2.08 </td>
   <td style="text-align:right;"> 142 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 46 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-1.04,-0.33] </td>
   <td style="text-align:right;"> -1.04 </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.2004154 </td>
   <td style="text-align:right;"> -0.6900240 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 3.129357 </td>
   <td style="text-align:right;"> 2.16 </td>
   <td style="text-align:right;"> 171 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 51 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.93,-0.28] </td>
   <td style="text-align:right;"> -0.93 </td>
   <td style="text-align:right;"> -0.28 </td>
   <td style="text-align:right;"> 0.2009706 </td>
   <td style="text-align:right;"> -0.6199134 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 4.020000 </td>
   <td style="text-align:right;"> 3.25 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.33,1.28] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 1.28 </td>
   <td style="text-align:right;"> 0.3968649 </td>
   <td style="text-align:right;"> 0.6400531 </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Amendments: Position Measures and Median Comparisons with the 'none' category.</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> mean </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> median </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> N </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> explained </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDs </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> observedUIDsY </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> HL Interval Medians Shift </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMin </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> intervalMax </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> p.value </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Estimate </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> isRelevant </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> HL Median Shift Estimate </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 2.863727 </td>
   <td style="text-align:right;"> 2.500 </td>
   <td style="text-align:right;"> 110 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:left;"> [2.59,2.97] </td>
   <td style="text-align:right;"> 2.59 </td>
   <td style="text-align:right;"> 2.97 </td>
   <td style="text-align:right;"> 0.0103802 </td>
   <td style="text-align:right;"> 2.784971 </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> NA </td>
  </tr>
  <tr>
   <td style="text-align:left;"> synth_fertilizer </td>
   <td style="text-align:right;"> 3.624474 </td>
   <td style="text-align:right;"> 2.455 </td>
   <td style="text-align:right;"> 114 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 38 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [-0.33,0.16] </td>
   <td style="text-align:right;"> -0.33 </td>
   <td style="text-align:right;"> 0.16 </td>
   <td style="text-align:right;"> 0.7112157 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> FALSE </td>
   <td style="text-align:right;"> -0.0900485 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic_amendment </td>
   <td style="text-align:right;"> 3.186812 </td>
   <td style="text-align:right;"> 3.020 </td>
   <td style="text-align:right;"> 69 </td>
   <td style="text-align:left;"> organic_carbon_percentage_20cm </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:left;"> [0.02,0.7] </td>
   <td style="text-align:right;"> 0.02 </td>
   <td style="text-align:right;"> 0.70 </td>
   <td style="text-align:right;"> 0.3714700 </td>
   <td style="text-align:right;"> NA </td>
   <td style="text-align:left;"> TRUE </td>
   <td style="text-align:right;"> 0.3400368 </td>
  </tr>
</tbody>
</table>

### Density Variation on Certain Percentiles

On this tables, we attempt to find interesting patterns to explain atypicals and hopefully all variance for a given variable.

We've already looked at these tables in detail on the *Quality Relationships* brief for produce: we are comparing how frequent is to find a certain quality for the full dataset and for a certain quantile, implying that if a certain attribute is more usual between the points on an extreme, it is worth checking if it is influential over the variable and explains the outlyingness of some values.

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Decile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.9,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.9230769 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 3.1048951 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.9230769 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 2.8461538 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.9230769 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 1.6526055 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 1.4050633 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.2758621 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 1.1935484 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 1.1808511 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 1.1443299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.0769231 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.4065934 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.0769231 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.3049451 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.0769231 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.2032967 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.0769231 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0.1742543 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.0769231 </td>
   <td style="text-align:right;"> 1 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.1377171 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Last Quartile for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0.75,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.4137931 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.3918495 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.4137931 </td>
   <td style="text-align:right;"> 12 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 1.2758621 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 0.9655172 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 1.1523915 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8620690 </td>
   <td style="text-align:right;"> 25 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0998811 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.4827586 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 1.0935961 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8965517 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 1.0586941 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 0.8965517 </td>
   <td style="text-align:right;"> 26 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 1.0259509 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.5172414 </td>
   <td style="text-align:right;"> 15 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.9260289 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.4482759 </td>
   <td style="text-align:right;"> 13 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.9214559 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 0.6206897 </td>
   <td style="text-align:right;"> 18 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 0.8721082 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1379310 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.7290640 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.3793103 </td>
   <td style="text-align:right;"> 11 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.6790879 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.1379310 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.5467980 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.1379310 </td>
   <td style="text-align:right;"> 4 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 0.3645320 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>Presence on the Worst 15% for Every Practice</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> factor </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,0.15] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> frequency[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> total[0,1] </th>
   <th style="text-align:right;position: sticky; top:0; background-color: #FFFFFF;"> reason </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> seed_treatment.biological </td>
   <td style="text-align:right;"> 0.4705882 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.2972973 </td>
   <td style="text-align:right;"> 33 </td>
   <td style="text-align:right;"> 1.5828877 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.none </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 1.4743833 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.synth_fertilizer </td>
   <td style="text-align:right;"> 0.4705882 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.3243243 </td>
   <td style="text-align:right;"> 36 </td>
   <td style="text-align:right;"> 1.4509804 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> regenerative </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.7117117 </td>
   <td style="text-align:right;"> 79 </td>
   <td style="text-align:right;"> 1.4050633 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.heavy_tillage </td>
   <td style="text-align:right;"> 0.4705882 </td>
   <td style="text-align:right;"> 8 </td>
   <td style="text-align:right;"> 0.3783784 </td>
   <td style="text-align:right;"> 42 </td>
   <td style="text-align:right;"> 1.2436975 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> transitioning </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.8378378 </td>
   <td style="text-align:right;"> 93 </td>
   <td style="text-align:right;"> 1.1935484 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> organic </td>
   <td style="text-align:right;"> 1.0000000 </td>
   <td style="text-align:right;"> 17 </td>
   <td style="text-align:right;"> 0.8738739 </td>
   <td style="text-align:right;"> 97 </td>
   <td style="text-align:right;"> 1.1443299 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage.light_tillage </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.7837838 </td>
   <td style="text-align:right;"> 87 </td>
   <td style="text-align:right;"> 1.0507099 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tillage </td>
   <td style="text-align:right;"> 0.8235294 </td>
   <td style="text-align:right;"> 14 </td>
   <td style="text-align:right;"> 0.8468468 </td>
   <td style="text-align:right;"> 94 </td>
   <td style="text-align:right;"> 0.9724656 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.none </td>
   <td style="text-align:right;"> 0.5294118 </td>
   <td style="text-align:right;"> 9 </td>
   <td style="text-align:right;"> 0.5585586 </td>
   <td style="text-align:right;"> 62 </td>
   <td style="text-align:right;"> 0.9478178 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.organic_amendment </td>
   <td style="text-align:right;"> 0.1764706 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.1891892 </td>
   <td style="text-align:right;"> 21 </td>
   <td style="text-align:right;"> 0.9327731 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amendments.none </td>
   <td style="text-align:right;"> 0.3529412 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.4864865 </td>
   <td style="text-align:right;"> 54 </td>
   <td style="text-align:right;"> 0.7254902 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> covercrops </td>
   <td style="text-align:right;"> 0.1764706 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.2522523 </td>
   <td style="text-align:right;"> 28 </td>
   <td style="text-align:right;"> 0.6995798 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> companion_cropping.true </td>
   <td style="text-align:right;"> 0.1764706 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.4414414 </td>
   <td style="text-align:right;"> 49 </td>
   <td style="text-align:right;"> 0.3997599 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> none </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0540541 </td>
   <td style="text-align:right;"> 6 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> certified_organic </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> seed_treatment.fungicide </td>
   <td style="text-align:right;"> 0.0000000 </td>
   <td style="text-align:right;"> 0 </td>
   <td style="text-align:right;"> 0.0270270 </td>
   <td style="text-align:right;"> 3 </td>
   <td style="text-align:right;"> 0.0000000 </td>
  </tr>
</tbody>
</table>

# Predicting Soil Organic Carbon Content

 In an attempt of having as much reassurance as possible, both Linear Models and Random Forest Models have been trained for this experiment. 
 
 
 As it is to be expected when mixing diverse sets of variables which can't be expected to seek for an intrinsical mathematical model such as a physical equation, random forest yields an increase in precission. 
 
 The behaviour of this increase in observed precission points to future enquiry paths about the informing power of each variable, especially considering the most powerful model replaces the 10 bionutrient meter scan variables by just `climateRegion` achieving an apparent improvement. This result is probably the result of an overfit over the climate region and probably shouldn't be considered relevant. I wouldn't attribute this to a mathematical overfit, but to an under representation of the internal variance of each climatic region, so a further check is also seeing how many actual locations represent each climatic region and how far from each other they are.
 
 Overall, the prediction seems feasible but probably requires some further variables to be useful, besides the checks we considered above. 
 
 This experiment will without doubt benefit from a comparison with all the other soil samples we have, even considering that `crop` as factor for sure has a high influence on the results both a as a consequence of the physiological action of each species on it and because it is to be expected that the crop has been chosen because of preexisting qualities of the soil. 
 
 A proposed strategy to face the possible overfit over regions is to clusterize the residulas/actual vs. predicted plots and search for evident clusters.
 
 
## Grain Soils
 

<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 1-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 2-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 3-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 4-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 5-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 6-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 7-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 1-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 2-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 3-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 4-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 5-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 6-20cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 7-20cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> bionutrientMeter10cm </td>
   <td style="text-align:left;"> bionutrientMeter10cm, scsMetadata10cm </td>
   <td style="text-align:left;"> bionutrientMeter10cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata10cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm </td>
   <td style="text-align:left;"> scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm </td>
   <td style="text-align:left;"> bionutrientMeter20cm </td>
   <td style="text-align:left;"> bionutrientMeter20cm, scsMetadata20cm </td>
   <td style="text-align:left;"> bionutrientMeter20cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm </td>
   <td style="text-align:left;"> scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm </td>
   <td style="text-align:left;"> scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm </td>
  </tr>
</tbody>
</table>

<<<<<<< HEAD
<!--html_preserve--><div id="htmlwidget-10c0ce4c331cd3b4cb26" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-10c0ce4c331cd3b4cb26">{"x":{"filter":"none","caption":"<caption>Random Forest on Grain Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["10cm","20cm","10cm","20cm","10cm","20cm","20cm","20cm","10cm","10cm","20cm","20cm","10cm","10cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm","bionutrientMeter20cm","bionutrientMeter10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm, climateRegion, medFarmPractices","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm"],[0.4015,0.4573,0.4631,0.5245,0.5656,0.5718,0.6072,0.6727,0.6818,0.6835,0.6898,0.6919,0.721,0.7386],[281,290,267,249,243,276,238,238,232,234,238,235,229,234],[21,2,22,1,14,29,2,4,2,37,15,5,14,29],[0.98932384341637,0.986206896551724,0.98876404494382,1,0.979423868312757,0.981884057971015,0.983193277310924,0.974789915966387,0.987068965517241,0.961538461538462,0.953781512605042,0.987234042553191,0.96943231441048,0.957264957264957],[0.0290556900726392,0.039715873996294,0.024818401937046,0.10772104607721,0.0181598062953995,0.0206609017912292,0.124221668742217,0.133063511830635,0.122881355932203,0.0441888619854721,0.0485678704856787,0.108343711083437,0.0472154963680387,0.0334745762711866],[null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>N<\/th>\n      <th>mtry<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n      <th>model<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[4,5,6,7,8]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[4]; $(this.api().cell(row, 4).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-a8c0f2aff79514efd2ee" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-a8c0f2aff79514efd2ee">{"x":{"filter":"none","caption":"<caption>Linear Model on Grain Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10"],["10cm","20cm","10cm","20cm","10cm","20cm","20cm","10cm","20cm","10cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm","bionutrientMeter10cm"],[0.4627,0.3905,0.3712,0.3296,0.3248,0.2496,0.2137,0.191,0.1877,0.1599]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>depth<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":4},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

## Soil Models for All Crops

<!--html_preserve--><div id="htmlwidget-6594051c5b3ef59c4d8c" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-6594051c5b3ef59c4d8c">{"x":{"filter":"none","caption":"<caption>Random Forest on all Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["20cm","20cm","10cm","10cm","20cm","10cm","10cm","20cm","20cm","20cm","20cm","10cm","10cm","10cm"],["bionutrientMeter20cm","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm","bionutrientMeter20cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices"],[0.2936,0.3353,0.4109,0.4808,0.534,0.5418,0.5731,0.5782,0.5884,0.5971,0.5974,0.6039,0.6061,0.6124],[0.854471069549971,0.869598180439727,0.865060240963855,0.884615384615385,0.90622009569378,0.836325237592397,0.818833162743091,0.869670152855994,0.793466807165437,0.792452830188679,0.781488549618321,0.789778206364513,0.892233009708738,0.862745098039216],[0.0496023138105568,0.043916109202676,0.137126865671642,0.13715103793844,0.0817780794027825,0.0735400144196107,0.0666906993511175,0.0610583446404342,0.0655270655270655,0.0678062678062678,0.0393620631150322,0.0590551181102362,0.120204008589835,0.0868690434168726]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-48a715cbb097d3a7f18c" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-48a715cbb097d3a7f18c">{"x":{"filter":"none","caption":"<caption>Linear Model on all Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["10cm","10cm","10cm","10cm","20cm","20cm","20cm","20cm","10cm","10cm","10cm","20cm","20cm","20cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","bionutrientMeter20cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm","bionutrientMeter10cm, scsMetadata10cm","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter20cm","bionutrientMeter20cm, scsMetadata20cm"],[0.2909,0.2883,0.2808,0.2647,0.205,0.2035,0.1889,0.1387,0.1221,0.1009,0.097,0.0966,0.0241,0.0208]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>depth<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":4},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->
=======
<!--html_preserve--><div id="htmlwidget-11474ba75bee43068bae" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-11474ba75bee43068bae">{"x":{"filter":"none","caption":"<caption>Random Forest on Grain Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["10cm","20cm","10cm","20cm","10cm","20cm","20cm","20cm","10cm","10cm","20cm","20cm","10cm","10cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm","bionutrientMeter20cm","bionutrientMeter10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm, climateRegion, medFarmPractices","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm"],[0.4015,0.4573,0.4631,0.5245,0.5656,0.5718,0.6072,0.6727,0.6818,0.6835,0.6898,0.6919,0.721,0.7386],[281,290,267,249,243,276,238,238,232,234,238,235,229,234],[21,2,22,1,14,29,2,4,2,37,15,5,14,29],[0.98932384341637,0.986206896551724,0.98876404494382,1,0.979423868312757,0.981884057971015,0.983193277310924,0.974789915966387,0.987068965517241,0.961538461538462,0.953781512605042,0.987234042553191,0.96943231441048,0.957264957264957],[0.0290556900726392,0.039715873996294,0.024818401937046,0.10772104607721,0.0181598062953995,0.0206609017912292,0.124221668742217,0.133063511830635,0.122881355932203,0.0441888619854721,0.0485678704856787,0.108343711083437,0.0472154963680387,0.0334745762711866],[null,null,null,null,null,null,null,null,null,null,null,null,null,null]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>N<\/th>\n      <th>mtry<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n      <th>model<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[4,5,6,7,8]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[4]; $(this.api().cell(row, 4).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-1dcf007e905cc6b8f150" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-1dcf007e905cc6b8f150">{"x":{"filter":"none","caption":"<caption>Linear Model on Grain Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10"],["10cm","20cm","10cm","20cm","10cm","20cm","20cm","10cm","20cm","10cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter20cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm","bionutrientMeter10cm"],[0.4627,0.3905,0.3712,0.3296,0.3248,0.2496,0.2137,0.191,0.1877,0.1599]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>depth<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":4},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->

## Soil Models for All Crops

<!--html_preserve--><div id="htmlwidget-f4870d59d199f81dbb02" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-f4870d59d199f81dbb02">{"x":{"filter":"none","caption":"<caption>Random Forest on all Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["20cm","20cm","10cm","10cm","20cm","10cm","10cm","20cm","20cm","20cm","20cm","10cm","10cm","10cm"],["bionutrientMeter20cm","bionutrientMeter20cm, scsMetadata20cm","bionutrientMeter10cm","bionutrientMeter10cm, scsMetadata10cm","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm","bionutrientMeter20cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices"],[0.2936,0.3353,0.4109,0.4808,0.534,0.5418,0.5731,0.5782,0.5884,0.5971,0.5974,0.6039,0.6061,0.6124],[0.854471069549971,0.869598180439727,0.865060240963855,0.884615384615385,0.90622009569378,0.836325237592397,0.818833162743091,0.869670152855994,0.793466807165437,0.792452830188679,0.781488549618321,0.789778206364513,0.892233009708738,0.862745098039216],[0.0496023138105568,0.043916109202676,0.137126865671642,0.13715103793844,0.0817780794027825,0.0735400144196107,0.0666906993511175,0.0610583446404342,0.0655270655270655,0.0678062678062678,0.0393620631150322,0.0590551181102362,0.120204008589835,0.0868690434168726]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-4b2c6d18f49202ddb7ad" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-4b2c6d18f49202ddb7ad">{"x":{"filter":"none","caption":"<caption>Linear Model on all Soils<\/caption>","data":[["1","2","3","4","5","6","7","8","9","10","11","12","13","14"],["10cm","10cm","10cm","10cm","20cm","20cm","20cm","20cm","10cm","10cm","10cm","20cm","20cm","20cm"],["soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter10cm, scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm, climateRegion, medFarmPractices","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","scsMetadata10cm, climateRegion, medFarmPractices, PH_soil_10cm, respiration_soil_10cm","bionutrientMeter20cm, scsMetadata20cm, climateRegion, medFarmPractices","scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_20cm","scsMetadata10cm, scsMetadata20cm, climateRegion, medFarmPractices, PH_soil_10cm, PH_soil_20cm, respiration_soil_10cm, respiration_soil_20cm","bionutrientMeter20cm, climateRegion, medFarmPractices","scsMetadata10cm, climateRegion, medFarmPractices","bionutrientMeter10cm","bionutrientMeter10cm, scsMetadata10cm","scsMetadata20cm, climateRegion, medFarmPractices, respiration_soil_20cm","bionutrientMeter20cm","bionutrientMeter20cm, scsMetadata20cm"],[0.2909,0.2883,0.2808,0.2647,0.205,0.2035,0.1889,0.1387,0.1221,0.1009,0.097,0.0966,0.0241,0.0208]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>depth<\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":4},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->
>>>>>>> 2020xrf

### Organic content percentage at 10cm, model '2-10cm'

* Saved as `./graphics/grainSoilHealRegressionModel210cm.png`.



## Depth as a variable


<table class="table table-hover table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>datasets composition</caption>
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 1-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 2-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 3-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 4-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 5-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 6-10cm </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> 7-10cm </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> bionutrientMeter </td>
   <td style="text-align:left;"> bionutrientMeter, scsMetadata </td>
   <td style="text-align:left;"> bionutrientMeter, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> bionutrientMeter, scsMetadata, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata, climateRegion, medFarmPractices </td>
   <td style="text-align:left;"> scsMetadata, climateRegion, medFarmPractices, PH_soil </td>
   <td style="text-align:left;"> bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil </td>
  </tr>
</tbody>
</table>

<<<<<<< HEAD
<!--html_preserve--><div id="htmlwidget-a529b3061160c2a7c050" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-a529b3061160c2a7c050">{"x":{"filter":"none","caption":"<caption>Random Forest on all Soils, depth as variable.<\/caption>","data":[["1","2","3","4","5","6","7"],["organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage"],["bionutrientMeter","bionutrientMeter, scsMetadata","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices"],[0.4283,0.4791,0.5368,0.5533,0.5916,0.6018,0.6319],[1660,1274,974,1030,977,1224,1037],[0.859638554216868,0.884615384615385,0.910677618069815,0.901941747572816,0.817809621289662,0.893790849673203,0.832208293153327],[0.127332089552239,0.137437365783822,0.127054794520548,0.132211882605584,0.0634462869502523,0.117984468761031,0.0809949892627057]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>N<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-de92138e05fe43323a42" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-de92138e05fe43323a42">{"x":{"filter":"none","caption":"<caption>Linear Model on all Soils, depth as a parameter.<\/caption>","data":[["1","2","3","4","5","6","7"],["soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices","bionutrientMeter, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices, PH_soil","scsMetadata, climateRegion, medFarmPractices","bionutrientMeter","bionutrientMeter, scsMetadata"],[0.3628,0.3045,0.2839,0.2744,0.1472,0.1028,0.0987]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":3},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->
=======
<!--html_preserve--><div id="htmlwidget-10c0ce4c331cd3b4cb26" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-10c0ce4c331cd3b4cb26">{"x":{"filter":"none","caption":"<caption>Random Forest on all Soils, depth as variable.<\/caption>","data":[["1","2","3","4","5","6","7"],["organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage","organic_carbon_percentage"],["bionutrientMeter","bionutrientMeter, scsMetadata","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices"],[0.4283,0.4791,0.5368,0.5533,0.5916,0.6018,0.6319],[1660,1274,974,1030,977,1224,1037],[0.859638554216868,0.884615384615385,0.910677618069815,0.901941747572816,0.817809621289662,0.893790849673203,0.832208293153327],[0.127332089552239,0.137437365783822,0.127054794520548,0.132211882605584,0.0634462869502523,0.117984468761031,0.0809949892627057]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>explained<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n      <th>N<\/th>\n      <th>covers<\/th>\n      <th>width<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"lengthMenu":[10,20,40,80],"columnDefs":[{"className":"dt-right","targets":[3,4,5,6]},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false,"rowCallback":"function(row, data) {\nvar value=data[3]; $(this.api().cell(row, 3).node()).css({'color':isNaN(parseFloat(value)) ? '' : value <= 0.6 ? \"black\" : value <= 0.8 ? \"blue\" : \"red\"});\n}"}},"evals":["options.rowCallback"],"jsHooks":[]}</script><!--/html_preserve--><!--html_preserve--><div id="htmlwidget-a8c0f2aff79514efd2ee" style="width:100%;height:auto;" class="datatables html-widget"></div>
<script type="application/json" data-for="htmlwidget-a8c0f2aff79514efd2ee">{"x":{"filter":"none","caption":"<caption>Linear Model on all Soils, depth as a parameter.<\/caption>","data":[["1","2","3","4","5","6","7"],["soil","soil","soil","soil","soil","soil","soil"],["bionutrientMeter, scsMetadata, climateRegion, medFarmPractices, PH_soil","bionutrientMeter, scsMetadata, climateRegion, medFarmPractices","bionutrientMeter, climateRegion, medFarmPractices","scsMetadata, climateRegion, medFarmPractices, PH_soil","scsMetadata, climateRegion, medFarmPractices","bionutrientMeter","bionutrientMeter, scsMetadata"],[0.3628,0.3045,0.2839,0.2744,0.1472,0.1028,0.0987]],"container":"<table class=\"display\">\n  <thead>\n    <tr>\n      <th> <\/th>\n      <th>species<\/th>\n      <th>dataset<\/th>\n      <th>cvRsquared<\/th>\n    <\/tr>\n  <\/thead>\n<\/table>","options":{"columnDefs":[{"className":"dt-right","targets":3},{"orderable":false,"targets":0}],"order":[],"autoWidth":false,"orderClasses":false}},"evals":[],"jsHooks":[]}</script><!--/html_preserve-->
>>>>>>> 2020xrf



