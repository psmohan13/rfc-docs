---
title: "Some Functional Data Modelling over our Produce Spectrograms"
date: "May 20, 2020"
author: "Real Food Campaign"
output:
  # pdf_document: default
  html_document: default
---

```{r preamble}
library("magrittr")
library("fda")
library("refund")
library("rainbow")
library("tidyverse")
```

# Introduction

In the context of our ongoing efforts to arrive to a model that should enable us to predict Polyphenols and Antioxidants contents of produce based on spectrogram data, we are considering this recent functional data analysis develompents. We will test in here functions to plot relevan information, search for outliers and finally attempt a regression.

At first, we'll work just wit one product type (carrots) to test the tooling and refine our code before batch aplying some or all the procedures to the whole dataset. This is done because calculation times are huge for some of the processes and we want a first and dynamical approach.

# First Graphical Approaches

## SIWARE

We've already stripped our data of some indisputably outlying curves, this rainbowplot shows some problems tipically found when working with this kind of data. Above all, we need to get rid (and hopefully explain in a later stage) of two type of outliers: some of them are outliers by their magnitude and some are outliers because of the shape of the curves they describe. This second type is exclusive of functional data and there is no standard solution to find it, so we are trying some solutions available in recent literature.

```{r first plots siware}

dataframe <- read_rds("../modelling/dfStrippedOfFarOutliers.Rds")

juiceVars <- dataframe %>% select( contains("Juice"), -contains("Device"), -contains("Date"), -contains("Scan") ) %>% colnames()
surfaceVars <- dataframe %>% select( contains("Surface"), -contains("Device"), -contains("Date"), -contains("Scan") ) %>% colnames()
targetVars <- c("Polyphenols", "Antioxidants", "Color")

## We'll add a column for identification of measuring repetitions

presentIDisReplica <- function( state, id ) {
  previousId <- state$id
  count <- state$count
  if ( id == previousId ) {
    newCount <- count + 1
  } else { newCount <- 1 }
  return( list( "id" = id, "count" = newCount ) )
}

replicasList <- purrr::accumulate( dataframe$id_full, presentIDisReplica, .init=list( "id" = 0, "count" = 1 ) )
replicasCol <- map_dbl( replicasList, ~(.x)$count )
dataframe$replica <- replicasCol[-1]
dataframe <- mutate( dataframe, id_rep = paste( id_full, replica, sep="_" ) )

## Now, we add the variation sequences

data <- dataframe %>%
  select( one_of(!!!juiceVars), one_of(!!!targetVars), Type, id_rep, replica ) %>%
  filter( Type == "kale" ) %>%
  arrange( id_rep ) %>%
  na.omit()

data <- data %>% 
  mutate( cluster = ifelse( data$"Juice-siware1600nm" < 50, "inferior", "superior" ) %>% as.factor )

siwareKale <- data  %>%
  gather( !!!juiceVars, key = "wavelength", value = "waveIntensity" ) %>%
  mutate( wavelength = as.factor(wavelength) ) %>%
  arrange( Antioxidants, id_rep, wavelength )

siwareKale$wavelength <- str_extract( siwareKale$wavelength, pattern = "[0-9]+" )
siwareKale$wavelength %<>% as.double

siwareKale %>%
  arrange( -Antioxidants ) %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Color, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  scale_color_viridis_d()

siwareKale %>%
  arrange( Antioxidants ) %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  scale_color_viridis_c()

siwareKale %>%
  ggplot() +
  aes( x = wavelength, y = as.factor(Antioxidants), z = waveIntensity, fill = waveIntensity, colour = waveIntensity, alfa = 0.3, label = id_rep ) +
  geom_tile( size = 15 ) +
  scale_fill_viridis_c() +
  scale_color_viridis_c()

siwareSuperiorTile <- siwareKale %>%
  filter( cluster == "superior" ) %>%
  ggplot() +
  aes( x = wavelength, y = as.factor(Polyphenols), z = waveIntensity, fill = waveIntensity, colour = waveIntensity, alfa = 0.3, label = id_rep ) +
  geom_tile( height = 1, width = 16 ) +
  scale_fill_viridis_c() +
  scale_color_viridis_c()

siwareSuperiorTile

## plot_gg( siwareSuperiorTile, multicore = TRUE, width = 5, height = 5, scale = 250 )
## Sys.sleep(0.5)
## render_snapshot( filename="spectraSnapshot.png", clear = TRUE )

jointDensity <- siwareKale %>%
  ggplot() +
  aes( x = Antioxidants, y = Polyphenols ) +
  stat_density_2d( aes( fill = ..level.. ), geom = "polygon" ) +
  ggtitle( "Joint Distribution of Antioxidants and Polyphenols" ) +
  scale_fill_viridis_c()

jointDensity

## plot_gg( jointDensity, multicore = TRUE, width = 5, height = 5, scale = 250 )
## render_depth(focallength=100,focus=0.72)
## render_snapshot( filename="densitySnapshot.png" )

kaleMatrixSup <- data %>%
  filter( cluster == "superior" ) %>%
  select( contains("siware") ) %>%
  as.matrix() %>% t()

colnames( kaleMatrixSup ) <- data %>%
  filter( cluster == "superior" ) %>%
  select( id_rep ) %>% unlist

boxplot <- fbplot( kaleMatrixSup )

outliersSW <- colnames(kaleMatrixSup)[ boxplot$outpoint ]

siwareKale %>%
  filter( cluster == "superior" ) %>%
  filter( !( id_rep %in% outliersSW ) ) %>%
  ggplot() +
  aes( x = wavelength, y = as.factor(Polyphenols), z = waveIntensity, fill = waveIntensity, colour = waveIntensity, alfa = 0.3, label = id_rep ) +
  geom_tile( height = 2, width = 16 ) +
  scale_fill_viridis_c() +
  scale_color_viridis_c()

siwareKale %>%
  filter( !( id_rep %in% outliersSW ) ) %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  ggtitle( "Curves for SWIR Juice" ) +
  scale_color_viridis_c()
```

#### Regression

```{r siware regression}
swY <- data %>%
  filter( !(id_rep %in% outliersSW ) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swX <- data %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFit <- pfr( swY ~ fpc( swX ) )

summary( rawFit )

penalizedFit <- pfr( swY ~ lf( swX, bs = "ps", k = 45, fx = TRUE ) )
summary( penalizedFit )

swY <- data %>%
  filter( cluster == "superior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swX <- data %>%
  filter( cluster == "superior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFit <- pfr( swY ~ fpc( swX ) )
summary( rawFit )

penalizedFit <- pfr( swY ~ lf( swX, bs = "ps", k = 45, fx = TRUE ) )
summary( penalizedFit )

swYinf <- data %>%
  filter( cluster == "inferior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swXinf <- data %>%
  filter( cluster == "inferior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFitInf <- pfr( swYinf ~ fpc( swXinf ) )
summary( rawFitInf )

penalizedFitInf <- pfr( swYinf ~ lf( swXinf, bs = "ps", k = 45, fx = TRUE ) )
summary( penalizedFitInf )
```

### Explaining The Clustering

#### Dates

```{r siware clustering date}
siwareKale %>%
  arrange( Antioxidants ) %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = id_rep, colour = Antioxidants, alfa = 0.3 ) +
  geom_line() +
  facet_wrap( cluster ~ . , scales = "free" ) +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  scale_color_viridis_c()

idsInferior <- siwareKale %>%
  filter( cluster == "inferior" ) %>%
  select( id_rep ) %>%
  unlist()

idsSuperior <- siwareKale %>%
  filter( cluster == "superior" ) %>%
  select( id_rep ) %>%
  unlist()

inferiorDates <- dataframe %>%
  filter( id_rep %in% idsInferior ) %>%
  select( siwareJuiceDate ) %>%
  unique() %>%
  unlist()

superiorDates <- dataframe %>%
  filter( id_rep %in% idsSuperior ) %>%
  select( siwareJuiceDate ) %>%
  unique() %>%
  unlist()

sharedDates <- intersect( superiorDates, inferiorDates )
allDates <- union( superiorDates, inferiorDates )
```

#### Processing Time

```{r siware clustering processing time}
inferiorTimes <- dataframe %>%
  filter( id_rep %in% idsInferior ) %>%
  select( Processing_time_hrs ) %>%
  unlist()

superiorTimes <- dataframe %>%
  filter( id_rep %in% idsSuperior ) %>%
  select( Processing_time_hrs ) %>%
  unlist()

inferiorTimes %>% na.omit %>% median
superiorTimes %>% na.omit %>% median
```

### Pre Processing

#### Normalization

The faceted by clusters graphics is terribly suggestive, as are the tests we've applied.

Even a very crude normalization results in an extremely coherent dataset.

```{r normalization }
siwareKale <- siwareKale %>%
  group_by( id_rep ) %>%
  mutate(norm = sqrt( sum( waveIntensity^2 ) ),
         scaledWI = waveIntensity / norm
         ) %>%
  ungroup()

siwareKale %>%
  arrange( Antioxidants ) %>%
  ggplot() +
  aes( x = wavelength, y = scaledWI, fill = id_rep, colour = Antioxidants, alfa = 0.3 ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  scale_color_viridis_c()

siwareMatrix <- siwareKale %>%
  select( id_rep, wavelength, scaledWI ) %>%
  spread( key = wavelength, value = scaledWI )

swY <- data %>%
  ## filter( !(id_rep %in% outliersSW ) ) %>%
  ## filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swX <- data %>%
  ## filter( !(id_rep %in% outliersSW) ) %>%
  ## filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFit <- pfr( swY ~ fpc( swX ) )

summary( rawFit )

penalizedFit <- pfr( swY ~ lf( swX, bs = "ps", k = 45, fx = TRUE ) )

summary( penalizedFit )
```

#### Penalized Smoothing

We'll remove the grosest outliers with the functional depth procedure before smoothing, to get the best possible PCA during this second process.
This penalized smoothing would be based on the second derivative and will use a basis with $K=p$. Once smoothed, the curves are ready for the best possible results on PCA and then we'll apply a more severe outlier search before finally attempting our regression. 
At this point, curve registration/phase correction appears to be unnecesary.

```{r smoothing}
boxplot <-  fbplot( siwareMatrix %>% select( -id_rep ) %>% as.matrix )
siwareOutliers <- siwareMatrix$id_rep[boxplot$outpoint]

siwareFrequencies <- siwareMatrix %>% select( -id_rep ) %>% colnames %>% as.double
rangevalSW <- c( min(siwareFrequencies), max(siwareFrequencies) )

siwareMatrix <- siwareMatrix %>% slice( - boxplot$outpoint )

# define a linear differential operator based on the second derivative.
Lcoef <- c(0,0,1)
ldOperator <- vec2Lfd( bwtvec =  Lcoef, rangeval = rangevalSW )

# Create a "tight" basis. With one node per frequency.
tightBasis <- create.bspline.basis( rangeval = rangevalSW, nbasis = length(siwareFrequencies), norder = 5 )

lambdaList <- 10^seq( from = 1, to = 10, by = 1 )
nlam <- length( lambdaList )
dfSave <- rep(NA,nlam)
names(dfSave) <- lambdaList
gcvSave <- dfSave

siwareCols <- siwareMatrix %>%
  select( - id_rep ) %>%
  as.matrix %>%
  t()

colnames( siwareCols ) <- siwareMatrix$id_rep

for ( ilam in 1:nlam ) {
  cat( paste( 'log10 lambda = ', lambdaList[ilam], '\n' ) )
  lambda <- lambdaList[ilam]
  fdParobj <- fdPar(fdobj = tightBasis, Lfdobj = ldOperator, lambda = lambda)
  smoothlist <- smooth.basis( siwareFrequencies, siwareCols , fdParobj )
  dfSave[ilam] <- smoothlist$df
  gcvSave[ilam] <- sum( smoothlist$gcv )
}

plot( lambdaList, gcvSave, type='b', lwd=2 )

## lambda <- which( gcvSave == min(gcvSave) )
lambda <- 10000
fdParobj <- fdPar(fdobj = tightBasis, Lfdobj = ldOperator, lambda = lambda)
smoothlist <- smooth.basis( siwareFrequencies, siwareCols , fdParobj )

coefs <- smoothlist$fd$coefs
tightBasisMat <- predict( tightBasis, siwareFrequencies )
smoothedSiware <- ( tightBasisMat %*% coefs  ) %>%
  t()

colnames( smoothedSiware ) <- siwareFrequencies

smoothedSiware <- as_tibble( smoothedSiware ) %>%
  mutate( id_rep = siwareMatrix$id_rep )

predictedCols <- dataframe %>%
  filter( id_rep %in% smoothedSiware$id_rep ) %>%
  select( id_rep, Antioxidants, Polyphenols, replica )

smoothedSiware <- left_join( x = smoothedSiware, y = predictedCols, by = "id_rep" )

## siwareKale %>%
##   arrange( Antioxidants ) %>%
##   ggplot() +
##   aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
##   geom_line() +
##   guides( label = FALSE, fill = FALSE, text = FALSE ) +
##   scale_color_viridis_c()

smoothedSiware %>%
  gather( -id_rep, -Antioxidants, -replica , key = wavelength, value = waveIntensity ) %>%
  arrange( id_rep, Antioxidants ) %>%
  mutate( wavelength = as.double(wavelength) ) %>%
  na.omit() %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  scale_color_viridis_c()

swX <- smoothedSiware %>%
  select( - replica, - id_rep, - Polyphenols, -Antioxidants ) %>%
  as.matrix() %>%
  t()

colnames( swX ) <- smoothedSiware$id_rep

boxplot <- fbplot( swX )
outliersSW2 <- smoothedSiware[boxplot$outpoint,"id_rep"]

outliersKaleSiwareJuice <- c( outliersSW, outliersSW2$id_rep )
write_rds( x=outliersKaleSiwareJuice, path="kaleSWJOutliers.Rds" )

smoothedSWFDS <- fds( x = siwareFrequencies, y = swX )

boxplotHDR <- fboxplot( data = smoothedSWFDS, plot.type = "functional", type = "hdr" )

## outliersSWRainbow <- foutliers( data = smoothedSWFDS, method = "depth.pond" )
## outSWR <- smoothedSiware[outliersSWRainbow$outliers,"id_rep"]


ooutliersHDR <- c( "2219_1",
                 "2376_2_1",
                 "3234_1",
                 "3234_2",
                 "3237_1",
                 "3436_3_1",
                 "3436_3_2",
                 "3696_1",
                 "3696_2",
                 "3915_1",
                 "3917_1",
                 "4571_3_1",
                 "4725_3_1",
                 "4725_3_2",
                 "4972_1_1",
                 "5112_3_1",
                 "5112_3_2"
                 )


smoothedSiwareLong <- smoothedSiware %>%
  gather( -id_rep, -Antioxidants, -replica , key = wavelength, value = waveIntensity ) %>%
  arrange( id_rep, Antioxidants ) %>%
  mutate( wavelength = as.double(wavelength) ) %>%
  na.omit()

sslOut <- smoothedSiwareLong %>%
  filter( id_rep %in% outliersSW2$id_rep )

smoothedSiwareLong %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  geom_line( data = sslOut, color = "red", size = 2 ) +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  scale_color_viridis_c()

swY <- smoothedSiware %>%
  filter( replica == 1 ) %>%
  select( Antioxidants ) %>%
  slice( - boxplot$outpoint ) %>%
  as.vector

swX <- smoothedSiware %>%
  filter( replica == 1 ) %>%
  select( - replica, - id_rep, - Polyphenols, -Antioxidants ) %>%
  slice( - boxplot$outpoint ) %>%
  as.matrix()


rawFitInf <- pfr( swY ~ fpc( swX ) )

summary( rawFitInf )

penalizedFitInf <- pfr( swY ~ lf( swX, bs = "ps", k = 30, fx = TRUE ) )
summary( penalizedFitInf )

swY <- data %>%
  filter( !(id_rep %in% outliersSW ) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swX <- data %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFit <- pfr( swY ~ fpc( swX ) )

summary( rawFit )

penalizedFit <- pfr( swY ~ lf( swX, bs = "ps", k = 45, fx = TRUE ) )

summary( penalizedFit )

```

#### Raw Data cleaned

```{r raw data cleaned}
swY <- data %>%
  filter( !(id_rep %in% outliersSW ) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swX <- data %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFit <- pfr( swY ~ fpc( swX ) )

summary( rawFit )

penalizedFit <- pfr( swY ~ lf( swX, bs = "ps", k = 45, fx = TRUE ) )
summary( penalizedFit )

swY <- data %>%
  filter( cluster == "superior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swX <- data %>%
  filter( cluster == "superior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFit <- pfr( swY ~ fpc( swX ) )
summary( rawFit )

penalizedFit <- pfr( swY ~ lf( swX, bs = "ps", k = 45, fx = TRUE ) )
summary( penalizedFit )

swYinf <- data %>%
  filter( cluster == "inferior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

swXinf <- data %>%
  filter( cluster == "inferior" ) %>%
  filter( !(id_rep %in% outliersSW) ) %>%
  filter( !(id_rep %in% outliersSW2 ) ) %>%
  filter( replica == 1 ) %>%
  select( contains( "siware" ) ) %>%
  as.matrix()

rawFitInf <- pfr( swYinf ~ fpc( swXinf ) )
summary( rawFitInf )

penalizedFitInf <- pfr( swYinf ~ lf( swXinf, bs = "ps", k = 45, fx = TRUE ) )
summary( penalizedFitInf )
```

## OurSci

```{r first plots}

dataframe <- read_rds("../modelling/dfStrippedOfFarOutliers.Rds")

juiceVars <- dataframe %>% select( contains("Juice"), -contains("Device"), -contains("Date"), -contains("siware") ) %>% colnames()
surfaceVars <- dataframe %>% select( contains("Surface"), -contains("Device"), -contains("Date"), -contains("siware") ) %>% colnames()
targetVars <- c("Polyphenols", "Antioxidants", "Color")

## We'll add a column for identification of measuring repetitions

presentIDisReplica <- function( state, id ) {
  previousId <- state$id
  count <- state$count
  if ( id == previousId ) {
    newCount <- count + 1
  } else { newCount <- 1 }
  return( list( "id" = id, "count" = newCount ) )
}

replicasList <- purrr::accumulate( dataframe$id_full, presentIDisReplica, .init=list( "id" = 0, "count" = 1 ) )
replicasCol <- map_dbl( replicasList, ~(.x)$count )
dataframe$replica <- replicasCol[-1]
dataframe <- mutate( dataframe, id_rep = paste( id_full, replica, sep="_" ) )

## Now, we add the variation sequences

data <- dataframe %>%
  select( one_of(!!!juiceVars), one_of(!!!targetVars), Type, id_rep, replica ) %>%
  filter( Type == "kale" ) %>%
  arrange( id_rep ) %>%
  na.omit()

osKale <- data  %>%
  gather( !!!juiceVars, key = "wavelength", value = "waveIntensity" ) %>%
  mutate( wavelength = as.factor(wavelength) ) %>%
  arrange( Antioxidants, id_rep, wavelength )

osKale$wavelength <- str_extract( osKale$wavelength, pattern = "[0-9]+" )
osKale$wavelength %<>% as.double

osKale %>%
  arrange( -Antioxidants ) %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  facet_wrap( Color ~ . )
  scale_color_viridis_c()

osKale %>%
  arrange( Antioxidants ) %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  scale_color_viridis_c()

oursciKaleMat <- osKale %>%
  ggplot() +
  aes( x = as.factor(wavelength), y = as.factor(Antioxidants), z = waveIntensity, fill = waveIntensity, colour = waveIntensity, alfa = 0.3, label = id_rep ) +
  geom_tile( size = 15 ) +
  scale_fill_viridis_c() +
  scale_color_viridis_c()

oursciKaleMat

osKale %>%
  ## filter( Polyphenols > 160 ) %>%
  ggplot() +
  aes( x = as.factor( wavelength ), y = as.factor(Polyphenols), z = waveIntensity, fill = waveIntensity, colour = waveIntensity, alfa = 0.3, label = id_rep ) +
  geom_tile( height = 2, width = 16 ) +
  scale_fill_viridis_c() +
  scale_color_viridis_c()

osKale %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  ggtitle( "Curves for SWIR Juice" ) +
  scale_color_viridis_c()

osKale %>%
  ggplot() +
  aes( x = Antioxidants, y = Polyphenols ) +
  stat_density_2d( aes( fill = ..level.. ), geom = "polygon" ) +
  ggtitle( "Joint Distribution of Antioxidants and Polyphenols" ) +
  scale_fill_viridis_c()

kaleMatrix <- data %>% select( contains("Scan") ) %>% as.matrix() %>% t()
colnames( kaleMatrix ) <- data$id_rep

boxplot <- fbplot( kaleMatrix )

outliersOS <- colnames(kaleMatrix)[ boxplot$outpoint ]

osKale %>%
  filter( !( id_rep %in% outliersOS ) ) %>%
  ggplot() +
  aes( x = as.factor( wavelength ), y = as.factor(Polyphenols), z = waveIntensity, fill = waveIntensity, colour = waveIntensity, alfa = 0.3, label = id_rep ) +
  geom_tile( height = 2, width = 16 ) +
  scale_fill_viridis_c() +
  scale_color_viridis_c()

osKale %>%
  filter( !( id_rep %in% outliersOS ) ) %>%
  ggplot() +
  aes( x = wavelength, y = waveIntensity, fill = as.factor( id_rep ), colour = Antioxidants, alfa = 0.3, label = id_rep ) +
  geom_line() +
  guides( label = FALSE, fill = FALSE, text = FALSE ) +
  ggtitle( "Curves for OurSci Juice" ) +
  scale_color_viridis_c()

```

#### Regression

```{r OurSci regression}

osY <- data %>%
  filter( !(id_rep %in% outliersOS) ) %>%
  filter( replica == 1 ) %>%
  select(Polyphenols)

osX <- data %>%
  filter( replica == 1 ) %>%
  filter( !(id_rep %in% outliersOS) ) %>%
  select( contains( "Scan" ) ) %>%
  as.matrix()

rawFit <- pfr( osY ~ fpc( osX ) )
summary( rawFit )

penalizedFit <- pfr( osY ~ lf( osX, bs = "ps", k = 10, fx = TRUE ) )
summary( penalizedFit )

```

# Generalization

This function will apply this proces to all the produce types, for all the four curves sets ( both `surface` and `juice` for `siware` and `Our Sci's` devices ).
Our target is to get a list of atypical data to supply input into our modelling efforts.

```{r function encapsulation}

## This first function will get us a matrix with the dataset and another one with the response, X and Y respectively.

siwareJuice <- dataframe %>% select( contains("Juice"), -contains("Device"), -contains("Date"), -contains("Scan") ) %>% colnames()

siwareSurface <- dataframe %>% select( contains("Surface"), -contains("Device"), -contains("Date"), -contains("Scan") ) %>% colnames()

uvJuice <- dataframe %>% select( contains("Scan"), -contains("surface"), -contains("ID") ) %>% colnames()

uvSurface <- dataframe %>% select( contains("Scan"), -contains("juice"), -contains("ID") ) %>% colnames()

responseVars <- c( "Polyphenols", "Antioxidants" )

curveSets <- list(
  "siwareJuice" = siwareJuice,
  "siwareSurface" = siwareSurface,
  "uvJuice" = uvJuice,
  "uvSurface" = uvSurface
)

siwareFrequencies <- siwareMatrix %>% select( -id_rep ) %>% colnames %>% as.double
rangevalSW <- c( min(siwareFrequencies), max(siwareFrequencies) )

getFunctionGrids <- function( dataframe, produce, curveSet = c("uvJuice", "uvSurface", "siwareJuice", "siwareSurface") ) {
  productFrame <- dataframe %>%
    filter( Type == !!produce ) %>%
    select( !!!curveSets[[curveSet]], !!!responseVars, id_rep ) %>%
    na.omit()

  regressorsMatrix <- productFrame %>%
    select( !!!curveSets[[curveSet]] ) %>%
    as.matrix() %>%
    t()

  colnames(regressorsMatrix) <- productFrame$id_rep

  antiY <- productFrame$Antioxidants
  polyY <- productFrame$Polyphenols

  output <- list(
    "regressors" = regressorsMatrix,
    "antiY" = antiY,
    "polyY" = polyY
  )

  return( output )
}

test <- getFunctionGrids( dataframe = dataframe, curveSet = "siwareSurface", produce = "grape" )

findOutliers <- function( dataframe, produce, curveSet = c("uvJuice", "uvSurface", "siwareJuice", "siwareSurface") ) {
  grids <- getFunctionGrids( dataframe, produce, curveSet )
  regressors <- grids$regressors
  boxplot <- fbplot( regressors, plot = FALSE )
  outliers <- boxplot$outpoint

  ids <- colnames( regressors )
  return( ids[outliers] )
}

testFO1 <- findOutliers( dataframe = dataframe, curveSet = "uvSurface", produce = "grape" )

findOutliers2 <- function( dataframe, produce, curveSet = c("uvJuice", "uvSurface", "siwareJuice", "siwareSurface", lamda = 10^4), scale = TRUE, retScaled = FALSE, retSmoothed = FALSE ) {
  grids <- getFunctionGrids( dataframe, produce, curveSet )
  regressors <- grids$regressors
  boxplot <- fbplot( regressors, plot = FALSE )
  outliers <- boxplot$outpoint
  ids <- colnames( regressors )
  outlierIDs <- ids[outliers]

  regressors <- regressors[,-outliers]

  if ( scale ) {
    regressors <- scale( x = regressors, center = FALSE )
  }

  # define a linear differential operator based on the second derivative.
  Lcoef <- c(0,0,1)
  ldOperator <- vec2Lfd( bwtvec =  Lcoef, rangeval = rangevalSW )
  # Create a "tight" basis. With one node per frequency.
  tightBasis <- create.bspline.basis( rangeval = rangevalSW, nbasis = length(siwareFrequencies), norder = 5 )
  # define functional data object and feed smoothing function
  fdParobj <- fdPar(fdobj = tightBasis, Lfdobj = ldOperator, lambda = lambda)
  smoothlist <- smooth.basis( siwareFrequencies, regressors , fdParobj )
  # get numeric values
  coefs <- smoothlist$fd$coefs
  tightBasisMat <- predict( tightBasis, siwareFrequencies )
  smoothed <- ( tightBasisMat %*% coefs  )

  rownames( smoothed ) <- paste( curveSet, "smooth", siwareFrequencies, sep = "_" )

  boxplot2 <- fbplot( smoothed, plot = FALSE )

  outliers2 <- boxplot2$outpoint

  ids2 <- colnames( smoothed )
  outlierIDs2 <- ids2[outliers2]

  output <- list(
    outliers = c(outlierIDs, outlierIDs2)
  )

  if ( retScaled ) {
    output$scaled = regressors[ ,-outliers2 ]
  }

  if ( retSmoothed ) {
    output$smoothed = smoothed[ ,-outliers2 ]
  }

  return( output )
}

test <- findOutliers2( dataframe = dataframe, curveSet = "siwareSurface", produce = "grape", retScaled = TRUE, retSmoothed = TRUE )

smoothedAsDataframe <- function( smoothed ) {
  smoothedDF <- smoothed %>%
    t() %>%
    as_tibble( rownames = "id_rep" ) 

  ## ## This could be usefull to build a function that takes the dataframe and returns the smoothed produce type table.
  ## predictedCols <- dataframe %>%
  ##   filter( id_rep %in% smoothedSiware$id_rep ) %>%
  ##   select( id_rep, Antioxidants, Polyphenols, replica )

  smoothedDF
}

zzz <- fds( y = test[["smoothed"]], x = siwareFrequencies )
foutliers( data = zzz, method = "depth.pond" )


```
