---
title: "2019 Survey. Modelling. New attempt, simplified cathegories."
author: "Real Food Campaign"
date: "May 29, 2020"
output:
  # pdf_document: default
  html_document: default
---

```{r preamble, echo=F}
library(knitr)
library(broom)
library("magrittr")
library("doParallel")
library(caret)
library("tidyverse")
## library("gpls")
library("randomForest")

set.seed(42424242)
## setwd("/home/oc/OurSci/rfc-docs/2019")
## setwd("../")
```

# Novelties in this brief

Fundamentally, we've changed our quantiles organization strategy and now we are classifying into `inferior`, `medium` and `superior` categories. We've also increased the metada made available to the models and considered also classification via a PLS transformation.

## Importing Needed Objects

These have been obtained in the `dataAnalisis.Rmd` main script, located at the root folder.

* `clasFramePoly` and `clasFrameAnti` are datasets prepared for our modelling. They are stripped of unneeded columns, their cathegories are factorized and quartile columns exist for the named variables. They've been splitted because this makes easier to define the modelling functions in the calls (we invoke every variable, with `Poly ~ .`).
* `variablesSets` contains the tooling we need to adjust the models: a list of sets of variables ( `datasets` ), the metadata list. ( `explicativeMetadata` ) and the dataframe over with we map the modelling, with a column for every produce type and another one for every possible dataset combinaion ( `modelsFrame` ). 

```{r importing}

clasFramePoly <- read_rds( "./classFramePoly.Rds" )
clasFrameAnti <- read_rds( "./clasFrameAnti.Rds" )

mFTooling <- read_rds( "./variablesSets.Rds" )

## datasets <- mFTooling$datasetsVarSets
explicativeMetadata <- mFTooling$metadata
modelsFrame <- mFTooling$modelsFrame

explicativeMetadata <- c(
  "Brix",
  "Source",
  "Color"
  ## "Dry_Mass_%_(x_100)",
  ## State <- turn into climate regions
)

explicativeMetadata2 <- c(
  "Brix",
  "Source",
  "Color",
  "Dry_Mass_%_(x_100)"
)

regsframe <- clasFrameAnti %>%
  select( -contains( "Device_ID" ) ) %>%
  select( contains( "siware" ), contains("Scan"), Type, Polyphenols, Antioxidants, id_sample, !!explicativeMetadata, `Dry_Mass_%_(x_100)` ) %>%
  filter( !is.na(Type) ) %>%
  filter( Type != "other" ) %>%
  mutate( Type = fct_drop( Type ),
         Color = as.factor(Color),
         Source = as.factor(Source)
         )

produce <-  regsframe$Type %>% unique %>% na.omit %>% enframe( name=NULL, value="produce" ) %>% filter( produce != "other" )
siwareJuiceVars <- regsframe %>% select( contains("Juice-siware") ) %>% colnames
siwareSurfaceVars <- regsframe %>% select( contains("Surface-siware") ) %>% colnames
uvJuiceVars <- regsframe %>% select( contains("Juice_Scan") ) %>% colnames
uvSurfaceVars <- regsframe %>% select( contains("Surface_Scan") ) %>% colnames

datasets <- list(
  metadata = explicativeMetadata,
  uvJuice = uvJuiceVars,
  uvSurface = uvSurfaceVars,
  uvAll = c( uvJuiceVars, uvSurfaceVars, explicativeMetadata ),
  siwareJuice = siwareJuiceVars,
  siwareSurface = siwareSurfaceVars,
  siwareAll = c( siwareJuiceVars, siwareSurfaceVars, explicativeMetadata ),
  juiceAll = c( uvJuiceVars, siwareJuiceVars, explicativeMetadata ),
  surfaceAll = c( uvSurfaceVars, siwareSurfaceVars, explicativeMetadata ),
  all = c( uvJuiceVars, siwareJuiceVars, uvSurfaceVars, siwareSurfaceVars, explicativeMetadata )
)

datasets2 <- list(
  metadata2 = explicativeMetadata2,
  uvAll2 = c( uvJuiceVars, uvSurfaceVars, explicativeMetadata2 ),
  siwareAll2 = c( siwareJuiceVars, siwareSurfaceVars, explicativeMetadata2 ),
  juiceAll2 = c( uvJuiceVars, siwareJuiceVars, explicativeMetadata2 ),
  surfaceAll2 = c( uvSurfaceVars, siwareSurfaceVars, explicativeMetadata2 ),
  all2 = c( uvJuiceVars, siwareJuiceVars, uvSurfaceVars, siwareSurfaceVars, explicativeMetadata2 )
)

## produce.sample_type+ produce.brix + produce.sample_color + farm_variety + sample_source + climate_region

datasetsNames <- datasets %>% names %>% enframe( name=NULL, value="dataset")
```

# Polyphenols

## Clasification

```{r polyphenols quartiles }

treeTGrid <- expand.grid(
  mtry = 1:40
)

classTree4 <- function( pType, dSet, df, verbose = FALSE, kFolds = 7 ) {
  explicative <- datasets[[dSet]]
  modelData <- df %>% filter( Type == !!pType ) %>% select( quartiles3Cat, !!explicative, id_sample )  %>% na.omit %>% mutate( quartiles3Cat = fct_drop( quartiles3Cat ) )
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  if (verbose) {
    print(modelData)
  }

  treeTCont <- trainControl( method="cv",
                            index = repetitionsFolds,
                            number = kFolds
                            )
  model <- train(
    quartiles3Cat ~ .,
    data = modelData,
    method = "rf",
    trControl = treeTCont,
    tuneGrid = treeTGrid,
    ntree = 500
  )
  return(model)
}

## cluster <- makePSOCKcluster(4)
## registerDoParallel(cluster)

## ## testTree <- classTree4( "kale", "all", clasFramePoly, TRUE )

## allPolyTreeC <- modelsFrame

## allPolyTreeC4 <- allPolyTreeC %>% mutate(
##                                  model = map2( allPolyTreeC4$produce, allPolyTreeC4$dataset, ~classTree4( .x, .y, clasFramePoly ) ),
##                                )

## stopCluster(cluster)

## allPolyTreeC4 <- allPolyTreeC4 %>% mutate(
##                                    Accuracy = map_dbl( allPolyTreeC$model, ~ .x$results %>%
##                                                                              arrange( - Accuracy ) %>%
##                                                                              slice(1) %>%
##                                                                              select( Accuracy ) %>%
##                                                                              unlist()
##                                                       ),
##                                    confMatrix = map( allPolyTreeC4$model, ~ ( .x$finalModel )$confusion ),
##                                    mtry = model %>% map_dbl( ~ .x["bestTune"] %>% unlist )
##                                  , oobError = map( allPolyTreeC4$model, ~ ( .x$finalModel )$err.rate )
##                                  )

## write_rds( x = allPolyTreeC4, path="./allPolyTreeClassification4.Rds" )

## allPolyTreeC4 <- read_rds( "./allPolyTreeClassification4.Rds" )

## lightPolyTreeC4 <- allPolyTreeC4 %>% select( -model )

## write_rds( x = lightPolyTreeC4, path = "./lightPolyTreeClassification4.Rds" )

## rm( list = "allPolyTreeC4" )

lightPolyTreeC4 <- read_rds( "./lightPolyTreeClassification4.Rds" )

tablePolyTreeC4 <- lightPolyTreeC4 %>%
  select( -contains( "confMatrix" ), - contains("oobError") ) %>%
  group_by( produce ) %>%
  arrange( produce, - Accuracy ) %>%
  top_n( n=5, wt=Accuracy )

```

#### Performance

```{r cuartiles poly table}
tablePolyTreeC4 %>% kable()
```

#### One of the Confuison Matrices

The new formulation has resulted in a reasonable increase over precission.

```{r poly quartiles spinach matrix}

validateConfusionMatrix <- function( dataframe, dataSet, pType, classVar, mtry, folds ) {
explicative <- datasets[[dataSet]]

modelData <- clasFramePoly %>%
  filter( Type == pType ) %>%
  select( !!classVar , !!explicative, id_sample )  %>%
  na.omit()

modelData[[classVar]] %<>% fct_drop()

repetitionsFolds <- groupKFold( group = modelData$id_sample, k = folds  )
modelData <- modelData %>% select( - id_sample )

spX <- modelData %>%
  select( - !!classVar ) %>%
  as.matrix
spY <- modelData[[classVar]]
<<<<<<< HEAD

matrices <- list()

for ( i in 1:length(repetitionsFolds) ) {
  spXTrain <- spX[ - repetitionsFolds[[i]],]
  spXTest <- spX[repetitionsFolds[[i]],]

  spYTrain <- spY[ - repetitionsFolds[[i]]]
  spYTest <- spY[repetitionsFolds[[i]]]

  iterationModel <- randomForest( x = spXTrain, y = spYTrain, spXTest, ytest = spYTest, mtry = mtry )
  matrix <- iterationModel$test$confusion
  matrices[[i]] <- matrix
}

=======

matrices <- list()

for ( i in 1:length(repetitionsFolds) ) {
  spXTrain <- spX[ - repetitionsFolds[[i]],]
  spXTest <- spX[repetitionsFolds[[i]],]

  spYTrain <- spY[ - repetitionsFolds[[i]]]
  spYTest <- spY[repetitionsFolds[[i]]]

  iterationModel <- randomForest( x = spXTrain, y = spYTrain, spXTest, ytest = spYTest, mtry = mtry )
  matrix <- iterationModel$test$confusion
  matrices[[i]] <- matrix
}

>>>>>>> d907f0b2d8267a0ea76090659e4402a20ea9e3de
confMat <- matrices %>% reduce( `+` ) %>% as_tibble( rownames = "class" )

clasDict <- list(
  "inf" = 0,
  "mid" = 1,
  "sup" = 2
)

plotMatrix <- confMat %>%
  gather( inf, mid, sup, key = "actualClass", value = "NumberAsigned" ) %>%
  rename( assignedClass = class ) %>%
  mutate( x = clasDict[ actualClass ] %>% unlist, y = clasDict[ assignedClass ] %>% unlist )

totalSamples <- plotMatrix$NumberAsigned %>% sum()

plotMatrix <- plotMatrix %>% mutate(
                               NumberAsigned = NumberAsigned / totalSamples,
                               class.error = class.error / folds,
                               labelNumberAsigned = ( round( NumberAsigned, 3 ) * 100  )%>% as.character() %>% paste0( ., "%" )
                             )

return( plotMatrix )

}

spinach4Siware <- validateConfusionMatrix( dataframe = clasFramePoly, dataSet = "siwareJuice", pType = "spinach", classVar = "quartiles3Cat", mtry = 38, folds = 3 )

spinach4Siware %>%
  ggplot() +
  aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) +
  geom_tile() +
<<<<<<< HEAD
  geom_text( aes( label = labelNumberAsigned ), size = 15, color = "darkgray" ) +
=======
  geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) +
>>>>>>> d907f0b2d8267a0ea76090659e4402a20ea9e3de
  scale_fill_viridis_c( "Number Asigned", option = "plasma" ) +
  ggtitle( "Spinach Quartiles, SIWARE Juice." )

```

### Classification by Quintiles


```{r polyphenols quintiles }

classTree5 <- function( pType, dSet, df, verbose = FALSE, kFolds = 7 ) {
  explicative <- datasets[[dSet]]
  modelData <- df %>% filter( Type == !!pType ) %>% select( quintiles3Cat, !!explicative, id_sample )  %>% na.omit %>% mutate( quintiles3Cat = fct_drop( quintiles3Cat ) )
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  if (verbose) {
    print(modelData)
  }

  treeTCont <- trainControl( method="cv",
                            index = repetitionsFolds,
                            number = kFolds
                            )
  model <- train(
    quintiles3Cat ~ .,
    data = modelData,
    method = "rf",
    trControl = treeTCont,
    tuneGrid = treeTGrid,
    ntree = 500
  )
  return(model)
}

## cluster <- makePSOCKcluster(4)
## registerDoParallel(cluster)

## ## testTree <- classTree5( "kale", "all", clasFramePoly, TRUE )

## allPolyTreeC <- modelsFrame

## allPolyTreeC <- allPolyTreeC %>% mutate(
##                                  model = map2( allPolyTreeC$produce, allPolyTreeC$dataset, ~classTree5( .x, .y, clasFramePoly ) ),
##                                )

## stopCluster(cluster)

## allPolyTreeC5 <- allPolyTreeC %>% mutate(
##                                    Accuracy = map_dbl( allPolyTreeC$model, ~ .x$results %>%
##                                                                              arrange( - Accuracy ) %>%
##                                                                              slice(1) %>%
##                                                                              select( Accuracy ) %>%
##                                                                              unlist()
##                                                       ),
##                                    confMatrix = map( allPolyTreeC$model, ~ ( .x$finalModel )$confusion ),
##                                    mtry = model %>% map_dbl( ~ .x["bestTune"] %>% unlist )
##                                  , oobError = map( allPolyTreeC$model, ~ ( .x$finalModel )$err.rate )
##                                  )

## write_rds( x = allPolyTreeC5, path="./allPolyTreeClassification5.Rds" )

## allPolyTreeC5 <- read_rds( "./allPolyTreeClassification5.Rds" )

## lightPolyTreeC5 <- allPolyTreeC5 %>% select( -model )

## write_rds( x = lightPolyTreeC5, path = "lightPolyTreeClassification5.Rds" )

## rm( list = "allPolyTreeC5" )

lightPolyTreeC5 <- read_rds( "./lightPolyTreeClassification5.Rds" )

tablePolyTreeC5 <- lightPolyTreeC5 %>%
  select( -contains( "confMatrix" ), - contains("oobError") ) %>%
  group_by( produce ) %>%
  arrange( produce, - Accuracy ) %>%
  top_n( n=5, wt=Accuracy )

```

#### Performance

```{r poly quintiles table}
tablePolyTreeC5 %>% kable()
```

#### One of the Confuison Matrices ( Carrot, `uvJuice` )

```{r poly quintiles carrot matrix}

carrot5UVJuice <- validateConfusionMatrix( dataframe = clasFramePoly, dataSet = "uvJuice", pType = "carrot", classVar = "quintiles3Cat", mtry = 1, folds = 9 )

carrot5UVJuice %>%
  ggplot() +
  aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) +
  geom_tile() +
<<<<<<< HEAD
  geom_text( aes( label = labelNumberAsigned ), size = 15, color = "darkgray" ) +
=======
  geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) +
>>>>>>> d907f0b2d8267a0ea76090659e4402a20ea9e3de
  scale_fill_viridis_c( "Number Asigned", option = "plasma" ) +
  ggtitle( "Carrot Quintiles Confusion Matrix" )
```

# Antioxidants

## Clasification

```{r antioxidants quartiles }

treeTGrid <- expand.grid(
  mtry = 1:40
)

classTree4 <- function( pType, dSet, df, verbose = FALSE, kFolds = 7 ) {
  explicative <- datasets[[dSet]]
  modelData <- df %>% filter( Type == !!pType ) %>% select( quartiles3Cat, !!explicative, id_sample )  %>% na.omit %>% mutate( quartiles3Cat = fct_drop( quartiles3Cat ) )
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  if (verbose) {
    print(modelData)
  }

  treeTCont <- trainControl( method="cv",
                            index = repetitionsFolds,
                            number = kFolds
                            )
  model <- train(
    quartiles3Cat ~ .,
    data = modelData,
    method = "rf",
    trControl = treeTCont,
    tuneGrid = treeTGrid,
    ntree = 500
  )
  return(model)
}

## cluster <- makePSOCKcluster(4)
## registerDoParallel(cluster)

## testTree <- classTree4( "kale", "all", clasFrameAnti, TRUE )

## allAntiTreeC4 <- modelsFrame

## allAntiTreeC4 <- allAntiTreeC4 %>% mutate(
##                                  model = map2( allAntiTreeC4$produce, allAntiTreeC4$dataset, ~classTree4( .x, .y, clasFrameAnti ) ),
##                                )

## stopCluster(cluster)

## allAntiTreeC4 <- allAntiTreeC4 %>% mutate(
##                                    Accuracy = map_dbl( allAntiTreeC4$model, ~ .x$results %>%
##                                                                              arrange( - Accuracy ) %>%
##                                                                              slice(1) %>%
##                                                                              select( Accuracy ) %>%
##                                                                              unlist()
##                                                       ),
##                                    confMatrix = map( allAntiTreeC4$model, ~ ( .x$finalModel )$confusion ),
##                                    mtry = model %>% map_dbl( ~ .x["bestTune"] %>% unlist )
##                                  , oobError = map( allAntiTreeC4$model, ~ ( .x$finalModel )$err.rate )
##                                  )

## write_rds( x = allAntiTreeC4, path="./allAntiTreeClassification4.Rds" )

## allAntiTreeC4 <- read_rds( "./allAntiTreeClassification4.Rds" )

## lightAntiTreeC4 <- allAntiTreeC4 %>% select( -model )

## write_rds( x = lightAntiTreeC4, path = "./lightAntiTreeClassification4.Rds" )

## rm( list = "allAntiTreeC4" )

lightAntiTreeC4 <- read_rds( "./lightAntiTreeClassification4.Rds" )

tableAntiTreeC4 <- lightAntiTreeC4 %>%
  select( -contains( "confMatrix" ), - contains("oobError") ) %>%
  group_by( produce ) %>%
  arrange( produce, - Accuracy ) %>%
  top_n( n=5, wt=Accuracy )
```

#### Performance

```{r antioxidants table quartiles}
tableAntiTreeC4 %>% kable()
```

#### One of the Confusion Matrices ( Cherry Tomatto, `uvAll` )

```{r anti quartiles tomato matrix}

explicative <- datasets[["uvJuice"]]

tomato4UVJuice <- validateConfusionMatrix( dataframe = clasFrameAnti, dataSet = "uvJuice", pType = "cherry_tomato", classVar = "quartiles3Cat", mtry = 35, folds = 12 )

tomato4UVJuice %>%
  ggplot() +
  aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) +
  geom_tile() +
<<<<<<< HEAD
  geom_text( aes( label = labelNumberAsigned ), size = 15, color = "darkgray" ) +
=======
  geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) +
>>>>>>> d907f0b2d8267a0ea76090659e4402a20ea9e3de
  scale_fill_viridis_c( "Number Asigned", option = "plasma" ) +
  ggtitle( "Cherry Tomato Quartiles, Our Sci juice Sacn." )

```


### Antioxidants Quintiles

<<<<<<< HEAD
```{ r antioxidants quintiles }
=======

```{ r antioxidants quintiles, cache = TRUE }
>>>>>>> d907f0b2d8267a0ea76090659e4402a20ea9e3de

treeTGrid <- expand.grid(
  mtry = 1:40
)

classTree5 <- function( pType, dSet, df, verbose = FALSE, kFolds = 7 ) {
  explicative <- datasets[[dSet]]
  modelData <- df %>% filter( Type == !!pType ) %>% select( quintiles3Cat, !!explicative, id_sample )  %>% na.omit %>% mutate( quintiles3Cat = fct_drop( quintiles3Cat ) )
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  if (verbose) {
    print(modelData)
  }

  treeTCont <- trainControl( method="cv",
                            index = repetitionsFolds,
                            number = kFolds
                            )
  model <- train(
    quintiles3Cat ~ .,
    data = modelData,
    method = "rf",
    trControl = treeTCont,
    tuneGrid = treeTGrid,
    ntree = 500
  )
  return(model)
}
```

```{r }



## cluster <- makePSOCKcluster(4)
## registerDoParallel(cluster)

## testTree <- classTree4( "kale", "all", clasFrameAnti, TRUE )

## allAntiTreeC5 <- modelsFrame

## allAntiTreeC5 <- allAntiTreeC5 %>% mutate(
##                                  model = map2( allAntiTreeC5$produce, allAntiTreeC5$dataset, ~classTree4( .x, .y, clasFrameAnti ) ),
##                                )

## stopCluster(cluster)

## allAntiTreeC5 <- allAntiTreeC5 %>% mutate(
##                                    Accuracy = map_dbl( allAntiTreeC5$model, ~ .x$results %>%
##                                                                              arrange( - Accuracy ) %>%
##                                                                              slice(1) %>%
##                                                                              select( Accuracy ) %>%
##                                                                              unlist()
##                                                       ),
##                                    confMatrix = map( allAntiTreeC5$model, ~ ( .x$finalModel )$confusion ),
##                                    mtry = model %>% map_dbl( ~ .x["bestTune"] %>% unlist )
##                                  , oobError = map( allAntiTreeC5$model, ~ ( .x$finalModel )$err.rate )
##                                  )

## write_rds( x = allAntiTreeC5, path="./allAntiTreeClassification5.Rds" )

## allAntiTreeC5 <- read_rds( "./allAntiTreeClassification5.Rds" )

## lightAntiTreeC5 <- allAntiTreeC5 %>% select( -model )

## write_rds( x = lightAntiTreeC5, path = "./lightAntiTreeClassification5.Rds" )

## rm( list = "allAntiTreeC5" )

lightAntiTreeC5 <- read_rds( "./lightAntiTreeClassification5.Rds" )

tableAntiTreeC5 <- lightAntiTreeC5 %>%
  select( -contains( "confMatrix" ), - contains("oobError") ) %>%
  group_by( produce ) %>%
  arrange( produce, - Accuracy ) %>%
  top_n( n=5, wt=Accuracy )

```

#### Performance

```{r antioxidants quintiles table}
tableAntiTreeC5 %>% kable()
```

#### One of the Confuison Matrices ( Grape, `uvJuice` )

```{r poly quintiles grape matrix}

grape5UVJuice <- validateConfusionMatrix( dataframe = clasFrameAnti, dataSet = "uvJuice", pType = "grape", classVar = "quintiles3Cat", mtry = 1, folds = 2 )

grape5UVJuice %>%
  ggplot() +
  aes( x = actualClass, y = assignedClass, fill = NumberAsigned ) +
  geom_tile() +
<<<<<<< HEAD
  geom_text( aes( label = labelNumberAsigned ), size = 15, color = "darkgray" ) +
=======
  geom_text( aes( label = labelNumberAsigned ), size = 10, color = "darkgray" ) +
>>>>>>> d907f0b2d8267a0ea76090659e4402a20ea9e3de
  scale_fill_viridis_c( "Number Asigned", option = "plasma" ) +
  ggtitle( "Grape Quintiles Confusion Matrix" )

```


# PLS

Even though performance is inferior to what we observed in the Random Forest Models, insight into this methods is usefull because they are a posible key to improvements, above all on the usage of the high dimensional SIWARE data.

## Polyphenols

```{r PLS}

trainPLS <- function( pType, dSet, df, verbose = FALSE, kFolds = 7 ) {
  explicative <- datasets[[dSet]]
  modelData <- df %>% filter( Type == !!pType ) %>% select( Polyphenols, !!explicative, id_sample ) %>% na.omit
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  if (verbose) {
    print(modelData)
  }

  trainPLS<- trainControl( method="cv",
                          index = repetitionsFolds,
                          number = kFolds )

  plsTGrid <- expand.grid(
    ncomp = 1:15
  )

  model <- caret::train(
                    Polyphenols ~ .,
                    data = modelData,
                    method = "pls",
                    trControl = trainPLS,
                    metric = "Rsquared",
                    tuneGrid = plsTGrid
                  )
  return( model )
}

trainPLSClas <- function( pType, dSet, df, verbose = FALSE, kFolds = 5 ) {
  explicative <- datasets[[dSet]]
  modelData <- df %>% filter( Type == !!pType ) %>% select( polyQuartile, !!explicative, id_sample ) %>% na.omit %>% mutate( polyQuartile = fct_drop( polyQuartile ) )
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  if (verbose) {
    print(modelData)
  }

  trainPLS <- trainControl( method="cv",
                           index = repetitionsFolds,
                          number = kFolds )

  plsTGrid <- expand.grid(
    ## ncomp = 1:floor( length(explicative) * 0.7)
    ncomp = 1:10
  )

  model <- caret::train(
                    polyQuartile ~ .,
                    data = modelData,
                    method = "pls",
                    trControl = trainPLS,
                    tuneGrid = plsTGrid
                  )
  return( model )
}

## cluster <- makePSOCKcluster(4)
## registerDoParallel(cluster)
## ## testPLS <- trainPLS( pType = "carrot", dSet = "siwareAll", df = clasFramePoly )
## ## testPLSClas <- trainPLSClas( pType = "carrot", dSet = "siwareAll", df = clasFramePoly )

## allPolyPLS <- modelsFrame

## allPolyPLS <- allPolyPLS %>% mutate(
##                                  model = map2( allPolyPLS$produce, allPolyPLS$dataset, ~trainPLS( .x, .y, clasFramePoly ) ),
##                                )

## stopCluster(cluster)

## write_rds( x = allPolyPLS, path="./allPolyPLS.Rds" )

## cluster <- makePSOCKcluster(4)
## registerDoParallel(cluster)

## allPolyPLSC <- modelsFrame

## allPolyPLSC <- allPolyPLSC %>% mutate(
##                                model = map2( allPolyPLSC$produce, allPolyPLSC$dataset, ~trainPLSClas( .x, .y, clasFramePoly ) ),
##                                )

## stopCluster(cluster)

## write_rds( x = allPolyPLSC, path="./allPolyPLSClas.Rds" )

## allPolyPLSC <- allPolyPLSC %>% mutate(
##                                    Accuracy = map_dbl( allPolyPLSC$model, ~ .x$results %>%
##                                                                              arrange( - Accuracy ) %>%
##                                                                              slice(1) %>%
##                                                                              select( Accuracy ) %>%
##                                                                              unlist()
##                                                       ),
##                                    confMatrix = map( allPolyPLSC$model, ~ ( .x$finalModel )$confusion ),
##                                    comps = model %>% map_dbl( ~ .x["bestTune"] %>% unlist )
##                                  , oobError = map( allPolyPLSC$model, ~ ( .x$finalModel )$err.rate )
##                                  )

## write_rds( x = allPolyPLSC, path="./allPolyPLSClas.Rds" )

## allPolyPLSC <- read_rds( "./allPolyPLSClas.Rds" )

## lightPolyPLSC <- allPolyPLSC %>% select( -model )

## write_rds( x = lightPolyPLSC, path = "./lightPolyPLSClas.Rds" )

## rm( list = "allPolyPLSC" )

lightPolyPLSC <- read_rds( "./lightPolyPLSClas.Rds" )

tablePolyPLSC <- lightPolyPLSC %>%
  select( -contains( "confMatrix" ), - contains("oobError") ) %>%
  group_by( produce ) %>%
  arrange( produce, - Accuracy ) %>%
  top_n( n=5, wt=Accuracy )
```

#### Performance 

```{r PLS clas performance}
tablePolyPLSC %>% kable()
```



